#pragma once

#include <sstream>
#include <string>
#include <algorithm>
#include "types.hpp"
#include "hasher.hpp"
#include "prng.hpp"
#include "util.hpp"

#include <iostream>
#include <iomanip>



template<class prng_t_, class char_t = u16>
class Tabulation1PermutationHash: public Hasher {
    static const u64 C = sizeof(u64) / sizeof(char_t);
    static const u64 D = sizeof(u32) / sizeof(char_t);
    static const u64 char_size = 8 * sizeof(char_t);
    static const u64 num_chars = (u64) 1 << char_size;

    u32* T1[C];

    char_t* const permutation;

    std::string prng_name;
public:
    using prng_t = prng_t_;

    Tabulation1PermutationHash(prng_t* prng) :
        permutation((char_t*) std::calloc(num_chars, sizeof(char_t))) {
        for (u64 table_idx = 0; table_idx < C; ++table_idx) {
            T1[table_idx] = (u32*) std::calloc(num_chars, sizeof(u32));
            for (u64 char_idx = 0; char_idx < num_chars; ++char_idx) {
                T1[table_idx][char_idx] = prng->next_int((u64) 1 << 32);
            }
        }

        for (u64 char_idx = 0; char_idx < num_chars; ++char_idx) {
            permutation[char_idx] = char_idx;
        }
        shuffle_array<char_t, prng_t>(permutation, permutation + num_chars, prng);

        prng_name = prng->name();
        delete prng;
    }

    ~Tabulation1PermutationHash() {
        for (u64 table_idx = 0; table_idx < C; ++table_idx) {
            free(T1[table_idx]);
        }
        free(permutation);
    }

    u32 hash(u64 x) const {
        u32 result = 0;
        char_t x_as_chars[C];
        std::memcpy(&x_as_chars, &x, sizeof(u64));

        for (u64 char_idx = 0; char_idx < C; ++char_idx) {
            result ^= T1[char_idx][x_as_chars[char_idx]];
        }

        char_t result_as_chars[D];
        std::memcpy(&result_as_chars, &result, sizeof(u32));
        // Assume most significant character is at (D - 1), not at 0
        result_as_chars[D - 1] = permutation[result_as_chars[D - 1]];
        std::memcpy(&result, &result_as_chars, sizeof(u32));

        return result;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(Tabulation1PermutationHash "
           << "[" << num_chars << "]^" << C << " "
           << "[" << num_chars << "]^" << D << " "
           << prng_name << ")";
        return ss.str();
    }
};
