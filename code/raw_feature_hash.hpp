#pragma once

#include <string>
#include <sstream>
#include <fftw3.h>
#include "types.hpp"
#include "transformer.hpp"
#include "util.hpp"
#include "hasher.hpp"


template<class hasher_t>
class RawFeatureHashLazy: public Transformer<sparse_vector*, dense_vector>, public Transformer<dense_vector, dense_vector> {
    const u64 source_dimension;
    const u32 target_dimension;
    const hasher_t hasher;

    inline void update_res(const u64 src_idx, const double val, dense_vector res) const {
        u32 raw_hash = hasher.hash(src_idx);
        u32 dst = distribute<31>(raw_hash >> 1, target_dimension);
        i8 sign = 2 * (i8) (raw_hash & 1) - 1;
        res.data[dst] += sign * val;
    }

public:
    using prng_t = typename hasher_t::prng_t;

    RawFeatureHashLazy(u64 source_dimension, u32 target_dimension, prng_t* prng) :
        source_dimension(source_dimension),
        target_dimension(target_dimension),
        hasher(hasher_t{prng}) {
        assert(target_dimension < (u64) 1 << 31);
    }

    ~RawFeatureHashLazy() {
    }

    dense_vector transform(sparse_vector* x) const {
        dense_vector res = calloc_real(target_dimension);
        for (auto const &pair : *x) {
            update_res(pair.first, pair.second, res);
        }
        delete x;
        return res;
    }

    dense_vector transform(dense_vector x) const {
        assert(x.size == source_dimension);
        dense_vector res = calloc_real(target_dimension);
        for (u64 idx = 0; idx < x.size; ++idx) {
            update_res(idx, x.data[idx], res);
        }
        delete_vector(x);
        return res;
    }

    u64 output_dimension() const {
        return target_dimension;
    }

    double normalisation_divisor() const {
        return 1;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(RawFeatureHashLazy " << hasher.name() << ")";
        return ss.str();
    }
};


template<class hasher_t>
class RawFeatureHashEager: public Transformer<sparse_vector*, dense_vector>, public Transformer<dense_vector, dense_vector> {
    const u64 source_dimension;
    const u32 target_dimension;
    u32* const source2target;
    std::string hasher_name;

    inline void update_res(const u64 src_idx, const double val, dense_vector res) const {
        u32 raw_hash = source2target[src_idx];
        u32 dst = raw_hash >> 1;
        i8 sign = 2 * (i8) (raw_hash & 1) - 1;
        res.data[dst] += sign * val;
    }

public:
    using prng_t = typename hasher_t::prng_t;

    RawFeatureHashEager(u64 source_dimension, u32 target_dimension, prng_t* prng) :
        source_dimension(source_dimension),
        target_dimension(target_dimension),
        source2target((u32*) std::calloc(source_dimension, sizeof(u32))) {
        assert(target_dimension < (u64) 1 << 31);
        hasher_t hasher{prng};
        for (u64 source_idx = 0; source_idx < source_dimension; ++source_idx) {
            source2target[source_idx] = distribute(hasher.hash(source_idx), target_dimension << 1);
        }
        hasher_name = hasher.name();
    }

    ~RawFeatureHashEager() {
        free(source2target);
    }

    dense_vector transform(sparse_vector* x) const {
        dense_vector res = calloc_real(target_dimension);
        for (auto const &pair : *x) {
            update_res(pair.first, pair.second, res);
        }

        delete x;
        return res;
    }

    dense_vector transform(dense_vector x) const {
        assert(x.size == source_dimension);
        dense_vector res = calloc_real(target_dimension);
        for (u64 source_idx = 0; source_idx < source_dimension; ++source_idx) {
            update_res(source_idx, x.data[source_idx], res);
        }
        delete_vector(x);
        return res;
    }

    u64 output_dimension() const {
        return target_dimension;
    }

    double normalisation_divisor() const {
        return 1;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(RawFeatureHashEager " << hasher_name << ")";
        return ss.str();
    }
};
