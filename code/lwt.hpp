#pragma once

#include <string>
#include <sstream>
#include "types.hpp"
#include "random_signs.hpp"
#include "raw_lwt.hpp"
#include "fjlt.hpp"
#include "transformer.hpp"
#include "util.hpp"


template<class hasher_t, class sparsity_strat_t>
class EagerLeanWalshTransform: public Transformer<dense_vector, dense_vector> {
    typedef EagerRandomSigns<hasher_t> signs_t;
    typedef EagerFJLT<hasher_t, sparsity_strat_t> last_jl_t;

    const u64 source_dimension;
    const u32 target_dimension;
    const signs_t signs;
    const RawEagerLeanWalshTransform lwt;
    const last_jl_t last_embedder;
    const double internal_normalisation_divisor;

public:
    using prng_t = typename hasher_t::prng_t;

    EagerLeanWalshTransform(u64 source_dimension, u32 target_dimension, gsl_matrix* seed_matrix,
                            prng_t* sign_prng, prng_t* fjlt_sign_prng, prng_t* fjlt_sparse_prng) :
        source_dimension(source_dimension),
        target_dimension(target_dimension),
        signs(signs_t{source_dimension, sign_prng}),
        lwt(RawEagerLeanWalshTransform{signs.output_dimension(), seed_matrix}),
        last_embedder(last_jl_t{lwt.output_dimension(), target_dimension, fjlt_sign_prng, fjlt_sparse_prng}),
        internal_normalisation_divisor(signs.normalisation_divisor() * lwt.normalisation_divisor() * last_embedder.normalisation_divisor()) {
    }

    ~EagerLeanWalshTransform() {
    }

    dense_vector transform(dense_vector x) const {
        assert(x.size == source_dimension);
        auto post_sign = signs.transform(x);
        auto post_lwt = lwt.transform(post_sign);
        auto post_last = last_embedder.transform(post_lwt);
        divide_vector(post_last, internal_normalisation_divisor);
        return post_last;
    }

    u64 output_dimension() const {
        return target_dimension;
    }

    double normalisation_divisor() const {
        return 1;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(EagerLWT"
           << " " << signs.name()
           << " " << lwt.name()
           << " " << last_embedder.name()
           << ")";
        return ss.str();
    }
};
