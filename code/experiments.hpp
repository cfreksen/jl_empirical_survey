#pragma once


#include <type_traits>
#include <map>
#include <fstream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <chrono>
#include <algorithm>
#include <random>

#include "types.hpp"
#include "util.hpp"
#include "generator.hpp"
#include "transformer.hpp"
#include "factories.hpp"


class ProgressMessenger {
    const double work_rate_estimate_stability = 0.999;

    const u64 step_size;
    const u64 total_trials;
    const u32 trials_width;
    u64 curr_step;
    ttime last_time;

    double work_rate_avg;

public:
    ProgressMessenger(u64 step_size, u64 total_trials) :
        step_size(step_size), total_trials(total_trials),
        trials_width(std::to_string(total_trials).length()) {
        assert(total_trials % step_size == 0);
        curr_step = 0;
        last_time = ttime{};
        work_rate_avg = 0.0;
    }

    void finish_step() {
        ttime curr_time = cclock::now();
        ++curr_step;
        u64 trials_done = curr_step * step_size;
        double percentage = (100.0 * trials_done) / total_trials;

        std::cout << "\033[K";
        std::cout << std::setfill(' ');
        std::cout << std::setw(7) << std::setprecision(3) << std::fixed
                  << percentage << " %,  "
                  << std::setw(trials_width) << trials_done << " / "
                  << total_trials << ".  "
                  << std::setw(7) << std::setprecision(2) << std::fixed;

        double work_rate = -1;
        if (last_time == ttime{}) {
            std::cout << "?.??";
            work_rate_avg = 0.0;
        } else {
            auto curr_time = cclock::now();
            std::chrono::duration<double> step_time = curr_time - last_time;
            work_rate = step_size / step_time.count();
            std::cout << work_rate;
            if (work_rate_avg == 0.0) {
                work_rate_avg = work_rate;
            } else {
                work_rate_avg = work_rate_estimate_stability * work_rate_avg + (1 - work_rate_estimate_stability) * work_rate;
            }
        }
        std::cout << " vectors/s.  "
                  << "ETA: ";

        if (work_rate <= 0) {
            std::cout << "? days ??:??:??";
        } else {
            u64 seconds_left_total = (u64) ((total_trials - trials_done) / work_rate_avg);
            u64 seconds_left = seconds_left_total % 60;
            u64 minutes_left_total = (seconds_left_total) / 60;
            u64 minutes_left = minutes_left_total % 60;
            u64 hours_left_total = minutes_left_total / 60;
            u64 hours_left = hours_left_total % 24;
            u64 days_left_total = hours_left_total / 24;
            u64 days_left = days_left_total;
            std::cout << days_left << " days "
                      << std::setw(2) << std::setfill('0') << hours_left << ":"
                      << std::setw(2) << std::setfill('0') << minutes_left << ":"
                      << std::setw(2) << std::setfill('0') << seconds_left
                      << std::setfill(' ');
        }

        std::cout << "\r" << std::flush;

        last_time = curr_time;
    }

    void end_messages() {
        std::cout << "\033[K" << std::flush;
    }
};


template<class input_t>
std::vector<double>* measure_embedding_time(Transformer<input_t, dense_vector>* jlt,
                                            Generator<input_t>* input_generator,
                                            u64 num_measurements,
                                            u64 num_warmups) {
    auto res = new std::vector<double>{};
    res->reserve(num_measurements + num_warmups);
    for (u64 measurement_idx = 0; measurement_idx < num_measurements + num_warmups; ++measurement_idx) {
        input_t input;
        input_generator->gen_vector(input);
        auto pre_embed = cclock::now();
        auto embedded = jlt->transform(input);
        auto post_embed = cclock::now();
        delete_vector(embedded);

        std::chrono::duration<double> embed_time = post_embed - pre_embed;
        res->push_back(embed_time.count());
    }
    delete jlt;
    delete input_generator;
    res->erase(res->begin(), res->begin() + num_warmups);
    return res;
}


template<class input_t>
void time_varying_input_size(JLTFactory<input_t>* jlt_maker,
                             GeneratorFactory<input_t>* gen_maker,
                             const std::string jlt_seed_path,
                             const std::string gen_seed_path,
                             const std::string output_file_path,
                             u32 target_dimension,
                             u64 min_size, u64 max_size,
                             u64 measurements_per_size,
                             u64 warmups_per_size,
                             u64 rounds_of_measurements,
                             std::string label="") {
    const u64 num_substeps = 6;

    bool is_input_dense = std::is_same<input_t, dense_vector>::value;

    std::ofstream output_file{output_file_path};

    u32 size_width = std::max(std::to_string(max_size).length(), (u64) 6);
    u32 time_width = time_precision10 + 3;

    output_file << "## Metadata" << std::endl
                << "# input data type:  " << std::setw(size_width) << (is_input_dense ? "dense" : "sparse") << "\n"
                << "# min input size:   " << std::setw(size_width) << min_size << "\n"
                << "# max input size:   " << std::setw(size_width) << max_size << "\n"
                << "# target dimension: " << std::setw(size_width) << target_dimension << "\n"
                << "# num measurements: " << std::setw(size_width) << measurements_per_size << "\n"
                << "# num warmups:      " << std::setw(size_width) << warmups_per_size << "\n"
                << "# num meas. rounds: " << std::setw(size_width) << rounds_of_measurements << "\n"
                << "# JLT seed: " << jlt_seed_path << "\n"
                << "# gen seed: " << gen_seed_path << "\n";

    u64 dummy_dimension = (u64) 1 << 8;
    auto dummy_jlt = jlt_maker->mk_jlt(jlt_seed_path, dummy_dimension, target_dimension, 0);
    output_file << "# JLT: " << dummy_jlt->name() << "\n";
    delete dummy_jlt;
    auto dummy_gen = gen_maker->mk_gen(gen_seed_path, dummy_dimension);
    output_file << "# Generator: " << dummy_gen->name() << "\n";
    delete dummy_gen;

    if (label != "") {
        output_file << "# label: " << label << "\n";
    }
    output_file << "\n";

    output_file << "## Data" << "\n";
    output_file << "# "
                << std::setw(size_width - 2) << (is_input_dense ? "dim" : "s") << " "
                << std::setw(time_width) << "minimum" << " "
                << std::setw(time_width) << "10%" << " "
                << std::setw(time_width) << "median" << " "
                << std::setw(time_width) << "90%" << " "
                << std::setw(time_width) << "maximum" << std::endl;
    output_file << std::setprecision(time_precision10)
                << std::fixed;

    std::vector<u64> input_sizes{};
    double input_size_d = min_size;
    for (u64 trial = 0; input_size_d < max_size; ++trial) {
        if (trial % num_substeps == 0) {
            input_size_d = min_size << (trial / num_substeps); // Force exact power of 2
        } else {
            input_size_d = input_size_d * std::pow(2.0, 1.0 / num_substeps);
        }
        u64 next_size = (u64) input_size_d;
        if (input_sizes.empty() || input_sizes.back() != next_size) {
            input_sizes.push_back(next_size);
        }
    }

    std::map<u64, std::vector<double>> size2measurements{};

    u64 num_trials = input_sizes.size() * rounds_of_measurements;
    u64 total_measurements_per_size = measurements_per_size * rounds_of_measurements;
    ProgressMessenger progress{measurements_per_size, measurements_per_size * num_trials};

    for (u64 measurement_round = 0; measurement_round < rounds_of_measurements; ++measurement_round) {
        std::random_device rd;
        std::mt19937 g(rd());
        std::shuffle(input_sizes.begin(), input_sizes.end(), g);
        for (const u64 input_size : input_sizes) {
            const u64 source_dimension = is_input_dense ? input_size : (input_size * (measurements_per_size + warmups_per_size));
            auto jlt = jlt_maker->mk_jlt(jlt_seed_path, source_dimension, target_dimension, 0);
            auto gen = gen_maker->mk_gen(gen_seed_path, input_size);
            auto measurements = measure_embedding_time(jlt, gen, measurements_per_size, warmups_per_size);
            assert(measurements->size() == measurements_per_size);
            size2measurements[input_size].insert(size2measurements[input_size].end(), measurements->begin(), measurements->end());
            delete measurements;
            progress.finish_step();
        }
    }
    progress.end_messages();

    for (auto &kv : size2measurements) {
        u64 input_size = kv.first;
        auto measurements = kv.second;
        // While sorting is O(n lg n) and finding a few elements by rank
        // is O(n), this is simpler and fast enough. Sorting short lists
        // is cheap.
        std::sort(measurements.begin(), measurements.end());
        double min = measurements[(u64) (0.00 * total_measurements_per_size)];
        double p10 = measurements[(u64) (0.10 * total_measurements_per_size)];
        double med = measurements[(u64) (0.50 * total_measurements_per_size)];
        double p90 = measurements[(u64) (0.90 * total_measurements_per_size)];
        double max = measurements[(u64) (1.00 * total_measurements_per_size) - 1];
        output_file << std::setw(size_width) << input_size << " "
                    << std::setw(time_width) << min << " "
                    << std::setw(time_width) << p10 << " "
                    << std::setw(time_width) << med << " "
                    << std::setw(time_width) << p90 << " "
                    << std::setw(time_width) << max << std::endl;
    }
}


template<class input_t>
void run_distortion_experiment(JLTFactory<input_t>* jlt_maker,
                               GeneratorFactory<input_t>* gen_maker,
                               const std::string jlt_seed_path,
                               const std::string gen_seed_path,
                               const std::string output_file_path,
                               u64 input_size,
                               u32 target_dimension,
                               u64 num_jlt_samples,
                               u64 max_data_samples_per_jlt,
                               std::string jlt_label="",
                               std::string gen_label="",
                               std::string gen_short_name="") {
    bool is_input_dense = std::is_same<input_t, dense_vector>::value;

    u64 source_dimension = is_input_dense ? input_size : (u64) 1 << 63;
    auto gen = gen_maker->mk_gen(gen_seed_path, input_size);
    const u64 num_data_samples_per_jlt = gen->is_constant() ? 1 : max_data_samples_per_jlt;

    std::cout << "Writing to " << output_file_path << std::endl;
    std::ofstream output_file{output_file_path};

    const u64 num_measurements = num_jlt_samples * num_data_samples_per_jlt;

    u32 size_width = std::max(std::to_string(input_size).length(), (u64) 6);
    u32 distortion_width = std::numeric_limits<double>::max_digits10 + 3;
    u32 count_width = std::max(std::to_string(num_measurements).length(), (u64) 5);

    auto dummy_jlt = jlt_maker->mk_jlt(jlt_seed_path, source_dimension, target_dimension, 0);

    output_file << "## Metadata\n"
                << "# input data type:  " << std::setw(size_width) << (is_input_dense ? "dense" : "sparse") << "\n"
                << "# input size:       " << std::setw(size_width) << input_size << "\n"
                << "# target dimension: " << std::setw(size_width) << target_dimension << "\n"
                << "# num measurements: " << std::setw(size_width) << num_measurements << "\n"
                << "# num jlt samples:  " << std::setw(size_width) << num_jlt_samples << "\n"
                << "# num data per jlt: " << std::setw(size_width) << num_data_samples_per_jlt << "\n"
                << "# JLT seed: " << jlt_seed_path << "\n"
                << "# gen seed: " << gen_seed_path << "\n"
                << "# JLT:       " << dummy_jlt->name() << "\n"
                << "# Generator: " << gen->name() << "\n";
    delete dummy_jlt;
    if (jlt_label != "") {
        output_file << "# JLT label: " << jlt_label << "\n";
    }
    if (gen_label != "") {
        output_file << "# Generator label: " << gen_label << "\n";
    }
    if (gen_short_name != "") {
        output_file << "# Generator ID: " << gen_short_name << "\n";
    }

    output_file << "\n";

    output_file << "## Data" << "\n";
    output_file << "# "
                << std::setw(distortion_width - 2) << "distortion" << " "
                << std::setw(count_width) << "count" << std::endl;

    output_file << std::setprecision(std::numeric_limits<double>::max_digits10)
                << std::fixed;

    u64 step_size = std::min((u64) 1 << 4, num_jlt_samples / 2);
    ProgressMessenger progress{step_size * num_data_samples_per_jlt, num_measurements};
    assert(num_jlt_samples % step_size == 0);
    u64 num_steps = num_jlt_samples / step_size;

    double mk_jlt_time_total = 0.0;
    double embed_time_total = 0.0;

    std::map<double, u64> measurements{};
    for (u64 step_idx = 0, seed_offset = 0; step_idx < num_steps; ++step_idx) {
        for (u64 i = 0; i < step_size; ++i, ++seed_offset) {
            auto pre_mk_jlt = cclock::now();
            auto jlt = jlt_maker->mk_jlt(jlt_seed_path, source_dimension, target_dimension, seed_offset);
            auto post_mk_jlt = cclock::now();
            for (u64 j = 0; j < num_data_samples_per_jlt; ++j) {
                input_t input;
                gen->gen_vector(input);
                double norm_pre = norm_squared(input);
                auto pre_embed = cclock::now();
                auto embedded = jlt->transform(input);
                auto post_embed = cclock::now();
                double norm_post = norm_squared(embedded);
                double eps = compute_distortion(norm_post, norm_pre);
                measurements[eps]++;

                delete_vector(embedded);
                std::chrono::duration<double> embed_time = post_embed - pre_embed;
                embed_time_total += embed_time.count();
            }

            delete jlt;
            std::chrono::duration<double> mk_jlt_time = post_mk_jlt - pre_mk_jlt;
            mk_jlt_time_total += mk_jlt_time.count();
        }
        progress.finish_step();
    }
    progress.end_messages();
    delete gen;

    std::cout << std::setprecision(2) << std::fixed
              << "JLT sample time: " << std::setw(7) << mk_jlt_time_total << " s\n"
              << "embed time:      " << std::setw(7) << embed_time_total << " s" << std::endl;

    for (auto &kv : measurements) {
        output_file << std::setw(distortion_width) << kv.first << " "
                    << std::setw(count_width) << kv.second << "\n";
    }
    output_file << std::flush;
}
