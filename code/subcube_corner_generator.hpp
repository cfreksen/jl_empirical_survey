#pragma once

#include <string>
#include <sstream>
#include <numeric>
#include <assert.h>


class SubcubeCornerGenerator: public Generator<dense_vector>, public Generator<sparse_vector*> {
    const u64 vector_dimension;
    const u32 sparsity;
    u64* positions;
    u64 signs;

    void advance_positions() {
        signs = 0;
        for (u64 i = 0; i < sparsity; ++i) {
            if (i == sparsity - 1 or positions[i] + 1 < positions[i + 1]) {
                positions[i]++;
                std::iota(positions, positions + i, 0);
                break;
            }
        }
        assert(positions[sparsity - 1] < vector_dimension);
    }

public:
    SubcubeCornerGenerator(u64 vector_dimension, u32 sparsity) :
        vector_dimension(vector_dimension),
        sparsity(sparsity),
        signs(0) {
        assert(sparsity > 0);
        assert(sparsity <= 63);
        positions = (u64*) std::calloc(sparsity, sizeof(u64));
        std::iota(positions, positions + sparsity, 0);
    }

    ~SubcubeCornerGenerator() {
        free(positions);
    }

    void gen_vector(dense_vector& res) {
        res = calloc_real(vector_dimension);
        // Using `sparsity - 1` means that last sign is always
        // `0`. This is ok as all JLTs used are linear, and so
        // `JLT(-v) = -JLT(v)`. At the same time `v` and `-v` have the
        // same behaviour (same epsilon) so we do not need to generate
        // them both.
        if (signs >= (u64) 1 << (sparsity - 1)) {
            advance_positions();
        }

        u64 curr_signs = signs++;
        for (u64 i = 0; i < sparsity; ++i, curr_signs >>= 1) {
            res.data[positions[i]] = (curr_signs & 1 ? -1 : 1);
        }
    }

    void gen_vector(sparse_vector*& res) {
        res = new sparse_vector{};
        if (signs >= (u64) 1 << (sparsity - 1)) {
            advance_positions();
        }

        u64 curr_signs = signs++;
        for (u64 i = 0; i < sparsity; ++i, curr_signs >>= 1) {
            res->push_back({positions[i], curr_signs & 1 ? -1 : 1});
        }
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(SubcubeCornerGenerator " << sparsity << ")";
        return ss.str();
    }
};


class FixedCornerGenerator: public Generator<dense_vector> {
    const u64 vector_dimension;
    const u32 sparsity;
public:
    FixedCornerGenerator(u64 vector_dimension, u32 sparsity) :
        vector_dimension(vector_dimension),
        sparsity(sparsity) {
    }

    void gen_vector(dense_vector& res) {
        res = calloc_real(vector_dimension);
        for (u64 i = 0; i < sparsity; ++i) {
            res.data[i] = 1;
        }
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(FixedCornerGenerator " << sparsity << ")";
        return ss.str();
    }

    bool is_constant() const override {
        return true;
    }
};
