#pragma once

#include <string>
#include "types.hpp"
#include "prng.hpp"

class Hasher {
public:
    virtual u32 hash(u64 x) const = 0;
    virtual std::string name() const = 0;

    virtual ~Hasher() {};
};
