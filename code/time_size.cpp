#include <unordered_map>
#include <iostream>
#include <string>
#include <sstream>

#include "types.hpp"
#include "experiments.hpp"
#include "factories.hpp"
#include "parameter_strategies.hpp"

// Global settings
const u64 min_src = 1 << 2;               // 2
const u64 max_src = 1 << 10;              // 17
const u64 measurements_per_dim = 1 << 12; // 12
const u64 warmups_per_dim = measurements_per_dim;
const u64 rounds_of_measurements = 1 << 2; // 2

auto jlt_seed_path = "seeds/2019-01-01.bin";
auto gen_seed_path = "seeds/2019-01-02.bin";

auto output_folder_path = "../experiment_output/";

typedef SimpleTabulationHash<> fast_hasher;
typedef Tabulation1PermutationHash<> default_hasher;
typedef FractionalSparsity<1, 4> default_jlt_sparsity;
typedef PaperMiddleDimension default_middle_dimension;

// Default generators
UniformGeneratorFactory<fast_hasher> dense_gen_maker;
ClusterOnesGeneratorFactory sparse_gen_maker;

// Dense embedders supported
EagerRandomProjectionFactory<dense_vector, default_hasher> randproj_dense;
EagerToeplitzJLFactory<default_hasher> toeplitz_dense;
EagerFJLTFactory<default_hasher, default_jlt_sparsity> fjlt_dense;
EagerLWTFactory<default_hasher, default_jlt_sparsity> lwt_dense;
EagerGRHDFactory<default_hasher, default_middle_dimension> grhd_dense;
SparseJLFactory<RawEagerSparseBlockJL<default_hasher, default_jlt_sparsity>, dense_vector> sparse_block_dense;
SparseJLFactory<RawEagerSparseDKSJL<default_hasher, default_jlt_sparsity>, dense_vector> sparse_dks_dense;
EagerFHFactory<dense_vector, default_hasher> fh_eager_dense;
LazyFHFactory<dense_vector, Tabulation1PermutationHash<>> fh_lazy_dense;
LazyFHFactory<dense_vector, SimpleTabulationHash<>> fh_lazy_dense_st;
LazyFHFactory<dense_vector, DoubleTabulationHash<>> fh_lazy_dense_dt;

std::unordered_map<std::string,
                   std::pair<JLTFactory<dense_vector>*, std::string>> dense_embedders
    {{"randproj", {&randproj_dense, "Random Projection"}},
     {"toeplitz", {&toeplitz_dense, "Toeplitz"}},
     {"fjlt", {&fjlt_dense, "FJLT"}},
     {"lwt", {&lwt_dense, "LWT"}},
     {"grhd", {&grhd_dense, "GRHD"}},
     {"sparse_block", {&sparse_block_dense, "SparseJL (block)"}},
     {"fh_eager", {&fh_eager_dense, "Feature hashing (eager)"}},
     {"fh_lazy", {&fh_lazy_dense, "Feature hashing (lazy) (1Permutation)"}},
     {"fh_lazy_st", {&fh_lazy_dense_st, "Feature hashing (lazy) (SimpleTabulation)"}},
     {"fh_lazy_dt", {&fh_lazy_dense_dt, "Feature hashing (lazy) (DoubleTabulation)"}},
    };

// Sparse embedders supported
SparseJLFactory<RawLazySparseBlockJL<Tabulation1PermutationHash<>, default_jlt_sparsity>, sparse_vector*> sparse_block_sparse;
SparseJLFactory<RawLazySparseBlockJL<SimpleTabulationHash<>, default_jlt_sparsity>, sparse_vector*> sparse_block_sparse_st;
SparseJLFactory<RawLazySparseBlockJL<DoubleTabulationHash<>, default_jlt_sparsity>, sparse_vector*> sparse_block_sparse_dt;
SparseJLFactory<RawLazySparseDKSJL<Tabulation1PermutationHash<>, default_jlt_sparsity>, sparse_vector*> sparse_dks_sparse;
SparseJLFactory<RawLazySparseDKSJL<SimpleTabulationHash<>, default_jlt_sparsity>, sparse_vector*> sparse_dks_sparse_st;
SparseJLFactory<RawLazySparseDKSJL<DoubleTabulationHash<>, default_jlt_sparsity>, sparse_vector*> sparse_dks_sparse_dt;
LazyFHFactory<sparse_vector*, Tabulation1PermutationHash<>> fh_lazy_sparse;
LazyFHFactory<sparse_vector*, SimpleTabulationHash<>> fh_lazy_sparse_st;
LazyFHFactory<sparse_vector*, DoubleTabulationHash<>> fh_lazy_sparse_dt;
LazyFHFactory<sparse_vector*, MultiplyShiftHash<>> fh_lazy_sparse_ms;
LazyFHFactory<sparse_vector*, StrongMultiplyShiftHash<>> fh_lazy_sparse_sms;
EagerRandomProjectionFactory<sparse_vector*, default_hasher> randproj_sparse;

std::unordered_map<std::string,
                   std::pair<JLTFactory<sparse_vector*>*, std::string>> sparse_embedders
    {{"sparse_block", {&sparse_block_sparse, "SparseJL (block) (1Permutation)"}},
     {"sparse_block_st", {&sparse_block_sparse_st, "SparseJL (block) (SimpleTabulation)"}},
     {"sparse_block_dt", {&sparse_block_sparse_dt, "SparseJL (block) (DoubleTabulation)"}},
     {"sparse_dks", {&sparse_dks_sparse, "SparseJL (DKS) (1Permutation)"}},
     {"sparse_dks_st", {&sparse_dks_sparse_st, "SparseJL (DKS) (SimpleTabulation)"}},
     {"sparse_dks_dt", {&sparse_dks_sparse_dt, "SparseJL (DKS) (DoubleTabulation)"}},
     {"fh_lazy_1p", {&fh_lazy_sparse, "Feature hashing (lazy) (1Permutation)"}},
     {"fh_lazy_st", {&fh_lazy_sparse_st, "Feature hashing (lazy) (SimpleTabulation)"}},
     {"fh_lazy_dt", {&fh_lazy_sparse_dt, "Feature hashing (lazy) (DoubleTabulation)"}},
     {"fh_lazy_ms", {&fh_lazy_sparse_ms, "Feature hashing (lazy) (MultiplyShift)"}},
     {"fh_lazy_sms", {&fh_lazy_sparse_sms, "Feature hashing (lazy) (Strong MultiplyShift)"}},
     {"randproj", {&randproj_sparse, "Random Projection"}}
    };


template<class input_t>
void do_experiment(std::string short_name,
                   GeneratorFactory<input_t>* gen_maker, JLTFactory<input_t>* jlt_maker,
                   std::string label, u32 target_dimension, bool is_input_dense) {
    std::stringstream output_file_path;
    output_file_path << output_folder_path << "time_"
                     << (is_input_dense ? "dense" : "sparse") << "_"
                     << std::setfill('0')
                     << std::setw(5)
                     << target_dimension << "_"
                     << short_name
                     << ".txt";


    std::cout << "Running experiment with embedder '" << short_name
              << "', writing results to: " << output_file_path.str() << std::endl;
    time_varying_input_size<input_t>(jlt_maker, gen_maker,
                                     jlt_seed_path, gen_seed_path,
                                     output_file_path.str(),
                                     target_dimension, min_src, max_src,
                                     measurements_per_dim, warmups_per_dim,
                                     rounds_of_measurements,
                                     label);
}

void print_help(std::string program_name) {
    std::cerr << "Usage: " << program_name << " [OPTION]... INPUT_TYPE TARGET_DIM EMBEDDER...\n"
              << "\tArguments:\n"
              << "\t INPUT_TYPE       The data type of the generated vectors, either 'dense' or 'sparse'\n"
              << "\t TARGET_DIM       The target dimension that the JLT embeds down to\n"
              << "\t EMBEDDER         One or more embedders, or the string 'all' to run experiments for all embedders\n"
              << "\n"
              << "\tOptions:\n"
              << "\t -h, --help       Display this help message and exit\n"
              << "\n"
              << "\tKnown dense embedder names:\n"
              << "\t";
    for (auto& kv: dense_embedders) {
        std::cerr << " " << kv.first;
    }
    std::cerr << std::endl;
    std::cerr << "\tKnown sparse embedder names:\n"
              << "\t";
    for (auto& kv: sparse_embedders) {
        std::cerr << " " << kv.first;
    }
    std::cerr << std::endl;
}

int main(int argc, char** argv) {
    if (argc == 0) {
        std::cerr << "Error: Too few arguments" << std::endl;
        print_help("program_name");
        return 1;
    }
    if (argc < 4) {
        std::cerr << "Error: Too few arguments" << std::endl;
        print_help(argv[0]);
        return 1;
    }

    std::string data_type = argv[1];
    bool is_input_dense = (data_type == "dense");
    if (data_type != "dense" and data_type != "sparse") {
        std::cerr << "Error: Unknown input data type: " << data_type << std::endl
                  << " Known input data types: dense sparse." << std::endl;
        return 1;
    }
    const u32 target_dimension = std::stoi(argv[2]);

    for (int argi = 3; argi < argc; ++argi) {
        std::string arg = argv[argi];
        if (arg == "--help" or arg == "-h") {
            print_help(argv[0]);
            return 0;
        }
        if (arg == "all") {
            if (argi > 3) {
                std::cerr << "Warning: argument 'all' is not the only argument. Other arguments are redundant." << std::endl;
            }
            if (is_input_dense) {
                std::cout << "Doing running time experiments for all dense embedders" << std::endl;
                for (auto& kv: dense_embedders) {
                    do_experiment<dense_vector>(kv.first, &dense_gen_maker, kv.second.first, kv.second.second, target_dimension, is_input_dense);
                }
            } else {
                std::cout << "Doing running time experiments for all sparse embedders" << std::endl;
                for (auto& kv: sparse_embedders) {
                    do_experiment<sparse_vector*>(kv.first, &sparse_gen_maker, kv.second.first, kv.second.second, target_dimension, is_input_dense);
                }
            }
            break;
        } else {
            if (is_input_dense) {
                auto jlt_maker_it = dense_embedders.find(arg);
                if (jlt_maker_it == dense_embedders.end()) {
                    std::cerr << "Error: unknown dense embedder " << argv[argi] << std::endl;
                    std::cerr << " Known dense embedders are:" << std::endl;
                    for (auto& kv: dense_embedders) {
                        std::cerr << " " << kv.first;
                    }
                    std::cerr << std::endl;
                    return 1;
                } else {
                    do_experiment<dense_vector>(arg, &dense_gen_maker, jlt_maker_it->second.first, jlt_maker_it->second.second, target_dimension, is_input_dense);
                }
            } else {
                auto jlt_maker_it = sparse_embedders.find(arg);
                if (jlt_maker_it == sparse_embedders.end()) {
                    std::cerr << "Error: unknown sparse embedder " << argv[argi] << std::endl;
                    std::cerr << " Known sparse embedders are:" << std::endl;
                    for (auto& kv: sparse_embedders) {
                        std::cerr << " " << kv.first;
                    }
                    std::cerr << std::endl;
                    return 1;
                } else {
                    do_experiment<sparse_vector*>(arg, &sparse_gen_maker, jlt_maker_it->second.first, jlt_maker_it->second.second, target_dimension, is_input_dense);
                }
            }
        }
    }
}
