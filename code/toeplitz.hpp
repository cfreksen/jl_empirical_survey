#pragma once

#include <string>
#include <sstream>
#include "types.hpp"
#include "transformer.hpp"
#include "raw_toeplitz.hpp"


template<class hasher_t>
class EagerToeplitzJL: public Transformer<dense_vector, dense_vector> {
    const RawEagerToeplitz<hasher_t> jlt;

public:
    using prng_t = typename hasher_t::prng_t;

    EagerToeplitzJL(u64 source_dimension, u32 target_dimension, prng_t* sign_prng) :
        jlt(RawEagerToeplitz<hasher_t>{source_dimension, target_dimension, sign_prng}) {
    }

    ~EagerToeplitzJL() {
    }

    dense_vector transform(dense_vector x) const {
        auto res = jlt.transform(x);
        divide_vector(res, jlt.normalisation_divisor());
        return res;
    }

    u64 output_dimension() const {
        return jlt.output_dimension();
    }

    double normalisation_divisor() const {
        return 1;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(ToeplitzJL " << jlt.name() << ")";
        return ss.str();
    }
};
