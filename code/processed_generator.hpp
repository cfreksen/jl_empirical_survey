#pragma once

#include <string>
#include <sstream>
#include <gsl/gsl_vector.h>
#include "types.hpp"
#include "generator.hpp"
#include "transformer.hpp"

template<class vector_type>
class ProcessedGenerator: public Generator<vector_type> {
    // TODO: Consider if vector_dimension is needed, or can be
    // computed from processor->output_dimension().
    const u64 vector_dimension;
    Generator<vector_type>* primordial_generator;
    const Transformer<vector_type, vector_type>* processor;

public:
    ProcessedGenerator(u64 vector_dimension, Generator<vector_type>* primordial_generator,
                       Transformer<vector_type, vector_type>* processor) :
        vector_dimension(vector_dimension),
        primordial_generator(primordial_generator),
        processor(processor) {
    }

    ~ProcessedGenerator() {
        delete processor;
        delete primordial_generator;
    }

    void gen_vector(vector_type& res) {
        vector_type pre;
        primordial_generator->gen_vector(pre);
        res = processor->transform(pre);
        divide_vector(res, processor->normalisation_divisor());
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(ProcessedGenerator " << vector_dimension
           << " " << primordial_generator->name()
           << " " << processor->name() << ")";
        return ss.str();
    }
};
