#pragma once

#include <string>
#include <sstream>
#include <assert.h>
#include "types.hpp"
#include "generator.hpp"
#include "random_sign_generator.hpp"
#include "util.hpp"


template<class hasher_t>
class ClusterGenerator: public Generator<dense_vector>, public Generator<sparse_vector*> {
    const u64 vector_dimension;
    const u64 num_entries;
    const hasher_t pos_hash;
    HashingSignGenerator<hasher_t> sign_gen;
    u64 vector_count;

public:
    using prng_t = typename hasher_t::prng_t;

    ClusterGenerator(u64 vector_dimension, u64 num_entries, prng_t* pos_prng, prng_t* sign_prng) :
        vector_dimension(vector_dimension),
        num_entries(num_entries),
        pos_hash(hasher_t{pos_prng}),
        sign_gen(HashingSignGenerator<hasher_t>{sign_prng}),
        vector_count(0) {
        assert(num_entries <= vector_dimension);
        // We rely on small vector dimension to generate random
        // start_idx more easily
        assert(vector_dimension < (u64) 1 << 32);
    }

    ~ClusterGenerator() {
    }

    void gen_vector(sparse_vector*& res) {
        u64 start_idx = distribute(pos_hash.hash(++vector_count), vector_dimension - num_entries);
        res = new sparse_vector{};
        for (u64 i = 0; i < num_entries; ++i) {
            res->push_back(std::make_pair(start_idx + i, sign_gen.next_sign()));
        }

        start_idx += num_entries;
    }

    void gen_vector(dense_vector& res) {
        u64 start_idx = distribute(pos_hash.hash(++vector_count), vector_dimension - num_entries);
        res = calloc_real(vector_dimension);
        for (u64 i = 0; i < num_entries; ++i) {
            res.data[start_idx + i] = sign_gen.next_sign();
        }

        start_idx += num_entries;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(ClusterGenerator " << num_entries << " " << sign_gen.name() << ")";
        return ss.str();
    }
};

class ClusterOnesGenerator: public Generator<dense_vector>, public Generator<sparse_vector*> {
    const u64 vector_dimension;
    const u64 num_entries;
    u64 start_idx;

public:
    ClusterOnesGenerator(u64 vector_dimension, u64 num_entries) :
        vector_dimension(vector_dimension), num_entries(num_entries) {
        start_idx = 0;
        assert(num_entries <= vector_dimension);
    }

    void gen_vector(sparse_vector*& res) {
        assert(start_idx + num_entries <= vector_dimension);

        res = new sparse_vector{};
        for (u64 i = 0; i < num_entries; ++i) {
            res->push_back(std::make_pair(start_idx + i, 1.0));
        }

        start_idx += num_entries;
    }

    void gen_vector(dense_vector& res) {
        assert(start_idx + num_entries <= vector_dimension);

        res = calloc_real(vector_dimension);
        for (u64 i = 0; i < num_entries; ++i) {
            res.data[start_idx + i] = 1.0;
        }

        start_idx += num_entries;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(ClusterOnesGenerator " << num_entries << ")";
        return ss.str();
    }
};
