#pragma once

#include "types.hpp"
#include <string>


template<class vector_type>
class Generator {
public:
    virtual void gen_vector(vector_type& result) = 0;
    virtual std::string name() const = 0;
    virtual ~Generator() {};
    virtual bool is_constant() const {
        return false;
    }
};

template<class vector_type>
class GeneratorFactory {
public:
    virtual Generator<vector_type>* mk_gen(const std::string seed_file_path, u64 vector_size) const = 0;
};
