#pragma once

#include <string>
#include <sstream>
#include "types.hpp"
#include "transformer.hpp"
#include "random_signs.hpp"
#include "hadamard.hpp"
#include "raw_sparse_jl.hpp"


template<class hasher_t, class sparsity_strat_t>
class EagerFJLT: public Transformer<dense_vector, dense_vector> {
    typedef EagerRandomSigns<hasher_t> signs_t;
    typedef RawEagerSparseBlockJL<hasher_t, sparsity_strat_t> sparse_jl_t;

    const u64 source_dimension;
    const u32 target_dimension;
    const signs_t signs;
    const Hadamard hadamard;
    const sparse_jl_t sparse_jl;
    const double internal_normalisation_divisor;

public:
    using prng_t = typename hasher_t::prng_t;

    EagerFJLT(u64 source_dimension, u32 target_dimension, prng_t* sign_prng,
              prng_t* sparse_prng) :
        source_dimension(source_dimension), target_dimension(target_dimension),
        signs(signs_t{source_dimension, sign_prng}),
        hadamard(Hadamard{signs.output_dimension()}),
        sparse_jl(sparse_jl_t{hadamard.output_dimension(), target_dimension, sparse_prng}),
        internal_normalisation_divisor(signs.normalisation_divisor() * hadamard.normalisation_divisor() * sparse_jl.normalisation_divisor()) {
    }

    ~EagerFJLT() {
    }

    dense_vector transform(dense_vector x) const {
        assert(x.size == source_dimension);
        auto post_sign = signs.transform(x);
        auto post_hadamard = hadamard.transform(post_sign);
        auto post_sparse = sparse_jl.transform(post_hadamard);
        divide_vector(post_sparse, internal_normalisation_divisor);
        return post_sparse;
    }

    u64 output_dimension() const {
        return target_dimension;
    }

    double normalisation_divisor() const {
        return 1;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(EagerFJLT"
           << " " << signs.name()
           << " " << sparse_jl.name()
           << ")";
        return ss.str();
    }
};
