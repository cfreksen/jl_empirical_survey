#pragma once

#include <string>
#include <sstream>
#include <cmath>
#include <assert.h>
#include <gsl/gsl_spmatrix.h>
#include <gsl/gsl_spblas.h>
#include <fftw3.h>
#include "types.hpp"
#include "util.hpp"
#include "prng.hpp"
#include "transformer.hpp"


template<class hasher_t, class sparsity_strat_t>
class RawEagerSparseBlockJL: public Transformer<dense_vector, dense_vector> {
    const u64 source_dimension;
    const u32 target_dimension;
    const u32 sparsity;
    gsl_spmatrix* matrix;
    std::string hasher_name;

public:
    using prng_t = typename hasher_t::prng_t;

    RawEagerSparseBlockJL(u64 source_dimension, u32 target_dimension, prng_t* prng) :
        source_dimension(source_dimension),
        target_dimension(target_dimension),
        sparsity(choose_sparsity<sparsity_strat_t>(source_dimension, target_dimension)) {
        assert(sparsity > 0);
        assert(sparsity <= target_dimension);

        gsl_spmatrix* tmp_matrix = gsl_spmatrix_alloc_nzmax(target_dimension, source_dimension,
                                                            sparsity * source_dimension, GSL_SPMATRIX_TRIPLET);

        u32 small_block_length = target_dimension / sparsity;
        u32 large_block_length = small_block_length + 1;
        u32 num_large_blocks = target_dimension % sparsity;
        u32 num_small_blocks = sparsity - num_large_blocks;
        assert(large_block_length < (u64) 1 << 31); // To have a bit for sign in hash() output

        hasher_t hasher{prng};

        for (u64 col = 0; col < source_dimension; ++col) {
            u32 block_offset = 0;
            u32 block_idx = 0;
            for (; block_idx < num_small_blocks; ++block_idx, block_offset += small_block_length) {
                u64 hash = hasher.hash(col * sparsity + block_idx);
                u32 row = block_offset + distribute<31>(hash >> 1, small_block_length);
                i32 sign = 2 * (hash & 1) - 1;
                gsl_spmatrix_set(tmp_matrix, row, col, sign);
            }
            for (; block_idx < sparsity; ++block_idx, block_offset += large_block_length) {
                u64 hash = hasher.hash(col * sparsity + block_idx);
                u32 row = block_offset + distribute<31>(hash >> 1, large_block_length);
                i32 sign = 2 * (hash & 1) - 1;
                gsl_spmatrix_set(tmp_matrix, row, col, sign);
            }

            assert(block_offset == target_dimension);
        }
        // Compress matrix
        matrix = gsl_spmatrix_compress(tmp_matrix, GSL_SPMATRIX_CSC);
        gsl_spmatrix_free(tmp_matrix);
        hasher_name = hasher.name();
    }

    ~RawEagerSparseBlockJL() {
        gsl_spmatrix_free(matrix);
    }

    dense_vector transform(dense_vector x) const {
        assert(x.size == source_dimension);
        dense_vector res = calloc_real(target_dimension);
        {
            gsl_vector_view x_gsl_view = gsl_vector_view_array(x.data, source_dimension);
            gsl_vector_view res_gsl_view = gsl_vector_view_array(res.data, target_dimension);

            // Perform the matrix vector product
            gsl_spblas_dgemv(CblasNoTrans, 1, matrix, &x_gsl_view.vector, 0, &res_gsl_view.vector);
        }

        delete_vector(x);
        return res;
    }

    u64 output_dimension() const {
        return target_dimension;
    }

    double normalisation_divisor() const {
        return std::sqrt(sparsity);
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(RawEagerSparseJL Block " << sparsity << " " << hasher_name << ")";
        return ss.str();
    }
};


template<class hasher_t, class sparsity_strat_t>
class RawLazySparseBlockJL: public Transformer<sparse_vector*, dense_vector> {
    const u64 source_dimension;
    const u32 target_dimension;
    const u32 sparsity;
    const u32 small_block_length;
    const u32 large_block_length;
    const u32 num_small_blocks;
    const hasher_t hasher;

public:
    using prng_t = typename hasher_t::prng_t;

    RawLazySparseBlockJL(u64 source_dimension, u32 target_dimension, prng_t* prng) :
        source_dimension(source_dimension),
        target_dimension(target_dimension),
        sparsity(choose_sparsity<sparsity_strat_t>(source_dimension, target_dimension)),
        small_block_length(target_dimension / sparsity),
        large_block_length(small_block_length + 1),
        num_small_blocks(sparsity - target_dimension % sparsity),
        hasher(hasher_t{prng}) {
        assert(large_block_length < (u64) 1 << 31); // To have a bit for sign in hash() output
        assert(sparsity > 0);
        assert(sparsity <= target_dimension);
    }

    ~RawLazySparseBlockJL() {
    }

    dense_vector transform(sparse_vector* x) const {
        dense_vector res = calloc_real(target_dimension);

        for (auto const &pair : *x) {
            u32 block_offset = 0;
            u32 block_idx = 0;
            for (; block_idx < num_small_blocks; ++block_idx, block_offset += small_block_length) {
                u64 hash = hasher.hash(pair.first * sparsity + block_idx);
                u32 row = block_offset + distribute<31>(hash >> 1, small_block_length);
                i32 sign = 2 * (hash & 1) - 1;
                res.data[row] += sign * pair.second;
            }
            for (; block_idx < sparsity; ++block_idx, block_offset += large_block_length) {
                u64 hash = hasher.hash(pair.first * sparsity + block_idx);
                u32 row = block_offset + distribute<31>(hash >> 1, large_block_length);
                i32 sign = 2 * (hash & 1) - 1;
                res.data[row] += sign * pair.second;
            }
        }
        delete x;
        return res;
    }

    u64 output_dimension() const {
        return target_dimension;
    }

    double normalisation_divisor() const {
        return std::sqrt(sparsity);
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(RawLazySparseJL Block " << sparsity << " " << hasher.name() << ")";
        return ss.str();
    }
};


template<class hasher_t, class sparsity_strat_t>
class RawEagerSparseDKSJL: public Transformer<dense_vector, dense_vector> {
    const u64 source_dimension;
    const u32 target_dimension;
    const u32 sparsity;
    gsl_spmatrix* matrix;
    std::string hasher_name;

public:
    using prng_t = typename hasher_t::prng_t;

    RawEagerSparseDKSJL(u64 source_dimension, u32 target_dimension, prng_t* prng) :
        source_dimension(source_dimension),
        target_dimension(target_dimension),
        sparsity(choose_sparsity<sparsity_strat_t>(source_dimension, target_dimension)) {

        assert(sparsity > 0);
        assert(sparsity <= target_dimension);
        assert(target_dimension < (u64) 1 << 31);

        gsl_spmatrix* tmp_matrix = gsl_spmatrix_alloc_nzmax(target_dimension, source_dimension,
                                                            sparsity * source_dimension, GSL_SPMATRIX_TRIPLET);

        hasher_t hasher{prng};
        for (u64 col = 0; col < source_dimension; ++col) {
            for (u64 i = 0; i < sparsity; ++i) {
                u32 hash = hasher.hash(col * sparsity + i);
                u32 row = distribute<31>(hash >> 1, target_dimension);
                i32 sign = 2 * (hash & 1) - 1;
                sign += gsl_spmatrix_get(tmp_matrix, row, col);
                gsl_spmatrix_set(tmp_matrix, row, col, sign);
            }
        }
        // Compress matrix
        matrix = gsl_spmatrix_compress(tmp_matrix, GSL_SPMATRIX_CSC);
        gsl_spmatrix_free(tmp_matrix);
        hasher_name = hasher.name();
    }

    ~RawEagerSparseDKSJL() {
        gsl_spmatrix_free(matrix);
    }

    dense_vector transform(dense_vector x) const {
        assert(x.size == source_dimension);
        dense_vector res = calloc_real(target_dimension);
        {
            gsl_vector_view x_gsl_view = gsl_vector_view_array(x.data, source_dimension);
            gsl_vector_view res_gsl_view = gsl_vector_view_array(res.data, target_dimension);

            // Perform the matrix vector product
            gsl_spblas_dgemv(CblasNoTrans, 1, matrix, &x_gsl_view.vector, 0, &res_gsl_view.vector);
        }

        delete_vector(x);
        return res;
    }

    u64 output_dimension() const {
        return target_dimension;
    }

    double normalisation_divisor() const {
        return std::sqrt(sparsity);
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(RawEagerSparseJL DKS " << sparsity << " " << hasher_name << ")";
        return ss.str();
    }
};


template<class hasher_t, class sparsity_strat_t>
class RawLazySparseDKSJL: public Transformer<sparse_vector*, dense_vector> {
    const u64 source_dimension;
    const u32 target_dimension;
    const u32 sparsity;
    const hasher_t hasher;

public:
    using prng_t = typename hasher_t::prng_t;

    RawLazySparseDKSJL(u64 source_dimension, u32 target_dimension, prng_t* prng) :
        source_dimension(source_dimension),
        target_dimension(target_dimension),
        sparsity(choose_sparsity<sparsity_strat_t>(source_dimension, target_dimension)),
        hasher(hasher_t{prng}) {
        assert(target_dimension < (u64) 1 << 31);
        assert(sparsity > 0);
        assert(sparsity <= target_dimension);
    }

    ~RawLazySparseDKSJL() {
    }

    dense_vector transform(sparse_vector* x) const {
        dense_vector res = calloc_real(target_dimension);

        for (auto const &pair : *x) {
            for (u32 i = 0; i < sparsity; ++i) {
                u32 hash = hasher.hash(pair.first * sparsity + i);
                u32 row = distribute<31>(hash >> 1, target_dimension);
                i32 sign = 2 * (hash & 1) - 1;
                res.data[row] += sign * pair.second;
            }
        }
        delete x;
        return res;
    }

    u64 output_dimension() const {
        return target_dimension;
    }

    double normalisation_divisor() const {
        return std::sqrt(sparsity);
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(RawLazyJLSparse DKS " << sparsity << " " << hasher.name() << ")";
        return ss.str();
    }
};
