#pragma once

#include <string>
#include "types.hpp"

class DataParser {
public:
    virtual sparse_vector* next_vector() = 0;
    virtual std::string name() const = 0;
    virtual ~DataParser() {};
};
