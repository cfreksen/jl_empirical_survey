#pragma once

#include <complex>
#include <assert.h>
#include <cstring>
#include <string>
#include <sstream>
#include <fftw3.h>
#include "types.hpp"
#include "transformer.hpp"
#include "util.hpp"
#include "random_sign_generator.hpp"


template<class hasher_t>
class RawEagerToeplitz: public Transformer<dense_vector, dense_vector> {
    const u64 source_dimension;
    const u32 target_dimension;
    const u32 total_length;
    const u32 complex_length;
    const u64 num_blocks;

    std::complex<double>** toeplitz_blocks;

    double* const x_extended;
    double* const out_extended;
    fftw_complex* const x_fourier;
    std::complex<double>* const x_complex;

    fftw_plan x_plan;
    fftw_plan out_plan;

    std::string sign_gen_name;

    void block_fft_fast(std::complex<double>* ts_complex, dense_vector_data x,
                        dense_vector_data out) const {
        std::memset(x_extended, 0, total_length * sizeof(double));
        std::memcpy(x_extended, x, target_dimension * sizeof(double));

        fftw_execute(x_plan);

        // TODO: Consider using GSL for vector multiplication and addition
        for (u64 i = 0; i < complex_length; ++i) {
            x_complex[i] *= ts_complex[i];
        }

        fftw_execute(out_plan);

        for (u32 i = 0; i < target_dimension; ++i) {
            out[i] += out_extended[i + target_dimension - 1];
        }
    }

public:
    using prng_t = typename hasher_t::prng_t;

    RawEagerToeplitz(u64 source_dimension, u32 target_dimension, prng_t* sign_prng) :
        source_dimension(source_dimension),
        target_dimension(target_dimension),
        total_length(3 * target_dimension - 1),
        complex_length(total_length / 2 + 1),
        num_blocks(source_dimension / target_dimension),
        x_extended(calloc_real_raw(total_length)),
        out_extended(calloc_real_raw(total_length)),
        x_fourier(fftw_alloc_complex(complex_length)),
        x_complex(reinterpret_cast<std::complex<double>*>(x_fourier)) {

        assert(source_dimension % target_dimension == 0);

        x_plan = fftw_plan_dft_r2c_1d(total_length, x_extended, x_fourier, FFTW_MEASURE | FFTW_DESTROY_INPUT);
        out_plan = fftw_plan_dft_c2r_1d(total_length, x_fourier, out_extended, FFTW_MEASURE);

        // Generate Toeplitz
        double* ts_extended = calloc_real_raw(total_length);
        fftw_complex* ts_fourier = fftw_alloc_complex(complex_length);
        fftw_plan ts_plan = fftw_plan_dft_r2c_1d(total_length, ts_extended, ts_fourier, FFTW_MEASURE | FFTW_PRESERVE_INPUT);

        toeplitz_blocks = (std::complex<double>**) std::calloc(num_blocks, sizeof(std::complex<double>*));

        // generate some initial data into ts_extended. In a sense this is
        // out of bounds, but will be moved in bounds shortly
        HashingSignGenerator<hasher_t> sign_gen{sign_prng};
        for (u32 i = 0; i < target_dimension - 1; ++i) {
            ts_extended[i] = sign_gen.next_sign();
        }

        for (u64 block_idx = 0; block_idx < num_blocks; ++block_idx) {
            // move data overlapping with previous block. The source is at
            // the end of the array. Note that the destinations do not
            // overlap.
            std::memcpy(ts_extended + target_dimension, ts_extended, (target_dimension - 1) * sizeof(double));

            // Generate new data
            for (u32 i = 0; i < target_dimension; ++i) {
                ts_extended[i] = sign_gen.next_sign();
            }

            // FFT the Toeplitz data
            fftw_execute(ts_plan);

            // Move FFT Toeplitz data into toeplitz_blocks
            fftw_complex* new_dst = fftw_alloc_complex(complex_length);
            std::memcpy(new_dst, ts_fourier, complex_length * sizeof(fftw_complex));
            toeplitz_blocks[block_idx] = reinterpret_cast<std::complex<double>*>(new_dst);
        }

        // Clean up plans only needed for generating toeplitz_blocks
        fftw_destroy_plan(ts_plan);
        fftw_free(ts_fourier);
        fftw_free(ts_extended);

        sign_gen_name = sign_gen.name();
    }

    ~RawEagerToeplitz() {
        fftw_destroy_plan(x_plan);
        fftw_destroy_plan(out_plan);
        fftw_free(x_extended);
        fftw_free(out_extended);
        fftw_free(x_fourier);

        for (u64 block_idx = 0; block_idx < num_blocks; ++block_idx) {
            fftw_free(toeplitz_blocks[block_idx]);
        }
        fftw_free(toeplitz_blocks);
    }

    dense_vector transform(dense_vector x) const {
        assert(x.size == source_dimension);
        dense_vector res = calloc_real(target_dimension);

        for (u64 block_idx = 0; block_idx < num_blocks; ++block_idx) {
            block_fft_fast(toeplitz_blocks[block_idx],
                           x.data + block_idx * target_dimension, res.data);
        }

        delete_vector(x);
        return res;
    }

    u64 output_dimension() const {
        return target_dimension;
    }

    double normalisation_divisor() const {
        return total_length * std::sqrt(target_dimension);
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(RawEagerToeplitz " << sign_gen_name << ")";
        return ss.str();
    }
};
