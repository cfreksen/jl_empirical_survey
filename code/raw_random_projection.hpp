#pragma once

#include <string>
#include <sstream>
#include <cmath>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <fftw3.h>
#include "types.hpp"
#include "transformer.hpp"
#include "util.hpp"
#include "random_sign_generator.hpp"


template<class hasher_t>
class RawEagerRandomProjection: public Transformer<dense_vector, dense_vector>, public Transformer<sparse_vector*, dense_vector> {
    const u64 source_dimension;
    const u32 target_dimension;
    gsl_matrix* const matrix;
    std::string sign_gen_name;

public:
    using prng_t = typename hasher_t::prng_t;

    RawEagerRandomProjection(u64 source_dimension, u32 target_dimension, prng_t* sign_prng) :
        source_dimension(source_dimension),
        target_dimension(target_dimension),
        matrix(gsl_matrix_alloc(target_dimension, source_dimension)) {
        HashingSignGenerator<hasher_t> sign_gen{sign_prng};
        for (u32 row = 0; row < target_dimension; ++row) {
            for (u64 col = 0; col < source_dimension; ++col) {
                gsl_matrix_set(matrix, row, col, sign_gen.next_sign());
            }
        }
        sign_gen_name = sign_gen.name();
    }

    ~RawEagerRandomProjection() {
        gsl_matrix_free(matrix);
    }

    dense_vector transform(dense_vector x) const {
        assert(x.size == source_dimension);
        dense_vector res = calloc_real(target_dimension);
        {
            gsl_vector_view x_gsl_view = gsl_vector_view_array(x.data, source_dimension);
            gsl_vector_view res_gsl_view = gsl_vector_view_array(res.data, target_dimension);

            // Perform the matrix vector product
            gsl_blas_dgemv(CblasNoTrans, 1, matrix, &x_gsl_view.vector, 0, &res_gsl_view.vector);
        }

        delete_vector(x);

        return res;
    }

    dense_vector transform(sparse_vector* x) const {
        dense_vector res = calloc_real(target_dimension);
        gsl_vector_view res_gsl_view = gsl_vector_view_array(res.data, target_dimension);
        for (auto const &pair : *x) {
            assert(pair.first < source_dimension);
            gsl_vector_const_view column_gsl_view = gsl_matrix_const_column(matrix, pair.first);
            gsl_vector_add(&res_gsl_view.vector, &column_gsl_view.vector);
        }
        delete x;

        return res;
    }

    u64 output_dimension() const {
        return target_dimension;
    }

    double normalisation_divisor() const {
        return std::sqrt(target_dimension);
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(RawEagerRandomProjection " << sign_gen_name << ")";
        return ss.str();
    }
};
