#pragma once

#include <string>
#include <sstream>
#include <cstring>
#include <cmath>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <fftw3.h>
#include "types.hpp"
#include "transformer.hpp"
#include "util.hpp"


class RawEagerLeanWalshTransform: public Transformer<dense_vector, dense_vector> {
    const u64 source_dimension;

    gsl_matrix* const seed_matrix;
    const u64 num_seed_rows;
    const u64 num_seed_cols;

    const u64 l_init;
    const u64 padding;
    const u32 target_dimension;

    void recurse(u64 l, dense_vector_data z, dense_vector_data output) const {
        if (l == 1) {
            gsl_vector_view z_view = gsl_vector_view_array(z, num_seed_cols);
            gsl_vector_view output_view = gsl_vector_view_array(output, num_seed_rows);

            // Do normal matrix vector product with seed matrix
            gsl_blas_dgemv(CblasNoTrans, 1, seed_matrix, &z_view.vector, 0, &output_view.vector);
            return;
        }

        u64 block_rows = std::pow(num_seed_rows, l-1);
        u64 block_cols = std::pow(num_seed_cols, l-1);
        for (u64 r_idx = 0; r_idx < num_seed_rows; ++r_idx) {
            dense_vector_data curr_output = output + r_idx * block_rows;

            dense_vector_data new_z = calloc_real_raw(block_cols);
            gsl_vector_view new_z_view = gsl_vector_view_array(new_z, block_cols);

            for (u64 c_idx = 0; c_idx < num_seed_cols; ++c_idx) {
                double factor = gsl_matrix_get(seed_matrix, r_idx, c_idx);
                gsl_vector_view input_z_block_view = gsl_vector_view_array(z + c_idx * block_cols, block_cols);
                // new_z += factor * input_z_block
                gsl_blas_daxpy(factor, &input_z_block_view.vector, &new_z_view.vector);
            }

            recurse(l - 1, new_z, curr_output);
            fftw_free(new_z);
        }
    }

public:
    RawEagerLeanWalshTransform(u64 source_dimension, gsl_matrix* seed_matrix) :
        source_dimension(source_dimension),
        seed_matrix(seed_matrix),
        num_seed_rows(seed_matrix->size1),
        num_seed_cols(seed_matrix->size2),
        l_init((u64) std::ceil(std::log2(source_dimension) / std::log2(num_seed_cols))),
        padding(std::pow(num_seed_cols, l_init) - source_dimension),
        target_dimension(std::pow(num_seed_rows, l_init)) {
    }

    ~RawEagerLeanWalshTransform() {
        gsl_matrix_free(seed_matrix);
    }

    dense_vector transform(dense_vector x) const {
        assert(x.size == source_dimension);
        dense_vector padded_x;
        if (padding == 0) {
            padded_x = x;
        } else {
            padded_x = calloc_real(source_dimension + padding);
            std::memcpy(padded_x.data, x.data, source_dimension * sizeof(double));
            delete_vector(x);
        }
        dense_vector res = calloc_real(target_dimension);
        recurse(l_init, padded_x.data, res.data);
        delete_vector(padded_x);
        return res;
    }

    u64 output_dimension() const {
        return target_dimension;
    }

    double normalisation_divisor() const {
        return std::pow(num_seed_rows, l_init / 2.0);
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(RawEagerLeanWalshTransform " << num_seed_rows
           << " " << num_seed_cols << ")";
        return ss.str();
    }
};
