#pragma once

#include <string>
#include <sstream>
#include <cstring>
#include <fftw3.h>
#include <gsl/gsl_vector.h>
#include "types.hpp"
#include "generator.hpp"
#include "hasher.hpp"
#include "prng.hpp"
#include "util.hpp"


template<class hasher_t>
class UniformGenerator: public Generator<dense_vector> {
    const u64 vector_dimension;
    const hasher_t hasher;
    u64 hash_index;

    double next_double() {
        double raw = (double) hasher.hash(hash_index++);
        double max = (double) ((u64) 1 << 32) - 1;
        return 2 * raw / max - 1.0;
    }

public:
    using prng_t = typename hasher_t::prng_t;

    UniformGenerator(u64 vector_dimension, prng_t* prng) :
        vector_dimension(vector_dimension),
        hasher(hasher_t{prng}),
        hash_index(0) {
    }

    ~UniformGenerator() {
    }

    void gen_vector(dense_vector& res) {
        res = calloc_real(vector_dimension);
        for (u64 i = 0; i < vector_dimension; ++i) {
            res.data[i] = next_double();
        }
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(UniformGenerator " << hasher.name() << ")";
        return ss.str();
    }
};


template<class hasher_t>
class PerturbedUniformGenerator: public Generator<dense_vector> {
    const u64 vector_dimension;
    const u64 num_centers;
    u64 center_idx;
    const double l1_radius;
    UniformGenerator<hasher_t> ug;
    dense_vector* const centers;

public:
    using prng_t = typename hasher_t::prng_t;

    PerturbedUniformGenerator(u64 vector_dimension, u64 num_centers, double l1_radius,
                              prng_t* prng) :
        vector_dimension(vector_dimension),
        num_centers(num_centers),
        center_idx(0),
        l1_radius(l1_radius),
        ug(UniformGenerator<hasher_t>{vector_dimension, prng}),
        centers((dense_vector*) std::calloc(num_centers, sizeof(dense_vector))) {
        for (u64 i = 0; i < num_centers; ++i) {
            ug.gen_vector(centers[i]);
        }
    }

    ~PerturbedUniformGenerator() {
        for (u64 i = 0; i < num_centers; ++i) {
            delete_vector(centers[i]);
        }
        free(centers);
    }

    void gen_vector(dense_vector& res) {
        ug.gen_vector(res);
        auto res_view = gsl_vector_view_array(res.data, vector_dimension);
        auto center_view = gsl_vector_view_array(centers[center_idx].data, vector_dimension);
        gsl_vector_scale(&res_view.vector, l1_radius);
        gsl_vector_add(&res_view.vector, &center_view.vector);

        center_idx = (center_idx + 1) % num_centers;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(PerturbedUniformGenerator " << num_centers
           << " " << l1_radius
           << " " << ug.name() << ")";
        return ss.str();
    }
};
