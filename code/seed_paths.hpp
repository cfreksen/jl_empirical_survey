#pragma once

#include <string>

const std::vector<std::string> seed_paths{"seeds/2018-01.bin", "seeds/2018-02.bin", "seeds/2018-03.bin", "seeds/2018-04.bin", "seeds/2018-05.bin", "seeds/2018-06.bin", "seeds/2018-07.bin", "seeds/2018-08.bin", "seeds/2018-09.bin", "seeds/2018-10.bin", "seeds/2018-11.bin", "seeds/2018-12.bin",
                                          "seeds/2017-01.bin", "seeds/2017-02.bin", "seeds/2017-03.bin", "seeds/2017-04.bin", "seeds/2017-05.bin", "seeds/2017-06.bin", "seeds/2017-07.bin", "seeds/2017-08.bin", "seeds/2017-09.bin", "seeds/2017-10.bin", "seeds/2017-11.bin", "seeds/2017-12.bin",
                                          "seeds/2016-01.bin", "seeds/2016-02.bin", "seeds/2016-03.bin", "seeds/2016-04.bin", "seeds/2016-05.bin", "seeds/2016-06.bin", "seeds/2016-07.bin", "seeds/2016-08.bin", "seeds/2016-09.bin", "seeds/2016-10.bin", "seeds/2016-11.bin", "seeds/2016-12.bin",
};
