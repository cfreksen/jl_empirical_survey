#pragma once

#include <assert.h>

#include "types.hpp"
#include "prng.hpp"

#include "hasher.hpp"
#include "double_tabulation_hash.hpp"
#include "simple_tabulation_hash.hpp"
#include "tabulation_permutation_hash.hpp"
#include "multiply_shift_hash.hpp"

#include "transformer.hpp"
#include "fjlt.hpp"
#include "lwt.hpp"
#include "sparse_jl.hpp"
#include "random_projection.hpp"
#include "toeplitz.hpp"
#include "grhd.hpp"
#include "raw_feature_hash.hpp"
#include "raw_sparse_jl.hpp"

#include "generator.hpp"
#include "uniform_generator.hpp"
#include "cluster_generator.hpp"
#include "subcube_corner_generator.hpp"


// JLT factories

template<class input_t, class hasher_t>
class EagerRandomProjectionFactory: public JLTFactory<input_t> {
public:
    Transformer<input_t, dense_vector>* mk_jlt(const std::string seed_file_path, u64 source_dimension, u32 target_dimension, u32 seed_offset) const {
        using prng_t = typename hasher_t::prng_t;
        auto prng = new prng_t{seed_file_path, seed_offset};
        auto jlt = new EagerRandomProjection<hasher_t>{source_dimension, target_dimension, prng};
        return jlt;
    }
};

template<class hasher_t>
class EagerToeplitzJLFactory: public JLTFactory<dense_vector> {
public:
    Transformer<dense_vector, dense_vector>* mk_jlt(const std::string seed_file_path, u64 source_dimension, u32 target_dimension, u32 seed_offset) const {
        using prng_t = typename hasher_t::prng_t;
        auto prng = new prng_t{seed_file_path, seed_offset};
        auto jlt = new EagerToeplitzJL<hasher_t>{source_dimension, target_dimension, prng};
        return jlt;
    }
};

template<class hasher_t, class sparsity_strat_t>
class EagerFJLTFactory: public JLTFactory<dense_vector> {
public:
    Transformer<dense_vector, dense_vector>* mk_jlt(const std::string seed_file_path, u64 source_dimension, u32 target_dimension, u32 seed_offset) const {
        assert(target_dimension <= source_dimension);
        using prng_t = typename hasher_t::prng_t;
        auto sign_prng = new prng_t{seed_file_path, 2 * seed_offset};
        auto sparse_prng = new prng_t{seed_file_path, 2 * seed_offset + 1};
        auto jlt = new EagerFJLT<hasher_t, sparsity_strat_t>{source_dimension, target_dimension,
                                                                     sign_prng, sparse_prng};
        return jlt;
    }
};

template<class hasher_t, class sparsity_strat_t, u32 seed_rows, u32 seed_cols>
class EagerLWTFactory: public JLTFactory<dense_vector> {
public:
    Transformer<dense_vector, dense_vector>* mk_jlt(const std::string seed_file_path, u64 source_dimension, u32 target_dimension, u32 seed_offset) const {
        using prng_t = typename hasher_t::prng_t;
        auto seed_matrix = generate_subhadamard(seed_rows, seed_cols);
        auto sign_prng = new prng_t{seed_file_path, 3 * seed_offset};
        auto fjlt_sign_prng = new prng_t{seed_file_path, 3 * seed_offset + 1};
        auto fjlt_sparse_prng = new prng_t{seed_file_path, 3 * seed_offset + 2};
        auto jlt = new EagerLeanWalshTransform<hasher_t, sparsity_strat_t>{source_dimension, target_dimension,
                                                                           seed_matrix, sign_prng, fjlt_sign_prng,
                                                                           fjlt_sparse_prng};
        return jlt;
    }
};

template<class hasher_t, class middim_strat_t>
class EagerGRHDFactory: public JLTFactory<dense_vector> {
public:
    Transformer<dense_vector, dense_vector>* mk_jlt(const std::string seed_file_path, u64 source_dimension, u32 target_dimension, u32 seed_offset) const {
        using prng_t = typename hasher_t::prng_t;
        auto sign_prng = new prng_t{seed_file_path, 3 * seed_offset};
        auto subsample_prng = new prng_t{seed_file_path, 3 * seed_offset + 1};
        auto last_prng = new prng_t{seed_file_path, 3 * seed_offset + 2};
        auto jlt = new EagerGRHD<hasher_t, middim_strat_t>{source_dimension, target_dimension, sign_prng, subsample_prng, last_prng};
        return jlt;
    }
};

template<class raw_jlt_t, class input_t>
class SparseJLFactory: public JLTFactory<input_t> {
public:
    Transformer<input_t, dense_vector>* mk_jlt(const std::string seed_file_path, u64 source_dimension, u32 target_dimension, u32 seed_offset) const {
        using prng_t = typename raw_jlt_t::prng_t;
        auto prng = new prng_t{seed_file_path, seed_offset};
        auto jlt = new SparseJL<raw_jlt_t, input_t>{source_dimension, target_dimension, prng};
        return jlt;
    }
};

template<class input_t, class hasher_t>
class EagerFHFactory: public JLTFactory<input_t> {
public:
    Transformer<input_t, dense_vector>* mk_jlt(const std::string seed_file_path, u64 source_dimension, u32 target_dimension, u32 seed_offset) const {
        using prng_t = typename hasher_t::prng_t;
        auto fh_prng = new prng_t{seed_file_path, seed_offset};
        auto jlt = new RawFeatureHashEager<hasher_t>{source_dimension, target_dimension,
                                                     fh_prng};
        return jlt;
    }
};

template<class input_t, class hasher_t>
class LazyFHFactory: public JLTFactory<input_t> {
public:
    Transformer<input_t, dense_vector>* mk_jlt(const std::string seed_file_path, u64 source_dimension, u32 target_dimension, u32 seed_offset) const {
        using prng_t = typename hasher_t::prng_t;
        auto fh_prng = new prng_t{seed_file_path, seed_offset};
        auto jlt = new RawFeatureHashLazy<hasher_t>{source_dimension, target_dimension,
                                                    fh_prng};
        return jlt;
    }
};


// Generator factories

template<class hasher_t>
class UniformGeneratorFactory: public GeneratorFactory<dense_vector> {
public:
    Generator<dense_vector>* mk_gen(const std::string seed_file_path, u64 dimension) const {
        using prng_t = typename hasher_t::prng_t;
        auto prng = new prng_t{seed_file_path, 0};
        return new UniformGenerator<hasher_t>{dimension, prng};
    }
};

class ClusterOnesGeneratorFactory: public GeneratorFactory<sparse_vector*> {
public:
    Generator<sparse_vector*>* mk_gen(const std::string, u64 sparsity) const {
        u64 dummy_dimension = (u64) 1 << 63;
        return new ClusterOnesGenerator{dummy_dimension, sparsity};
    }
};


template<u32 sparsity, class hasher_t>
class ClusterGeneratorDenseFactory: public GeneratorFactory<dense_vector> {
public:
    Generator<dense_vector>* mk_gen(const std::string seed_file_path, u64 vector_dimension) const {
        using prng_t = typename hasher_t::prng_t;
        auto pos_prng = new prng_t{seed_file_path, 0};
        auto sign_prng = new prng_t{seed_file_path, 1};
        return new ClusterGenerator<hasher_t>{vector_dimension, sparsity, pos_prng, sign_prng};
    }
};

template<u32 sparsity>
class SubcubeCornerGeneratorDenseFactory: public GeneratorFactory<dense_vector> {
public:
    Generator<dense_vector>* mk_gen(const std::string, u64 dimension) const {
        return new SubcubeCornerGenerator{dimension, sparsity};
    }
};

template<u32 sparsity>
class FixedCornerGeneratorFactory: public GeneratorFactory<dense_vector> {
public:
    Generator<dense_vector>* mk_gen(const std::string, u64 dimension) const {
        return new FixedCornerGenerator{dimension, sparsity};
    }
};
