#pragma once

#include <string>
#include <sstream>
#include <cmath>
#include "types.hpp"
#include "hasher.hpp"
#include "util.hpp"

class RandomSignGenerator {
public:
    virtual void set_to_block_idx(u64 block_idx) = 0;
    virtual i8 next_sign() = 0;
    virtual std::string name() const = 0;
    virtual ~RandomSignGenerator() {};
};


template<class hasher_t>
class HashingSignGenerator: public RandomSignGenerator {
    const u64 log_block_length;
    const hasher_t hasher;

    void refill_sign_buffer() {
        signs_left = sign_buffer_size;
        sign_buffer = hasher.hash((block_idx << log_block_length) + small_idx);
        ++small_idx;
    }

    u64 block_idx;
    u64 small_idx;

    const u32 sign_buffer_size = 32;
    u32 sign_buffer;
    u32 signs_left;

public:
    using prng_t = typename hasher_t::prng_t;

    HashingSignGenerator(prng_t* prng, u64 block_length=0) :
        log_block_length(block_length != 0 ? (u64) std::ceil(std::log2(block_length)) : 63),
        hasher(hasher_t{prng}),
        block_idx(0),
        small_idx(0),
        signs_left(0) {
    }

    ~HashingSignGenerator() {
    }

    i8 next_sign() {
        if (signs_left == 0) {
            refill_sign_buffer();
        }

        i32 sign = sign_buffer & 1;
        sign_buffer >>= 1;
        --signs_left;

        return 2 * sign - 1;
    }

    void set_to_block_idx(u64 _block_idx) {
        block_idx = _block_idx;
        small_idx = 0;
        refill_sign_buffer();
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(HashingSignGenerator " << hasher.name() << ")";
        return ss.str();
    }
};
