#include <type_traits>
#include <string>
#include <iostream>
#include <vector>
#include <unordered_map>
#include <cstdlib>

#include <boost/program_options.hpp>

#include "types.hpp"
#include "experiments.hpp"
#include "seed_paths.hpp"
#include "jlt_defs.hpp"

namespace po = boost::program_options;

template<class vector_t>
using embedders_t = std::unordered_map<std::string,
                                       std::tuple<JLTFactory<vector_t>*, std::string, std::string>>;
template<class vector_t>
using generators_t = std::unordered_map<std::string,
                                        std::tuple<GeneratorFactory<vector_t>*, std::string>>;

generators_t<dense_vector> dense_generators
    {{"uniform", {&uniform_dense_gen, "Uniform"}},
     {"sparse2", {&cluster_k2_dense_gen, "Sparse 2"}},
     {"sparse4", {&cluster_k4_dense_gen, "Sparse 4"}},
     {"sparse8", {&cluster_k8_dense_gen, "Sparse 8"}},
     {"sparse16", {&cluster_k16_dense_gen, "Sparse 16"}},
     {"sparse32", {&cluster_k32_dense_gen, "Sparse 32"}},
     {"sparse64", {&cluster_k64_dense_gen, "Sparse 64"}},
     {"sparse128", {&cluster_k128_dense_gen, "Sparse 128"}},
     {"sparse256", {&cluster_k256_dense_gen, "Sparse 256"}},
     {"sparse512", {&cluster_k512_dense_gen, "Sparse 512"}},
    };

embedders_t<dense_vector> dense_embedders
    {{"fh_1p", {&fh_eager_dense_1p, "FH (1Permutation)", seed_paths[1]}},
     {"fh_st", {&fh_eager_dense_st, "FH (SimpleTabulation)", seed_paths[2]}},
     {"fh_dt", {&fh_eager_dense_dt, "FH (DoubleTabulation)", seed_paths[3]}},
     {"fh_ms", {&fh_eager_dense_ms, "FH (MultiplyShift)", seed_paths[4]}},
     {"fh_sms", {&fh_eager_dense_sms, "FH (StrongMultiplyShift)", seed_paths[5]}}
    };

generators_t<sparse_vector*> sparse_generators
    {
    };

embedders_t<sparse_vector*> sparse_embedders
    {
    };

std::unordered_map<std::string, std::vector<std::string>> embedder_sets
    {{"fh_hash", {"fh_1p", "fh_st", "fh_dt", "fh_ms", "fh_sms"}}
    };

template<class T> T get_arg(std::string, po::variables_map);

template<class vector_t>
void handle_args(po::variables_map vm,
                 generators_t<vector_t> input_generators, embedders_t<vector_t> embedders) {
    bool is_input_dense = std::is_same<vector_t, dense_vector>::value;
    auto input_size = get_arg<u64>("input_size", vm);
    auto target_dimension = get_arg<u32>("target_dimension", vm);
    auto short_gen_name = get_arg<std::string>("generator", vm);
    auto embedder_set_name = get_arg<std::string>("embedder_set", vm);
    auto output_folder = get_arg<std::string>("output_folder", vm);
    auto gen_seed_path = get_arg<std::string>("gen_seed", vm);
    auto num_jlt_samples = get_arg<u64>("jlt_samples", vm);
    auto max_data_samples_per_jlt = get_arg<u64>("data_samples", vm);

    auto gen_maker_it = input_generators.find(short_gen_name);
    if (gen_maker_it == input_generators.end()) {
        std::cerr << "Error: unknown input generator " << short_gen_name << std::endl;
        std::exit(EXIT_FAILURE);
    }
    auto [gen_maker, gen_label] = gen_maker_it->second;

    auto embedder_set_it = embedder_sets.find(embedder_set_name);
    if (embedder_set_it == embedder_sets.end()) {
        std::cerr << "Error: unknown embedder set " << embedder_set_name << std::endl;
        std::exit(EXIT_FAILURE);
    }

    for (auto& short_jlt_name : embedder_set_it->second) {
        auto [jlt_maker, jlt_label, jlt_seed_path] = embedders.at(short_jlt_name);
        std::stringstream output_file_path;
        output_file_path << output_folder << "dist_"
                         << (is_input_dense ? "dense" : "sparse") << "_"
                         << std::setfill('0')
                         << std::setw(5)
                         << input_size << "_"
                         << std::setfill('0')
                         << std::setw(5)
                         << target_dimension << "_"
                         << short_gen_name << "_"
                         << short_jlt_name
                         << ".txt";
        run_distortion_experiment(jlt_maker, gen_maker,
                                  jlt_seed_path, gen_seed_path,
                                  (std::string) output_file_path.str(),
                                  input_size, target_dimension,
                                  num_jlt_samples, max_data_samples_per_jlt,
                                  jlt_label, gen_label, short_gen_name);
    }
}


int main(int argc, char** argv) {
    // TODO: Find good default values for num X samples
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "produce help message")
        ("vector_type,v", po::value<std::string>(), "input vector type, either 'dense' or 'sparse'")
        ("input_size,i", po::value<u64>(), "input size, either source dimension (d) or input vector sparsity (k) depending on the input vector type")
        ("target_dimension,m", po::value<u32>(), "target dimension")
        ("generator,g", po::value<std::string>(), "input generator")
        ("embedder_set,e", po::value<std::string>(), "name of a embedder/JLT set")
        ("output_folder,o", po::value<std::string>()->default_value("../experiment_output/"), "folder to write experiment result files to")
        ("auto,a", po::bool_switch(), "if set, then the program will not ask for users to input missing parameters, but terminate instead")
        ("gen_seed", po::value<std::string>()->default_value(seed_paths[0]), "path to seed file used when generating input vectors")
        ("jlt_samples,j", po::value<u64>()->default_value(1 << 6), "number of JLTs sampled")
        ("data_samples,d", po::value<u64>()->default_value(1 << 16), "number of vectors sampled per JLT")
        ;
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).
              options(desc).run(), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << "Usage: " << argv[0] << " [OPTION...]\n";
        std::cout << desc << std::endl;
        return EXIT_FAILURE;
    }

    auto vector_type = get_arg<std::string>("vector_type", vm);

    if (vector_type == "dense") {
        handle_args<dense_vector>(vm, dense_generators, dense_embedders);
    } else {
        handle_args<sparse_vector*>(vm, sparse_generators, sparse_embedders);
    }
}


template<class T>
T get_arg(std::string name, po::variables_map vm) {
    T result;
    if (vm.count(name)) {
        result = vm[name].as<T>();
    } else if (!vm["auto"].as<bool>()) {
        std::cout << "Please type in missing parameter " << name << ":" << std::endl;
        std::cin >> result;
    } else {
        std::cerr << "Error: Parameter " << name << " was not specified while in auto/noninteractive mode" << std::endl;
        std::exit(EXIT_FAILURE);
    }
    return result;
}
