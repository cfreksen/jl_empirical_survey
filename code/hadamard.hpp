#pragma once

#include <string>
#include <sstream>
#include <cstring>
#include <cmath>
#include "ffht/fht_header_only.h"
#include "types.hpp"
#include "util.hpp"
#include "transformer.hpp"


class Hadamard: public Transformer<dense_vector, dense_vector>, public Transformer<sparse_vector*, dense_vector> {
    const u64 original_dimension;
    const u32 log_vector_dimension;
    const u64 padding;

public:
    Hadamard(u64 vector_dimension) :
        original_dimension(vector_dimension),
        log_vector_dimension((u32) std::ceil(std::log2(original_dimension))),
        padding(((u64) 1 << log_vector_dimension) - original_dimension) {
    }

    dense_vector transform(dense_vector x) const {
        assert(x.size == original_dimension);
        dense_vector padded_x;
        if (padding == 0) {
            padded_x = x;
        } else {
            padded_x = calloc_real(original_dimension + padding);
            std::memcpy(padded_x.data, x.data, original_dimension * sizeof(double));
            delete_vector(x);
        }

        fht_double(padded_x.data, log_vector_dimension);
        return padded_x;
    }

    dense_vector transform(sparse_vector* x) const {
        auto dense = calloc_real(output_dimension());
        for (auto const &pair : *x) {
            dense.data[pair.first] = pair.second;
        }
        delete x;

        fht_double(dense.data, log_vector_dimension);
        return dense;
    }

    double normalisation_divisor() const {
        return std::sqrt((u64) 1 << log_vector_dimension);
    }

    u64 output_dimension() const {
        return (u64) 1 << log_vector_dimension;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(Hadamard " << output_dimension() << ")";
        return ss.str();
    }
};
