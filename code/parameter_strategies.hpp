#pragma once

#include <string>
#include <sstream>
#include <cmath>
#include "types.hpp"


class SparsityStrategy {
public:
    virtual u32 sparsity(u64 source_dimension, u32 target_dimension) const = 0;
    virtual std::string name() const = 0;
    virtual ~SparsityStrategy() {};
};

class MiddleDimensionStrategy {
public:
    virtual u32 middle_dimension(u64 source_dimension, u32 target_dimension) const = 0;
    virtual std::string name() const = 0;
    virtual ~MiddleDimensionStrategy() {};
};


template<u64 numerator, u64 denominator>
class FractionalSparsity: public SparsityStrategy {
public:
    u32 sparsity(u64, u32 target_dimension) const {
        return (numerator * target_dimension) / denominator;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(FractionalSparsity " << numerator << "/" << denominator << ")";
        return ss.str();
    }
};

template<u32 s>
class ConstantSparsity: public SparsityStrategy {
public:
    u32 sparsity(u64, u32) const {
        return s;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(ConstantSparsity " << s << ")";
        return ss.str();
    }
};

class SqrtSparsity: public SparsityStrategy {
public:
    u32 sparsity(u64, u32 target_dimension) const {
        return (u32) std::sqrt(target_dimension);
    }

    std::string name() const {
        return "SqrtSparsity";
    }
};

template<u64 log_base=2>
class logSourceSparsity: public SparsityStrategy {
public:
    u32 sparsity(u64 source_dimension, u32) const {
        return (u32) (std::log2(source_dimension) / std::log2(log_base));
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(logSourceSparsity " << log_base << ")";
        return ss.str();
    }
};

class PaperMiddleDimension: public MiddleDimensionStrategy {
public:
    u32 middle_dimension(u64 source_dimension, u32 target_dimension) const {
        return std::min((u64) (target_dimension * std::pow(std::log2(source_dimension), 4)),
                        source_dimension / 2);
    }

    std::string name() const {
        return "(PaperMiddleDimension m lg^4 d)";
    };
};

template<u64 numerator, u64 denominator>
class FractionalMiddleDimension: public MiddleDimensionStrategy {
public:
    u32 middle_dimension(u64 source_dimension, u32 target_dimension) const {
        return std::max((numerator * source_dimension) / denominator,
                        (u64) target_dimension);
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(FractionalMiddleDimension " << numerator << "/" << denominator << ")";
        return ss.str();
    }
};

template<u32 middim>
class ConstantMiddleDimension: public MiddleDimensionStrategy {
public:
    u32 middle_dimension(u64, u32) const {
        return middim;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(ConstantMiddleDimension " << middim << ")";
        return ss.str();
    }
};
