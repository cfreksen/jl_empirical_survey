#pragma once

#include "types.hpp"
#include "prng.hpp"
#include "hasher.hpp"
#include "tabulation_permutation_hash.hpp"


const u32 default_prng_degree = 10;
typedef PolynomialPrng<default_prng_degree> default_prng;

typedef Tabulation1PermutationHash<default_prng> default_hasher;
