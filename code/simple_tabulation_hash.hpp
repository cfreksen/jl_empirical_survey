#pragma once

#include <sstream>
#include <string>
#include "types.hpp"
#include "hasher.hpp"

template<class prng_t_, class char_t=u8>
class SimpleTabulationHash: public Hasher {
    static const u64 C = sizeof(u64) / sizeof(char_t);
    static const u64 char_size = 8 * sizeof(char_t);
    static const u64 num_chars = (u64) 1 << char_size;

    u32* tables[C];
    std::string prng_name;

public:
    using prng_t = prng_t_;

    SimpleTabulationHash(prng_t* prng) {
        for (u64 table_idx = 0; table_idx < C; ++table_idx) {
            tables[table_idx] = (u32*) std::calloc(num_chars, sizeof(u32));
            for (u64 i = 0; i < num_chars; ++i) {
                tables[table_idx][i] = prng->next_int(((u64) 1) << 32);
            }
        }

        prng_name = prng->name();
        delete prng;
    }

    ~SimpleTabulationHash() {
        for (u64 table_idx = 0; table_idx < C; ++table_idx) {
            free(tables[table_idx]);
        }
    }

    u32 hash(u64 x) const {
        u32 res = 0;
        for (u64 char_idx = 0; char_idx < C; ++char_idx) {
            u64 key = (char_t) (x >> (char_idx * char_size));
            res ^= tables[char_idx][key];
        }
        return res;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(SimpleTabulationHash "
           << "[" << num_chars << "]^" << C << " "
           << prng_name << ")";
        return ss.str();
    }
};
