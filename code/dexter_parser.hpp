#pragma once

#include <assert.h>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <stdexcept>

#include "types.hpp"
#include "data_parser.hpp"


class DexterParser: public DataParser {
    std::string filename;
    std::ifstream file_stream;

public:
    DexterParser(std::string path) :
        filename(path),
        file_stream(std::ifstream{path}) {
        if (!file_stream.good()) {
            std::cerr << "Error: Could not open " << path << std::endl;
            throw std::invalid_argument("Invalid path given to DexterParser constructor.");
        }
    }

    ~DexterParser() {
    }

    sparse_vector* next_vector() {
        auto result = new sparse_vector{};
        if (file_stream.eof()) {
            return result;
        }

        u64 word_id;
        double word_count;
        char ignored_char;

        while (!file_stream.eof() and file_stream.peek() != '\r'
               and file_stream.peek() != '\n') {
            file_stream >> word_id >> ignored_char >> word_count;
            result->push_back(std::make_pair(word_id, word_count));

            // Eat whitespace
            while (!file_stream.eof() and (file_stream.peek() == '\t'
                                           or file_stream.peek() == ' ')) {
                file_stream.read(&ignored_char, 1);
            }
        }

        // Eat the newline(s)
        while (!file_stream.eof() and (file_stream.peek() == '\r'
                                       or file_stream.peek() == '\n')) {
            file_stream.read(&ignored_char, 1);
        }

        return result;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(DexterParser " << filename << ")";
        return ss.str();
    }
};
