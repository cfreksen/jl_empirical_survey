#pragma once

#include <cstring>
#include <string>
#include <sstream>
#include <bitset>
#include <stdexcept>
#include <assert.h>
#include <iostream>
#include <iomanip>
#include <unordered_map>
#include <cmath>
#include <numeric>
#include <fftw3.h>
#include "gsl/gsl_matrix.h"
#include "gsl/gsl_spmatrix.h"
#include "types.hpp"
#include "prng.hpp"
#include "hasher.hpp"

bool is_power_of_2(u64 n) {
    return n > 0 && !(n & (n - 1));
}

bool is_power_of_2_signed(i64 n) {
    return n > 0 && !(n & (n - 1));
}

dense_vector_data calloc_real_raw(u64 vector_dimension) {
    dense_vector_data res = fftw_alloc_real(vector_dimension);
    // 0.0 is all zero bytes, so memset is ok
    std::memset(res, 0, vector_dimension * sizeof(double));
    return res;
}

dense_vector calloc_real(u64 vector_dimension) {
    dense_vector res;
    res.size = vector_dimension;
    res.data = calloc_real_raw(vector_dimension);
    return res;
}

void delete_vector(dense_vector& v) {
    fftw_free(v.data);
    v.data = nullptr;
}

void delete_vector(sparse_vector* v) {
    delete v;
}

double norm_squared(dense_vector v) {
    double res = 0.0;
    for (u64 idx = 0; idx < v.size; ++idx) {
        res += v.data[idx] * v.data[idx];
    }
    return res;
}

double norm_squared(sparse_vector* v) {
    double res = 0.0;
    for (auto &kv : *v) {
        res += std::pow(kv.second, 2);
    }
    return res;
}

double linfinity_norm(dense_vector v) {
    double res = 0.0;
    for (u64 i = 0; i < v.size; ++i) {
        res = std::max(res, std::abs(v.data[i]));
    }
    return res;
}

u64 l0_norm(dense_vector v) {
    u64 res = 0;
    for (u64 idx = 0; idx < v.size; ++idx) {
        res += (v.data[idx] != 0.0 and v.data[idx] != -0.0);
    }
    return res;
}

gsl_matrix* pm_string2gsl_matrix(std::string s, u64 rows, u64 cols) {
    assert(s.size() == rows * cols);
    gsl_matrix* result = gsl_matrix_alloc(rows, cols);
    u64 s_idx = 0;
    for (u64 r = 0; r < rows; ++r) {
        for (u64 c = 0; c < cols; ++c) {
            char curr = s[s_idx];
            ++s_idx;
            double val;
            if (curr == '+') {
                val = 1;
            } else if (curr == '-') {
                val = -1;
            } else {
                throw std::invalid_argument("Char in matrix string is not '+' or '-'");
            }
            gsl_matrix_set(result, r, c, val);
        }
    }
    return result;
}

dense_vector string2dense_vector(std::string s, u64 vector_dimension) {
    auto result = calloc_real(vector_dimension);
    std::istringstream iss(s);
    for (u64 i = 0; i < vector_dimension; ++i) {
        iss >> result.data[i];
    }

    return result;
}

void divide_vector(dense_vector v, double divisor) {
    auto v_view = gsl_vector_view_array(v.data, v.size);
    gsl_vector_scale(&v_view.vector, 1.0 / divisor);
}

void divide_vector(sparse_vector* v, double divisor) {
    for (auto &pair : *v) {
        pair.second /= divisor;
    }
}

double compute_distortion(double embedded_squared, double original_squared) {
    double ratio = std::sqrt(embedded_squared / original_squared);
    return std::max(ratio - 1.0, 1.0 - ratio);
}

gsl_matrix* generate_subhadamard(u32 num_rows, u32 num_columns) {
    assert(num_rows <= num_columns);
    assert(is_power_of_2(num_columns));
    gsl_matrix* result = gsl_matrix_alloc(num_rows, num_columns);
    for (u32 row = 0; row < num_rows; ++row) {
        u32 adjusted_row = row + (num_columns - num_rows);
        for (u32 col = 0; col < num_columns; ++col) {
            u32 bit_inner_product = (u32) std::bitset<32>(adjusted_row & col).count();
            gsl_matrix_set(result, row, col, std::pow(-1, bit_inner_product));
        }
    }

    return result;
}

template<class element_t, class prng_t>
void shuffle_array(element_t* begin, element_t* end, prng_t* prng) {
    u64 n = end - begin;
    for (u64 i = n - 1; i > 0; --i) {
        std::swap(begin[i], begin[prng->next_int(i+1)]);
    }
}

void print_gsl_matrix(gsl_matrix* matrix) {
    std::cout.setf(std::ios::fixed);
    std::cout.precision(0);
    for (u64 r = 0; r < matrix->size1; ++r) {
        std::cout << std::setw(2) <<  gsl_matrix_get(matrix, r, 0);
        for (u64 c = 1; c < matrix->size2; ++c) {
            std::cout << " " << std::setw(2) << gsl_matrix_get(matrix, r, c);
        }
        std::cout << std::endl;
    }
}

void print_gsl_matrix(gsl_spmatrix* matrix) {
    std::cout.setf(std::ios::fixed);
    std::cout.precision(1);
    for (u64 r = 0; r < matrix->size1; ++r) {
        std::cout << std::setw(7) <<  gsl_spmatrix_get(matrix, r, 0);
        for (u64 c = 1; c < matrix->size2; ++c) {
            std::cout << " " << std::setw(7) << gsl_spmatrix_get(matrix, r, c);
        }
        std::cout << std::endl;
    }
}

void print_vector(dense_vector x) {
    std::cout.setf(std::ios::fixed);
    std::cout.precision(2);
    std::cout << std::setw(5) << x.data[0];
    for (u64 i = 1; i < x.size; ++i) {
        std::cout << " " << std::setw(5) << x.data[i];
    }
    std::cout << std::endl;
}

std::vector<u64>* mk_histogram(std::unordered_map<double, u64>* data, u64 num_buckets) {
    double min = 0;
    double max = 2;
    std::vector<u64>* result = new std::vector<u64>{};
    for (u64 i = 0; i < num_buckets; ++i) {
        result->push_back(0);
    }

    for (auto &kv : *data) {
        double first = kv.first;
        assert(kv.first >= min);
        if (first > max) {
            first = max - 0.001;
        }

        u64 idx = ((first - min) / (max - min)) * num_buckets;

        assert(idx < num_buckets);
        (*result)[idx] += kv.second;
    }

    return result;
}

void print_hist_row(u64 v, u64 max_v) {
    u64 row_width = 80;
    for (u64 i = 0; i < row_width * v / max_v; ++i) {
        std::cout << "#";
    }
    std::cout << std::endl;
}

template<class sparsity_strat_t>
u32 choose_sparsity(u64 source_dimension, u32 target_dimension) {
    sparsity_strat_t sparsity_strat;
    return sparsity_strat.sparsity(source_dimension, target_dimension);
}

template<class middim_strat_t>
u32 choose_middle_dimension(u64 source_dimension, u32 target_dimension) {
    middim_strat_t middim_strat;
    return middim_strat.middle_dimension(source_dimension, target_dimension);
}

// `hash` should be a uniform random [0, 2**`num_hash_bits`). Result should have a uniform
// distribution in [0, `num_buckets`)
template<u32 num_hash_bits=32>
inline u32 distribute(u32 hash, u32 num_buckets) {
    return ((u64) hash * num_buckets) >> num_hash_bits;
}

template<class element_t=u64>
element_t* sample_without_replacement(u64 population_size, u64 num_samples, const Hasher* const hasher, u32 hash_offset=0) {
    assert(num_samples <= population_size);

    // Based on "Algorithm L" from Kim-Hung Li, 1994,
    // "Reservoir-Sampling Algorithms of Time Complexity O(n(1 +
    // log(N/n)))"

    // `* 3 * pop_size` should be sufficient to ensure keys do not overlap
    // for different offsets
    u64 hash_key = hash_offset * 3 * population_size;

    element_t* result = (element_t*) std::calloc(num_samples, sizeof(element_t));
    std::iota(result, result + num_samples, 0);

    double W = 1.0;

    for (element_t i = num_samples - 1;;) {
        W *= std::exp(std::log((double) hasher->hash(++hash_key) / ((u64) 1 << 32))
                      / num_samples);

        i += 1 + (element_t) std::floor(std::log((double) hasher->hash(++hash_key) / ((u64) 1 << 32))
                                        / std::log(1 - W));
        if (i >= population_size) {
            break;
        }
        result[distribute(hasher->hash(++hash_key), num_samples)] = i;
    }
    return result;
}
