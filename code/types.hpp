#pragma once

#include <inttypes.h>
#include <vector>
#include <chrono>

typedef int8_t i8;
typedef uint8_t u8;
typedef int16_t i16;
typedef uint16_t u16;
typedef int32_t i32;
typedef uint32_t u32;
typedef int64_t i64;
typedef uint64_t u64;
typedef __int128_t i128;
typedef __uint128_t u128;

typedef std::vector<std::pair<u64, double>> sparse_vector;
typedef double* dense_vector_data;
struct dense_vector {
    u64 size;
    dense_vector_data data;
};

// There is some C++ issue with using 'clock' and 'clock_t' in this
// kind of typedef.
typedef std::chrono::high_resolution_clock cclock;
typedef std::chrono::time_point<cclock> ttime;

// The timer uses microsecond resolution
const u32 time_precision10 = 9;
