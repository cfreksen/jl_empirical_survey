#pragma once

#include <string>
#include <sstream>
#include "types.hpp"
#include "random_signs.hpp"
#include "transformer.hpp"
#include "hadamard.hpp"
#include "util.hpp"
#include "raw_random_projection.hpp"

template<class hasher_t, class middim_strat_t>
class EagerGRHD: public Transformer<dense_vector, dense_vector> {
    typedef EagerRandomSigns<hasher_t> signs_t;
    typedef RawEagerRandomProjection<hasher_t> last_jl_t;

    const u64 source_dimension;
    const u32 target_dimension;
    const u32 middle_dimension;
    const signs_t signs;
    const Hadamard hadamard;
    u32* const subsamples;
    const last_jl_t* last_embedder;

    double internal_normalisation_divisor;

public:
    using prng_t = typename hasher_t::prng_t;

    EagerGRHD(u64 source_dimension, u32 target_dimension,
              prng_t* sign_prng, prng_t* subsample_prng, prng_t* last_prng) :
        source_dimension(source_dimension),
        target_dimension(target_dimension),
        middle_dimension(choose_middle_dimension<middim_strat_t>(source_dimension, target_dimension)),
        signs(signs_t{source_dimension, sign_prng}),
        hadamard(Hadamard{signs.output_dimension()}),
        subsamples((u32*) std::calloc(middle_dimension, sizeof(u32))) {
        hasher_t subsample_hasher{subsample_prng};
        for (u32 i = 0; i < middle_dimension; ++i) {
            subsamples[i] = distribute(subsample_hasher.hash(i), hadamard.output_dimension());
        }

        last_embedder = new last_jl_t{middle_dimension, target_dimension, last_prng};

        double subsample_normalisation_divisor = std::sqrt((double) middle_dimension / (double) source_dimension);
        internal_normalisation_divisor = signs.normalisation_divisor() * hadamard.normalisation_divisor() * subsample_normalisation_divisor * last_embedder->normalisation_divisor();
    }

    ~EagerGRHD() {
        free(subsamples);
        delete last_embedder;
    }

    dense_vector transform(dense_vector x) const {
        assert(x.size == source_dimension);
        auto post_sign = signs.transform(x);
        auto post_hadamard = hadamard.transform(post_sign);
        auto post_subsample = calloc_real(middle_dimension);
        for (u32 i = 0; i < middle_dimension; ++i) {
            post_subsample.data[i] = post_hadamard.data[subsamples[i]];
        }
        delete_vector(post_hadamard);
        auto post_last = last_embedder->transform(post_subsample);
        divide_vector(post_last, internal_normalisation_divisor);
        return post_last;
    }


    u64 output_dimension() const {
        return target_dimension;
    }

    double normalisation_divisor() const {
        return 1;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(EagerGRHD " << middle_dimension
           << " " << signs.name()
           << " " << last_embedder->name()
           << ")";
        return ss.str();
    }
};
