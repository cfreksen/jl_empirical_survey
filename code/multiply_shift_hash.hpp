#pragma once

#include <string>
#include "types.hpp"
#include "hasher.hpp"

// These algorithms are based on the implementations in
// https://arxiv.org/abs/1504.06804
namespace msh_util {
    template<class prng_t>
    u64 get_u64(prng_t* prng) {
        return (prng->next_int(((u64) 1) << 32) << 32) | prng->next_int(((u64) 1) << 32);
    }

    template<class prng_t>
    u64 get_odd_u64(prng_t* prng) {
        return get_u64(prng) | 1;
    }
}

template<class prng_t_>
class MultiplyShiftHash: public Hasher {
    const u64 seed;
    const std::string prng_name;

public:
    using prng_t = prng_t_;

    MultiplyShiftHash(prng_t* prng) :
        seed(msh_util::get_odd_u64(prng)),
        prng_name(prng->name()) {
        delete prng;
    }

    u32 hash(u64 x) const {
        return (u32) ((seed * x) >> 32);
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(MultiplyShiftHash "
           << prng_name << ")";
        return ss.str();
    }
};


template<class prng_t_>
class StrongMultiplyShiftHash: public Hasher {
    const u64 a1;
    const u64 a2;
    const u64 b;
    const std::string prng_name;

public:
    using prng_t = prng_t_;

    StrongMultiplyShiftHash(prng_t* prng) :
        a1(msh_util::get_u64(prng)),
        a2(msh_util::get_u64(prng)),
        b(msh_util::get_u64(prng)),
        prng_name(prng->name()) {
        delete prng;
    }

    u32 hash(u64 x) const {
        return ((a1 + x) * (a2 + (x >> 32)) + b) >> 32;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(StrongMultiplyShiftHash "
           << prng_name << ")";
        return ss.str();
    }
};
