#include <unordered_map>
#include <iostream>
#include <string>

#include "types.hpp"
#include "experiments.hpp"
#include "factories.hpp"
#include "parameter_strategies.hpp"
#include "seed_paths.hpp"

// Global settings
const u32 num_measurements_power = 12;
const u64 num_measurements = (u64) 1 << num_measurements_power;

const u64 num_data_samples_per_jlt = (u64) 1 << 7;

std::string gen_seed_path = "seeds/2019-01-02.bin";

auto output_folder_path = "../experiment_output/";

typedef SimpleTabulationHash<> fast_hasher;
typedef Tabulation1PermutationHash<> default_hasher;
typedef FractionalSparsity<1, 4> default_jlt_sparsity;
typedef PaperMiddleDimension default_middle_dimension;

// Generators
UniformGeneratorFactory<default_hasher> uniform_dense_gen_maker;
FixedCornerGeneratorFactory<2> fixed2_dense_gen_maker;
FixedCornerGeneratorFactory<4> fixed4_dense_gen_maker;
FixedCornerGeneratorFactory<10> fixed10_dense_gen_maker;
FixedCornerGeneratorFactory<16> fixed16_dense_gen_maker;
FixedCornerGeneratorFactory<64> fixed64_dense_gen_maker;
ClusterGeneratorDenseFactory<2, default_hasher> cluster2_dense_gen_maker;
ClusterGeneratorDenseFactory<4, default_hasher> cluster4_dense_gen_maker;
ClusterGeneratorDenseFactory<10, default_hasher> cluster10_dense_gen_maker;
ClusterGeneratorDenseFactory<16, default_hasher> cluster16_dense_gen_maker;
ClusterGeneratorDenseFactory<64, default_hasher> cluster64_dense_gen_maker;
ClusterGeneratorDenseFactory<256, default_hasher> cluster256_dense_gen_maker;
ClusterOnesGeneratorFactory cluster_ones_gen_maker;

template<class vector_t>
using generators_t = std::unordered_map<std::string,
                                        std::tuple<GeneratorFactory<vector_t>*, std::string>>;

generators_t<dense_vector> dense_generators
    {{"uniform", {&uniform_dense_gen_maker, "Uniform"}},
     {"sparse2", {&cluster2_dense_gen_maker, "Sparse 2"}},
     {"sparse4", {&cluster4_dense_gen_maker, "Sparse 4"}},
     {"sparse10", {&cluster10_dense_gen_maker, "Sparse 10"}},
     {"sparse16", {&cluster16_dense_gen_maker, "Sparse 16"}},
     {"sparse64", {&cluster64_dense_gen_maker, "Sparse 64"}}
    };

generators_t<sparse_vector*> sparse_generators
    {{"sparse", {&cluster_ones_gen_maker, "Sparse"}}
    };

// Dense embedders supported
EagerRandomProjectionFactory<dense_vector, default_hasher> randproj_dense;
EagerToeplitzJLFactory<default_hasher> toeplitz_dense;
EagerFJLTFactory<default_hasher, default_jlt_sparsity> fjlt_dense;
EagerLWTFactory<default_hasher, default_jlt_sparsity> lwt_dense;
EagerGRHDFactory<default_hasher, default_middle_dimension> grhd_dense;
SparseJLFactory<RawEagerSparseBlockJL<default_hasher, default_jlt_sparsity>, dense_vector> sparse_block_dense;
SparseJLFactory<RawEagerSparseDKSJL<default_hasher, default_jlt_sparsity>, dense_vector> sparse_dks_dense;
EagerFHFactory<dense_vector, Tabulation1PermutationHash<>> fh_eager_dense_1p;
EagerFHFactory<dense_vector, SimpleTabulationHash<>> fh_eager_dense_st;
EagerFHFactory<dense_vector, DoubleTabulationHash<>> fh_eager_dense_dt;
EagerFHFactory<dense_vector, MultiplyShiftHash<>> fh_eager_dense_ms;
EagerFHFactory<dense_vector, StrongMultiplyShiftHash<>> fh_eager_dense_sms;
EagerFJLTFactory<default_hasher, FractionalSparsity<1, 32>> fjlt_1q32_dense;
EagerFJLTFactory<default_hasher, FractionalSparsity<1, 16>> fjlt_1q16_dense;
EagerFJLTFactory<default_hasher, FractionalSparsity<1, 8>> fjlt_1q8_dense;
EagerFJLTFactory<default_hasher, FractionalSparsity<1, 4>> fjlt_1q4_dense;
EagerFJLTFactory<default_hasher, FractionalSparsity<1, 2>> fjlt_1q2_dense;
EagerGRHDFactory<default_hasher, FractionalMiddleDimension<1, 32>> grhd_1m32_dense;
EagerGRHDFactory<default_hasher, FractionalMiddleDimension<1, 16>> grhd_1m16_dense;
EagerGRHDFactory<default_hasher, FractionalMiddleDimension<1, 8>> grhd_1m8_dense;
EagerGRHDFactory<default_hasher, FractionalMiddleDimension<1, 4>> grhd_1m4_dense;
EagerGRHDFactory<default_hasher, FractionalMiddleDimension<1, 2>> grhd_1m2_dense;

// Sparse embedders supported
LazyFHFactory<sparse_vector*, Tabulation1PermutationHash<>> fh_lazy_sparse_1p;
LazyFHFactory<sparse_vector*, SimpleTabulationHash<>> fh_lazy_sparse_st;
LazyFHFactory<sparse_vector*, DoubleTabulationHash<>> fh_lazy_sparse_dt;
LazyFHFactory<sparse_vector*, MultiplyShiftHash<>> fh_lazy_sparse_ms;
LazyFHFactory<sparse_vector*, StrongMultiplyShiftHash<>> fh_lazy_sparse_sms;
SparseJLFactory<RawLazySparseBlockJL<Tabulation1PermutationHash<>, ConstantSparsity<16>>, sparse_vector*> sparse_block_1p_s16;
SparseJLFactory<RawLazySparseBlockJL<SimpleTabulationHash<>, ConstantSparsity<16>>, sparse_vector*> sparse_block_st_s16;
SparseJLFactory<RawLazySparseBlockJL<DoubleTabulationHash<>, ConstantSparsity<16>>, sparse_vector*> sparse_block_dt_s16;
SparseJLFactory<RawLazySparseBlockJL<MultiplyShiftHash<>, ConstantSparsity<16>>, sparse_vector*> sparse_block_ms_s16;
SparseJLFactory<RawLazySparseBlockJL<StrongMultiplyShiftHash<>, ConstantSparsity<16>>, sparse_vector*> sparse_block_sms_s16;


template<class vector_t>
using embedders_t = std::unordered_map<std::string,
                                       std::tuple<JLTFactory<vector_t>*, std::string, std::string>>;
embedders_t<dense_vector> dense_embedders
    {{"randproj", {&randproj_dense, "Random Projection", seed_paths[0]}},
     {"toeplitz", {&toeplitz_dense, "Toeplitz", seed_paths[1]}},
     {"fjlt", {&fjlt_dense, "FJLT", seed_paths[2]}},
     {"lwt", {&lwt_dense, "LWT", seed_paths[3]}},
     {"grhd", {&grhd_dense, "GRHD", seed_paths[4]}},
     {"sparse_block", {&sparse_block_dense, "SparseJL (block)", seed_paths[5]}},
     {"sparse_dks", {&sparse_dks_dense, "SparseJL (dks)", seed_paths[6]}},
     {"fh_1p", {&fh_eager_dense_1p, "Feature hashing (1Permutation)", seed_paths[7]}},
     {"fh_st", {&fh_eager_dense_st, "Feature hashing (SimpleTabulation)", seed_paths[8]}},
     {"fh_dt", {&fh_eager_dense_dt, "Feature hashing (DoubleTabulation)", seed_paths[9]}},
     {"fh_ms", {&fh_eager_dense_ms, "Feature hashing (MultiplyShift)", seed_paths[10]}},
     {"fh_sms", {&fh_eager_dense_sms, "Feature hashing (Strong MultiplyShift)", seed_paths[11]}},
     {"fjlt_q32", {&fjlt_dense, "FJLT q = 1/32", seed_paths[12]}},
     {"fjlt_q16", {&fjlt_dense, "FJLT q = 1/16", seed_paths[13]}},
     {"fjlt_q8", {&fjlt_dense, "FJLT q = 1/8", seed_paths[14]}},
     {"fjlt_q4", {&fjlt_dense, "FJLT q = 1/4", seed_paths[15]}},
     {"fjlt_q2", {&fjlt_dense, "FJLT q = 1/2", seed_paths[16]}},
     {"grhd_mid32", {&grhd_1m32_dense, "GRHD mid = d/32", seed_paths[17]}},
     {"grhd_mid16", {&grhd_1m16_dense, "GRHD mid = d/16", seed_paths[18]}},
     {"grhd_mid8", {&grhd_1m8_dense, "GRHD mid = d/8", seed_paths[19]}},
     {"grhd_mid4", {&grhd_1m4_dense, "GRHD mid = d/4", seed_paths[20]}},
     {"grhd_mid2", {&grhd_1m2_dense, "GRHD mid = d/2", seed_paths[21]}},
    };

embedders_t<sparse_vector*> sparse_embedders
    {{"fh_1p", {&fh_lazy_sparse_1p, "Feature hashing (1Permutation)", seed_paths[22]}},
     {"fh_st", {&fh_lazy_sparse_st, "Feature hashing (SimpleTabulation)", seed_paths[23]}},
     {"fh_dt", {&fh_lazy_sparse_dt, "Feature hashing (DoubleTabulation)", seed_paths[24]}},
     {"fh_ms", {&fh_lazy_sparse_ms, "Feature hashing (MultiplyShift)", seed_paths[25]}},
     {"fh_sms", {&fh_lazy_sparse_sms, "Feature hashing (Strong MultiplyShift)", seed_paths[26]}},
    };


std::unordered_map<std::string, std::vector<std::string>> embedder_sets
  {{"defaults", {"randproj", "toeplitz", "fjlt", "lwt", "grhd", "sparse_block", "fh_1p"}},
   {"fjlts", {"fjlt_q2", "fjlt_q4", "fjlt_q8", "fjlt_q16", "fjlt_q32"}},
   {"grhds", {"grhd_mid2", "grhd_mid4", "grhd_mid8", "grhd_mid16", "grhd_mid32"}},
   {"fh_hash", {"fh_1p", "fh_st", "fh_dt", "fh_ms", "fh_sms"}},
  };


template<class input_t>
void do_experiment(std::string short_jlt_name, std::string short_gen_name,
                   GeneratorFactory<input_t>* gen_maker, JLTFactory<input_t>* jlt_maker,
                   std::string jlt_label, std::string jlt_seed_path, std::string gen_maker_label,
                   u64 input_size, u32 target_dimension, bool is_input_dense) {
    std::stringstream output_file_path;
    output_file_path << output_folder_path << "dist_"
                     << (is_input_dense ? "dense" : "sparse") << "_"
                     << std::setfill('0')
                     << std::setw(5)
                     << input_size << "_"
                     << std::setfill('0')
                     << std::setw(5)
                     << target_dimension << "_"
                     << short_gen_name << "_"
                     << short_jlt_name
                     << ".txt";
    run_distortion_experiment<input_t>(jlt_maker, gen_maker, jlt_seed_path, gen_seed_path,
                                       (std::string) output_file_path.str(), input_size,
                                       target_dimension, num_measurements, num_data_samples_per_jlt,
                                       jlt_label, gen_maker_label, short_gen_name);
}


void print_help(std::string program_name) {
    std::cerr << "Usage: " << program_name << " INPUT_TYPE INPUT_SIZE TARGET_DIM INPUT_GENERATOR EMBEDDER...\n";
}


template<class vector_t>
int handle_distribution_args(int num_dist_args, char** argv,
                             bool is_input_dense, u64 input_size, u32 target_dimension,
                             generators_t<vector_t> input_generators, embedders_t<vector_t> embedders) {
    std::string short_gen_name = argv[0];
    auto gen_maker_it = input_generators.find(short_gen_name);
    if (gen_maker_it == input_generators.end()) {
        std::cerr << "Error: unknown input generator " << argv[0] << std::endl;
        return 1;
    }
    auto [gen_maker, gen_maker_label] = gen_maker_it->second;

    for (int argi = 1; argi < num_dist_args; ++argi) {
        std::string arg = argv[argi];
        auto embedder_set_it = embedder_sets.find(arg);
        if (embedder_set_it != embedder_sets.end()) {
            for (auto& jlt_key : embedder_set_it->second) {
                auto [jlt_maker, jlt_label, jlt_seed_path] = embedders[jlt_key];
                do_experiment<vector_t>(jlt_key, short_gen_name, gen_maker, jlt_maker, jlt_label, jlt_seed_path, gen_maker_label, input_size, target_dimension, is_input_dense);
            }
        } else {
            auto jlt_maker_it = embedders.find(arg);
            if (jlt_maker_it == embedders.end()) {
                std::cerr << "Error: unknown embedder " << argv[argi] << std::endl;
                std::cerr << " Known embedders are:" << std::endl;
                for (auto& kv: embedders) {
                    std::cerr << " " << kv.first;
                }
                std::cerr << std::endl;
                return 1;
            } else {
                auto [jlt_maker, jlt_label, jlt_seed_path] = jlt_maker_it->second;
                do_experiment<vector_t>(arg, short_gen_name, gen_maker, jlt_maker, jlt_label, jlt_seed_path, gen_maker_label, input_size, target_dimension, is_input_dense);
            }
        }
    }
    return 0;
}

int main(int argc, char** argv) {
    if (argc <= 1 or argv[1] == std::string{"--help"} or argv[1] == std::string{"-h"}) {
        print_help(argc == 0 ? "program_name" : argv[0]);
        return 0;
    }

    if (argc < 5) {
        std::cerr << "Error: Too few arguments" << std::endl;
        print_help(argv[0]);
        return 1;
    }

    std::string data_type = argv[1];
    bool is_input_dense = (data_type == "dense");
    if (data_type != "dense" and data_type != "sparse") {
        std::cerr << "Error: Unknown input data type: " << data_type << std::endl
                  << " Known input data types: dense sparse." << std::endl;
        return 1;
    }
    const u64 input_size = std::stoi(argv[2]);
    const u32 target_dimension = std::stoi(argv[3]);

    if (is_input_dense) {
        return handle_distribution_args<dense_vector>(argc - 4, argv + 4, is_input_dense, input_size, target_dimension, dense_generators, dense_embedders);
    } else {
        return handle_distribution_args<sparse_vector*>(argc - 4, argv + 4, is_input_dense, input_size, target_dimension, sparse_generators, sparse_embedders);
    }
}
