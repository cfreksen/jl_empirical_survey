#pragma once

#include <string>
#include "types.hpp"

template<class input_t, class output_t>
class Transformer {
public:
    virtual output_t transform(input_t x) const = 0;
    virtual double normalisation_divisor() const = 0;
    virtual u64 output_dimension() const = 0;
    virtual std::string name() const = 0;
    virtual ~Transformer() {};
};

template<class input_t>
class JLTFactory {
    typedef Transformer<input_t, dense_vector> embedder_t;
public:
    virtual embedder_t* mk_jlt(const std::string seed_file_path, u64 source_dimension, u32 target_dimension, u32 seed_offset) const = 0;
};
