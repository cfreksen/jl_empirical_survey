#pragma once

#include <string>
#include <sstream>
#include <iostream>
#include <cassert>
#include "types.hpp"

class Prng {
protected:
    u64 mulmod(u64 a, u64 b, u64 m) {
        return (u64) (((u128) a * ((u128) b)) % m);
    }

public:
    virtual u64 next_int(u64 limit) = 0;
    virtual ~Prng() {};
    virtual std::string name() const = 0;
};


template<u64 degree = 20, bool verbose = false>
class PolynomialPrng: public Prng {
    u64 coefficients[degree];
    const std::string seed_file_name;
    const u32 seed_file_offset;
    u64 count;

    static const u64 prime = (((u64) 1) << 61) - 1;

public:
    PolynomialPrng(const std::string seed_file_path, u32 offset = 0) :
        seed_file_name(seed_file_path),
        seed_file_offset(offset),
        count(2) {

        std::FILE* seed_file = std::fopen(seed_file_path.c_str(), "rb");
        if (!seed_file) {
            std::stringstream ss;
            ss << "Error: Opening of seed file for PRNG failed, " << seed_file_path;
            std::perror(ss.str().c_str());
            throw std::invalid_argument(seed_file_path);
        }
        std::fseek(seed_file, offset * sizeof(u64) * degree, SEEK_SET);
        u64 beginning_of_seed = ftell(seed_file);
        size_t coefficients_read = std::fread(coefficients, sizeof(u64), degree, seed_file);
        u64 end_of_seed = ftell(seed_file) - 1;
        std::fclose(seed_file);
        if (verbose) {
            std::cout << "PolyPRNG seeded by " << seed_file_path
                      << " from byte " << beginning_of_seed
                      << " to byte " << end_of_seed << std::endl;
        }

        if (coefficients_read != degree) {
            std::fprintf(stderr, "Error: Insufficient number of coefficients in seed_file for PRNG.\n\tExpected: %4" PRIu64 "\n\tActual:   %4" PRIu64 "\n", degree, coefficients_read);
            throw std::out_of_range(seed_file_path);
        }

        if (coefficients[degree - 1] == 0) {
            std::fprintf(stderr, "Error: Last coefficient in PRNG seed is 0\n");
            throw std::invalid_argument(seed_file_path);
        }

        for (u64 idx = 0; idx < degree; ++idx) {
            coefficients[idx] = coefficients[idx] % prime;
        }

        count = 2;
    }

    u64 next_int(u64 limit) {
        u64 accum = coefficients[0];
        u64 count_power = count;
        for (u64 i = 1; i < degree; ++i) {
            // accum will not overflow u64, as old accum and mulmod
            // result are both < 2^61 - 1
            accum += mulmod(count_power, coefficients[i], prime);
            if (accum >= prime) {
                accum -= prime;
            }

            count_power = mulmod(count, count_power, prime);
        }

        ++count;

        return accum % limit;
    }

    void increment_count(u64 inc) {
        assert(count + inc > count);
        assert(count + inc > inc);
        count += inc;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(PolyPRNG " << degree
           << " " << seed_file_name
           << " +" << seed_file_offset << ")";
        return ss.str();
    }
};
