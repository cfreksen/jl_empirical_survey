#pragma once

#include <string>
#include <sstream>
#include "types.hpp"
#include "transformer.hpp"
#include "raw_random_projection.hpp"


template<class hasher_t>
class EagerRandomProjection: public Transformer<dense_vector, dense_vector>, public Transformer<sparse_vector*, dense_vector> {
    const RawEagerRandomProjection<hasher_t> jlt;

public:
    using prng_t = typename hasher_t::prng_t;

    EagerRandomProjection(u64 source_dimension, u32 target_dimension, prng_t* sign_prng) :
        jlt(RawEagerRandomProjection<hasher_t>{source_dimension, target_dimension, sign_prng}) {
    }

    ~EagerRandomProjection() {
    }

    dense_vector transform(dense_vector x) const {
        auto res = jlt.transform(x);
        divide_vector(res, jlt.normalisation_divisor());
        return res;
    }

    dense_vector transform(sparse_vector* x) const {
        auto res = jlt.transform(x);
        divide_vector(res, jlt.normalisation_divisor());
        return res;
    }

    u64 output_dimension() const {
        return jlt.output_dimension();
    }

    double normalisation_divisor() const {
        return 1;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(RandomProjection " << jlt.name() << ")";
        return ss.str();
    }
};
