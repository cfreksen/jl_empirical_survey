#pragma once

#include <sstream>
#include <string>
#include <iostream>
#include <iomanip>

#include "types.hpp"
#include "hasher.hpp"
#include "prng.hpp"
#include "util.hpp"

template<class prng_t_, class char_t = u16, u64 D = 8, bool verbose = false>
class DoubleTabulationHash: public Hasher {
    static const u64 C = sizeof(u64) / sizeof(char_t);
    static const u64 char_size = 8 * sizeof(char_t);
    static const u64 num_chars = (u64) 1 << char_size;

    typedef char_t intermediate[D];

    intermediate* T1[C];
    u32* T2[D];
    std::string prng_name;

public:
    using prng_t = prng_t_;

    DoubleTabulationHash(prng_t* prng) {
        if (verbose) {
            std::cout << "Filling T1...\r" << std::flush;
        }
        for (u64 table_idx = 0; table_idx < C; ++table_idx) {
            T1[table_idx] = (intermediate*) std::calloc(num_chars, sizeof(intermediate));
            for (u64 char_idx = 0; char_idx < num_chars; ++char_idx) {
                for (u64 intermediate_idx = 0; intermediate_idx < D; ++intermediate_idx) {
                    T1[table_idx][char_idx][intermediate_idx] = prng->next_int(num_chars);
                }
            }
        }

        if (verbose) {
            std::cout << "Filling T2...\r" << std::flush;
        }
        for (u64 table_idx = 0; table_idx < D; ++table_idx) {
            T2[table_idx] = (u32*) std::calloc(num_chars, sizeof(u32));
            for (u64 char_idx = 0; char_idx < num_chars; ++char_idx) {
                T2[table_idx][char_idx] = prng->next_int((u64) 1 << 32);
            }
        }
        if (verbose) {
            std::cout << "\033[K" << std::flush;
        }

        prng_name = prng->name();
        delete prng;
    }

    ~DoubleTabulationHash() {
        for (u64 table_idx = 0; table_idx < C; ++table_idx) {
            free(T1[table_idx]);
        }
        for (u64 table_idx = 0; table_idx < D; ++table_idx) {
            free(T2[table_idx]);
        }
    }

    u32 hash(u64 x) const {
        intermediate inter_xor{};
        for (u64 char_idx = 0; char_idx < C; ++char_idx) {
            u64 key = (char_t) (x >> (char_idx * char_size));
            for (u64 inter_idx = 0; inter_idx < D; ++inter_idx) {
                inter_xor[inter_idx] ^= T1[char_idx][key][inter_idx];
            }
        }

        u32 res = 0;
        for (u64 inter_idx = 0; inter_idx < D; ++inter_idx) {
            res ^= T2[inter_idx][inter_xor[inter_idx]];
        }

        return res;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(DoubleTabulationHash "
           << "[" << num_chars << "]^" << C << " "
           << "[" << num_chars << "]^" << D << " "
           << prng_name << ")";
        return ss.str();
    }
};
