#pragma once

#include <string>
#include <sstream>
#include "types.hpp"
#include "transformer.hpp"
#include "util.hpp"
#include "raw_sparse_jl.hpp"

template<class raw_jlt_t, class input_t>
class SparseJL: public Transformer<input_t, dense_vector> {
    const raw_jlt_t jlt;

public:
    using prng_t = typename raw_jlt_t::prng_t;

    SparseJL(u64 source_dimension, u32 target_dimension, prng_t* prng) :
        jlt(source_dimension, target_dimension, prng) {
    }

    ~SparseJL() {
    }

    dense_vector transform(input_t x) const {
        auto res = jlt.transform(x);
        divide_vector(res, jlt.normalisation_divisor());
        return res;
    }

    u64 output_dimension() const {
        return jlt.output_dimension();
    }

    double normalisation_divisor() const {
        return 1;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(SparseJL " << jlt.name() << ")";
        return ss.str();
    }
};
