#define BOOST_TEST_MODULE JL Unit tests
#include <boost/test/included/unit_test.hpp>
#include <boost/utility/identity_type.hpp>

#include <string>
#include <cmath>
#include <iostream>

#include <fftw3.h>
#include "types.hpp"
#include "util.hpp"
#include "double_tabulation_hash.hpp"
#include "simple_tabulation_hash.hpp"
#include "tabulation_permutation_hash.hpp"
#include "multiply_shift_hash.hpp"
#include "random_signs.hpp"
#include "hadamard.hpp"
#include "raw_feature_hash.hpp"
#include "raw_random_projection.hpp"
#include "raw_sparse_jl.hpp"
#include "raw_toeplitz.hpp"
#include "raw_lwt.hpp"
#include "generator.hpp"
#include "cluster_generator.hpp"
#include "processed_generator.hpp"
#include "uniform_generator.hpp"
#include "subcube_corner_generator.hpp"
#include "dexter_parser.hpp"
#include "random_sign_generator.hpp"
#include "fjlt.hpp"
#include "lwt.hpp"
#include "sparse_jl.hpp"
#include "factories.hpp"
#include "parameter_strategies.hpp"
#include "experiments.hpp"

typedef PolynomialPrng<4> prng_t;
typedef Tabulation1PermutationHash<prng_t> default_hasher;
typedef FractionalSparsity<1, 4> default_jlt_sparsity;
typedef PaperMiddleDimension default_middle_dimension;


// Unit testing utility functions
void test_sparse_vector_equals(sparse_vector* actual, sparse_vector* expected) {
    BOOST_TEST_REQUIRE(actual->size() == expected->size());
    for (u64 i = 0; i < actual->size(); ++i) {
        BOOST_TEST((*actual)[i].first == (*expected)[i].first);
        BOOST_TEST((*actual)[i].second == (*expected)[i].second);
    }
}


BOOST_AUTO_TEST_SUITE(utility_tests)


BOOST_AUTO_TEST_CASE(test_expected_endianess) {
    u32 x = 1;
    u8 as_array[4];
    std::memcpy(&as_array, &x, sizeof(u32));
    BOOST_TEST(as_array[0] == 1);
    BOOST_TEST(as_array[3] == 0);
}

BOOST_AUTO_TEST_CASE(test_distribute_distributes) {
    u32 num_buckets = 123;
    u32 num_samples = 1 << 14;
    u64* counts = (u64*) std::calloc(num_buckets, sizeof(u64));

    PolynomialPrng<10> prng{"seeds/2019-01-01.bin"};
    for (u32 i = 0; i < num_samples; ++i) {
        u32 bucket = (u32) distribute(prng.next_int((u64) 1 << 32), num_buckets);
        BOOST_TEST_REQUIRE(bucket < num_buckets);
        counts[bucket]++;
    }

    u64 avg = num_samples / num_buckets;
    for (u64 h = 0; h < num_buckets; ++h) {
        BOOST_TEST(counts[h] > 0);
        BOOST_TEST_WARN(avg / 2 <= counts[h]);
        BOOST_TEST_WARN(counts[h] <= 2 * avg);
    }

    free(counts);
}

BOOST_AUTO_TEST_CASE(test_distribute_u29) {
    u32 num_buckets = 123;
    u32 num_samples = 1 << 14;
    u64* counts = (u64*) std::calloc(num_buckets, sizeof(u64));

    PolynomialPrng<10> prng{"seeds/2019-01-01.bin"};
    for (u32 i = 0; i < num_samples; ++i) {
        u32 bucket = (u32) distribute<29>(prng.next_int((u64) 1 << 29), num_buckets);
        BOOST_TEST_REQUIRE(bucket < num_buckets);
        counts[bucket]++;
    }

    u64 avg = num_samples / num_buckets;
    for (u64 h = 0; h < num_buckets; ++h) {
        BOOST_TEST(counts[h] > 0);
        BOOST_TEST_WARN(avg / 2 <= counts[h]);
        BOOST_TEST_WARN(counts[h] <= 2 * avg);
    }

    free(counts);
}

BOOST_AUTO_TEST_CASE(test_double_tabhash_dst) {
    u32 dst = 1 << 8;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto hasher = DoubleTabulationHash<prng_t>{prng};
    u64* counts = (u64*) std::calloc(dst, sizeof(u64));

    u64 total = 0;

    for (u64 i = 0; i < 100000; ++i, ++total) {
        u32 res = distribute(hasher.hash(i), dst);
        counts[res]++;
    }
    for (u64 i = 0; i < 10000; ++i, ++total) {
        // Some spread "big" numbers
        u32 res = distribute(hasher.hash(i * i * i + ((i * i) << 30)), dst);
        counts[res]++;
    }

    u64 avg = total / dst;
    for (u64 h = 0; h < dst; ++ h) {
        BOOST_TEST(counts[h] > 0);
        BOOST_TEST_WARN(avg / 2 <= counts[h]);
        BOOST_TEST_WARN(counts[h] <= 2 * avg);
    }

    free(counts);
}

BOOST_AUTO_TEST_CASE(test_double_tabhash_max_dst) {
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto hasher = DoubleTabulationHash{prng};

    u64 total = 0;
    u64 num_large = 0;

    for (u64 i = 0; i < 1000; ++i, ++total) {
        u32 res = hasher.hash(i);
        if (res > (u32) 1 << 31) {
            ++num_large;
        }
    }

    BOOST_TEST(num_large > 0);

    // 2/3 expected <= num_large <= 4/3 expected
    BOOST_TEST_WARN(total / 3 <= num_large);
    BOOST_TEST_WARN(num_large <= total * 2 / 3);
}

BOOST_AUTO_TEST_CASE(test_simple_tabhash_dst) {
    u32 dst = 1 << 8;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto hasher = SimpleTabulationHash<prng_t>{prng};
    u64* counts = (u64*) std::calloc(dst, sizeof(u64));

    u64 total = 0;

    for (u64 i = 0; i < 10000; ++i, ++total) {
        u32 res = distribute(hasher.hash(i), dst);
        counts[res]++;
    }
    for (u64 i = 0; i < 10000; ++i, ++total) {
        // Some spread "big" numbers
        u32 res = distribute(hasher.hash(i * i * i + ((i * i) << 30)), dst);
        counts[res]++;
    }

    u64 avg = total / dst;
    for (u64 h = 0; h < dst; ++ h) {
        BOOST_TEST(counts[h] > 0);
        BOOST_TEST_WARN(avg / 2 <= counts[h]);
        BOOST_TEST_WARN(counts[h] <= 2 * avg);
    }

    free(counts);
}

BOOST_AUTO_TEST_CASE(test_simple_tabhash_u16) {
    u32 dst = 1 << 8;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto hasher = SimpleTabulationHash<prng_t, u16>{prng};
    u64* counts = (u64*) std::calloc(dst, sizeof(u64));

    u64 total = 0;

    for (u64 i = 0; i < 10000; ++i, ++total) {
        u32 res = distribute(hasher.hash(i), dst);
        counts[res]++;
    }
    for (u64 i = 0; i < 10000; ++i, ++total) {
        // Some spread "big" numbers
        u32 res = distribute(hasher.hash(i * i * i + ((i * i) << 30)), dst);
        counts[res]++;
    }

    u64 avg = total / dst;
    for (u64 h = 0; h < dst; ++ h) {
        BOOST_TEST(counts[h] > 0);
        BOOST_TEST_WARN(avg / 2 <= counts[h]);
        BOOST_TEST_WARN(counts[h] <= 2 * avg);
    }

    free(counts);
}

BOOST_AUTO_TEST_CASE(test_tab1perm_hash_u8) {
    u32 dst = 1 << 8;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto hasher = Tabulation1PermutationHash<prng_t, u8>{prng};
    u64* counts = (u64*) std::calloc(dst, sizeof(u64));

    u64 total = 0;

    for (u64 i = 0; i < 10000; ++i, ++total) {
        u32 res = distribute(hasher.hash(i), dst);
        counts[res]++;
    }
    for (u64 i = 0; i < 10000; ++i, ++total) {
        // Some spread "big" numbers
        u32 res = distribute(hasher.hash(i * i * i + ((i * i) << 30)), dst);
        counts[res]++;
    }

    u64 avg = total / dst;
    for (u64 h = 0; h < dst; ++ h) {
        BOOST_TEST(counts[h] > 0);
        BOOST_TEST_WARN(avg / 2 <= counts[h]);
        BOOST_TEST_WARN(counts[h] <= 2 * avg);
    }

    free(counts);
}

BOOST_AUTO_TEST_CASE(test_tab1perm_hash_u16) {
    u32 dst = 1 << 8;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto hasher = Tabulation1PermutationHash<prng_t, u16>{prng};
    u64* counts = (u64*) std::calloc(dst, sizeof(u64));

    u64 total = 0;

    for (u64 i = 0; i < 10000; ++i, ++total) {
        u32 res = distribute(hasher.hash(i), dst);
        counts[res]++;
    }
    for (u64 i = 0; i < 10000; ++i, ++total) {
        // Some spread "big" numbers
        u32 res = distribute(hasher.hash(i * i * i + ((i * i) << 30)), dst);
        counts[res]++;
    }

    u64 avg = total / dst;
    for (u64 h = 0; h < dst; ++ h) {
        BOOST_TEST(counts[h] > 0);
        BOOST_TEST_WARN(avg / 2 <= counts[h]);
        BOOST_TEST_WARN(counts[h] <= 2 * avg);
    }

    free(counts);
}

BOOST_AUTO_TEST_CASE(test_multshift_hash) {
    u32 dst = 1 << 8;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto hasher = MultiplyShiftHash<prng_t>{prng};
    u64* counts = (u64*) std::calloc(dst, sizeof(u64));

    u64 total = 0;

    for (u64 i = 0; i < 10000; ++i, ++total) {
        u32 res = distribute(hasher.hash(i), dst);
        counts[res]++;
    }
    for (u64 i = 0; i < 10000; ++i, ++total) {
        // Some spread "big" numbers
        u32 res = distribute(hasher.hash(i * i * i + ((i * i) << 30)), dst);
        counts[res]++;
    }

    u64 avg = total / dst;
    for (u64 h = 0; h < dst; ++ h) {
        BOOST_TEST(counts[h] > 0);
        BOOST_TEST_WARN(avg / 2 <= counts[h]);
        BOOST_TEST_WARN(counts[h] <= 2 * avg);
    }

    free(counts);
}

BOOST_AUTO_TEST_CASE(test_strong_multshift_hash) {
    u32 dst = 1 << 8;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto hasher = StrongMultiplyShiftHash<prng_t>{prng};
    u64* counts = (u64*) std::calloc(dst, sizeof(u64));

    u64 total = 0;

    for (u64 i = 0; i < 10000; ++i, ++total) {
        u32 res = distribute(hasher.hash(i), dst);
        counts[res]++;
    }
    for (u64 i = 0; i < 10000; ++i, ++total) {
        // Some spread "big" numbers
        u32 res = distribute(hasher.hash(i * i * i + ((i * i) << 30)), dst);
        counts[res]++;
    }

    u64 avg = total / dst;
    for (u64 h = 0; h < dst; ++ h) {
        BOOST_TEST(counts[h] > 0);
        BOOST_TEST_WARN(avg / 2 <= counts[h]);
        BOOST_TEST_WARN(counts[h] <= 2 * avg);
    }

    free(counts);
}

BOOST_AUTO_TEST_CASE(test_hashing_sign_generator) {
    u64 block_length = (u64) 1 << 6;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto sign_gen = HashingSignGenerator<default_hasher>{prng, block_length};

    std::vector<i8> initial_block{};
    for (u64 i = 0; i < block_length; ++i) {
        initial_block.push_back(sign_gen.next_sign());
    }

    i64 sum0 = 0;
    std::vector<i8> block0{};
    sign_gen.set_to_block_idx(0);
    for (u64 i = 0; i < block_length; ++i) {
        i8 sign = sign_gen.next_sign();
        BOOST_TEST(std::abs(sign) == 1);
        block0.push_back(sign);
        sum0 += sign;
    }
    BOOST_TEST_WARN(-((i64) block_length) / 4 <= sum0);
    BOOST_TEST_WARN(sum0 <= (i64) block_length / 4);

    BOOST_CHECK_EQUAL_COLLECTIONS(initial_block.begin(), initial_block.end(),
                                  block0.begin(), block0.end());

    i64 sum1 = 0;
    std::vector<i8> block1{};
    sign_gen.set_to_block_idx(1);
    for (u64 i = 0; i < block_length; ++i) {
        i8 sign = sign_gen.next_sign();
        BOOST_TEST(std::abs(sign) == 1);
        block1.push_back(sign);
        sum1 += sign;
    }
    BOOST_TEST_WARN(-((i64) block_length) / 4 <= sum1);
    BOOST_TEST_WARN(sum1 <= (i64) block_length / 4);


    u64 diffs = 0;
    for (u64 i = 0; i < block_length; ++i) {
        diffs += (block0[i] != block1[i]);
    }
    BOOST_TEST(diffs > 0);
    BOOST_TEST_WARN(block_length / 3 <= diffs);
    BOOST_TEST_WARN(diffs <= 2 * block_length / 3);


    std::vector<i8> block1_again{};
    sign_gen.set_to_block_idx(1);
    for (u64 i = 0; i < block_length; ++i) {
        block1_again.push_back(sign_gen.next_sign());
    }

    BOOST_CHECK_EQUAL_COLLECTIONS(block1.begin(), block1.end(),
                                  block1_again.begin(), block1_again.end());
}

BOOST_AUTO_TEST_CASE(test_sample_without_replacement) {
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    default_hasher hasher{prng};
    u64 population_size = 100;
    u64 num_samples = 10;
    u64 num_trials = 1000;
    u64* counts = (u64*) std::calloc(population_size, sizeof(u64));

    for (u64 trial = 0; trial < num_trials; ++trial) {
        u64* res = sample_without_replacement(population_size, num_samples, &hasher, trial);
        for (u64 i = 0; i < num_samples; ++i) {
            BOOST_TEST_REQUIRE(res[i] < population_size);
            counts[res[i]]++;
            for (u64 j = i + 1; j < num_samples; ++j) {
                BOOST_TEST(res[i] != res[j]);
            }
        }
        free(res);
    }

    u64 avg = (num_trials * num_samples) / population_size;
    for (u64 i = 0; i < population_size; ++i) {
        BOOST_TEST(counts[i] > 0);
        BOOST_TEST_WARN(0.75 * avg <= counts[i]);
        BOOST_TEST_WARN(counts[i] <= 1.25 * avg);
    }
    free(counts);

}


BOOST_AUTO_TEST_SUITE(util_hpp_tests)

BOOST_AUTO_TEST_CASE(test_is_power_of_2) {
    BOOST_TEST(is_power_of_2(2));
    BOOST_TEST(is_power_of_2(4));
    BOOST_TEST(is_power_of_2(8));
    BOOST_TEST(is_power_of_2(1024));
    BOOST_TEST(is_power_of_2(1));
    BOOST_TEST(not is_power_of_2(3));
    BOOST_TEST(not is_power_of_2(15));
    BOOST_TEST(not is_power_of_2(17));
    BOOST_TEST(not is_power_of_2(0));

    BOOST_TEST(is_power_of_2_signed(2));
    BOOST_TEST(is_power_of_2_signed(4));
    BOOST_TEST(is_power_of_2_signed(8));
    BOOST_TEST(is_power_of_2_signed(1024));
    BOOST_TEST(is_power_of_2_signed(1));
    BOOST_TEST(not is_power_of_2_signed(3));
    BOOST_TEST(not is_power_of_2_signed(15));
    BOOST_TEST(not is_power_of_2_signed(17));
    BOOST_TEST(not is_power_of_2_signed(0));
    BOOST_TEST(not is_power_of_2_signed(-1));
    BOOST_TEST(not is_power_of_2_signed(-2));
    BOOST_TEST(not is_power_of_2_signed(-3));
    BOOST_TEST(not is_power_of_2_signed(-16));
}

BOOST_AUTO_TEST_CASE(test_dense_vector_allocation) {
    u64 length = 123;
    auto v = calloc_real(length);
    BOOST_TEST(v.size == length);
    for (u64 i = 0; i < v.size; ++i) {
        BOOST_TEST(v.data[i] == 0.0);
    }
    delete_vector(v);
}

BOOST_AUTO_TEST_CASE(test_norm_squared_dense) {
    auto v = calloc_real(4);
    BOOST_TEST(norm_squared(v) == 0);
    v.data[0] = 1;
    BOOST_TEST(norm_squared(v) == 1);
    v.data[1] = 1;
    BOOST_TEST(norm_squared(v) == 2);
    v.data[1] = 2;
    BOOST_TEST(norm_squared(v) == 5);
    delete_vector(v);
}

BOOST_AUTO_TEST_CASE(test_norm_squared_sparse) {
    sparse_vector* v = new sparse_vector{};
    BOOST_TEST(norm_squared(v) == 0);
    v->push_back({0, 1});
    BOOST_TEST(norm_squared(v) == 1);
    v->push_back({1, 1});
    BOOST_TEST(norm_squared(v) == 2);
    (*v)[1] = {1, 2};
    BOOST_TEST(norm_squared(v) == 5);
    delete_vector(v);
}

BOOST_AUTO_TEST_CASE(test_l0_norm_dense) {
    auto v = calloc_real(4);
    BOOST_TEST(l0_norm(v) == 0);
    v.data[0] = 1;
    BOOST_TEST(l0_norm(v) == 1);
    v.data[1] = 1;
    BOOST_TEST(l0_norm(v) == 2);
    v.data[1] = 2;
    BOOST_TEST(l0_norm(v) == 2);
    v.data[2] = 2;
    BOOST_TEST(l0_norm(v) == 3);
    delete_vector(v);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE(preprocessor_tests)


BOOST_AUTO_TEST_CASE(test_dense_random_sign) {
    const u64 vector_dimension = 1 << 8;
    dense_vector v = calloc_real(vector_dimension);
    for (u64 i = 0; i < vector_dimension; ++i) {
        v.data[i] = i;
    }
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto trans = EagerRandomSigns<default_hasher>{vector_dimension, prng};

    auto res = trans.transform(v);

    u64 num_pos = 0;

    for (u64 i = 0; i < vector_dimension; ++i) {
        BOOST_TEST(std::abs(res.data[i]) == (double) i);
        if (!(std::abs(res.data[i]) == (double) i)) {
            std::cerr << "res.data[" << i << "] = " << res.data[i] << std::endl;
        }
        if (res.data[i] > 0) {
            ++num_pos;
        }
    }

    BOOST_TEST_WARN(vector_dimension / 4 <= num_pos);
    BOOST_TEST_WARN(num_pos <= 3 * vector_dimension / 4);

    BOOST_TEST(trans.normalisation_divisor() == 1.0);

    delete_vector(res);
}

BOOST_AUTO_TEST_CASE(test_dense_hadamard_ones) {
    const u64 vector_dimension = 1 << 8;
    dense_vector v = calloc_real(vector_dimension);
    for (u64 i = 0; i < vector_dimension; ++i) {
        v.data[i] = 1;
    }
    auto trans = Hadamard{vector_dimension};

    auto res = trans.transform(v);

    BOOST_TEST(res.data[0] == vector_dimension);
    for (u64 i = 1; i < vector_dimension; ++i) {
        BOOST_TEST(res.data[i] == 0);
    }

    BOOST_TEST(trans.normalisation_divisor() == std::sqrt(vector_dimension));

    delete_vector(res);
}

BOOST_AUTO_TEST_CASE(test_dense_hadamard_misaligned_ei) {
    u64 small_dimension = (u64) 1 << 6;
    u64 original_dimension = 3 * small_dimension;
    auto trans = Hadamard{original_dimension};
    u64 output_dimension = trans.output_dimension();
    BOOST_TEST(output_dimension == 4 * small_dimension);

    for (u64 i = 0; i < original_dimension; ++i) {
        auto v = calloc_real(original_dimension);
        v.data[i] = 1;
        auto res = trans.transform(v);
        BOOST_TEST(std::sqrt(norm_squared(res)) == trans.normalisation_divisor());
        delete_vector(res);
    }
}

BOOST_AUTO_TEST_CASE(test_dense_hadamard_alignment) {
    u64 max_dimension = (u64) 1 << 10;
    for (u64 dim = 4; dim < max_dimension; ++dim) {
        auto trans = Hadamard{dim};
        u64 new_dim = trans.output_dimension();
        BOOST_TEST(dim <= new_dim);
        BOOST_TEST(new_dim < 2 * dim);
    }
}

BOOST_AUTO_TEST_CASE(test_sparse_hadamard_ones) {
    const u64 vector_dimension = 1 << 8;
    sparse_vector* v = new sparse_vector{};
    for (u64 i = 0; i < vector_dimension; ++i) {
        v->push_back(std::make_pair(i, 1.0));
    }
    auto trans = Hadamard{vector_dimension};

    auto res = trans.transform(v);

    BOOST_TEST(res.data[0] == vector_dimension);
    for (u64 i = 1; i < vector_dimension; ++i) {
        BOOST_TEST(res.data[i] == 0);
    }

    BOOST_TEST(trans.normalisation_divisor() == std::sqrt(vector_dimension));

    delete_vector(res);
}

BOOST_AUTO_TEST_CASE(test_sparse_hadamard_misaligned_ei) {
    u64 small_dimension = (u64) 1 << 6;
    u64 original_dimension = 3 * small_dimension;
    auto trans = Hadamard{original_dimension};
    u64 output_dimension = trans.output_dimension();
    BOOST_TEST(output_dimension == 4 * small_dimension);

    for (u64 i = 0; i < original_dimension; ++i) {
        sparse_vector* v = new sparse_vector{{i, 1.0}};
        auto res = trans.transform(v);
        BOOST_TEST(std::sqrt(norm_squared(res)) == trans.normalisation_divisor());
        delete_vector(res);
    }
}

BOOST_AUTO_TEST_CASE(test_sparse_hadamard_alignment) {
    u64 max_dimension = (u64) 1 << 10;
    for (u64 dim = 4; dim < max_dimension; ++dim) {
        auto trans = Hadamard{dim};
        u64 new_dim = trans.output_dimension();
        BOOST_TEST(dim <= new_dim);
        BOOST_TEST(new_dim < 2 * dim);
    }
}


BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE(raw_embedder_tests, * boost::unit_test::tolerance(0.0000001))


BOOST_AUTO_TEST_CASE(test_rawfh_eager_dense_ei) {
    u64 src = (u64) 1 << 6;
    u32 dst = (u64) 1 << 3;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto embedder = RawFeatureHashEager<default_hasher>{src, dst, prng};
    for (u64 i = 0; i < src; ++i) {
        dense_vector v = calloc_real(src);
        v.data[i] = 1.0;
        auto res = embedder.transform(v);
        BOOST_TEST(norm_squared(res) == 1.0);
        BOOST_TEST(l0_norm(res) == 1);
        delete_vector(res);
    }
    BOOST_TEST(embedder.normalisation_divisor() == 1.0);
}

BOOST_AUTO_TEST_CASE(test_rawfh_eager_sparse_ei) {
    u64 src = (u64) 1 << 6;
    u32 dst = (u64) 1 << 3;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto embedder = RawFeatureHashEager<default_hasher>{src, dst, prng};
    for (u64 i = 0; i < src; ++i) {
        sparse_vector* v = new sparse_vector{{i, 1.0}};
        auto res = embedder.transform(v);
        BOOST_TEST(norm_squared(res) == 1.0);
        BOOST_TEST(l0_norm(res) == 1);
        delete_vector(res);
    }
    BOOST_TEST(embedder.normalisation_divisor() == 1.0);
}

BOOST_AUTO_TEST_CASE(test_rawfh_lazy_dense_ei) {
    u64 src = (u64) 1 << 6;
    u32 dst = (u64) 1 << 3;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto embedder = RawFeatureHashLazy<default_hasher>{src, dst, prng};
    for (u64 i = 0; i < src; ++i) {
        dense_vector v = calloc_real(src);
        v.data[i] = 1.0;
        auto res = embedder.transform(v);
        BOOST_TEST(norm_squared(res) == 1.0);
        BOOST_TEST(l0_norm(res) == 1);
        delete_vector(res);
    }
    BOOST_TEST(embedder.normalisation_divisor() == 1.0);
}

BOOST_AUTO_TEST_CASE(test_rawfh_lazy_sparse_ei) {
    u64 src = (u64) 1 << 6;
    u32 dst = (u64) 1 << 3;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto embedder = RawFeatureHashLazy<default_hasher>{src, dst, prng};
    for (u64 i = 0; i < src; ++i) {
        sparse_vector* v = new sparse_vector{{i, 1.0}};
        auto res = embedder.transform(v);
        BOOST_TEST(norm_squared(res) == 1.0);
        BOOST_TEST(l0_norm(res) == 1);
        delete_vector(res);
    }
    BOOST_TEST(embedder.normalisation_divisor() == 1.0);
}

BOOST_AUTO_TEST_CASE(test_randproj_dense_ei) {
    u64 src = (u64) 1 << 6;
    u32 dst = (u64) 1 << 3;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto embedder = RawEagerRandomProjection<default_hasher>{src, dst, prng};
    u64 seen_neg = 0;
    u64 seen_pos = 0;

    for (u64 i = 0; i < src; ++i) {
        dense_vector v = calloc_real(src);
        v.data[i] = 1.0;
        auto res = embedder.transform(v);
        for (u32 j = 0; j < dst; ++j) {
            BOOST_TEST(std::abs(res.data[j]) == 1.0);
            if (res.data[j] < -0.001) {
                ++seen_neg;
            } else if (res.data[j] > 0.001) {
                ++seen_pos;
            }
        }
        delete_vector(res);
    }
    BOOST_TEST(seen_pos > 0);
    BOOST_TEST(seen_neg > 0);
    BOOST_TEST_WARN(src * dst / 4 <= seen_pos);
    BOOST_TEST_WARN(seen_pos <= 3 * src * dst / 4);
    BOOST_TEST(embedder.normalisation_divisor() == std::sqrt(dst));
}

BOOST_AUTO_TEST_CASE(test_randproj_eagersparse_ei) {
    u64 src = (u64) 1 << 6;
    u32 dst = (u64) 1 << 3;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto embedder = RawEagerRandomProjection<default_hasher>{src, dst, prng};
    u64 seen_neg = 0;
    u64 seen_pos = 0;

    for (u64 i = 0; i < src; ++i) {
        sparse_vector* v = new sparse_vector{{i, 1.0}};
        auto res = embedder.transform(v);
        for (u32 j = 0; j < dst; ++j) {
            BOOST_TEST(std::abs(res.data[j]) == 1.0);
            if (res.data[j] < -0.001) {
                ++seen_neg;
            } else if (res.data[j] > 0.001) {
                ++seen_pos;
            }
        }
        delete_vector(res);
    }

    BOOST_TEST(seen_pos > 0);
    BOOST_TEST(seen_neg > 0);
    BOOST_TEST_WARN(src * dst / 4 <= seen_pos);
    BOOST_TEST_WARN(seen_pos <= 3 * src * dst / 4);
    BOOST_TEST(embedder.normalisation_divisor() == std::sqrt(dst));
}

BOOST_AUTO_TEST_CASE(test_sjl_block_dense_ei) {
    u64 src = (u64) 1 << 8;
    u32 dst = (u64) 1 << 3;
    const u32 sparsity = 4;
    typedef ConstantSparsity<sparsity> sstrat;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto embedder = RawEagerSparseBlockJL<default_hasher, sstrat>{src, dst, prng};
    u64 seen_neg = 0;
    u64 seen_pos = 0;
    u64* counts = (u64*) std::calloc(dst, sizeof(u64));

    for (u64 i = 0; i < src; ++i) {
        dense_vector v = calloc_real(src);
        v.data[i] = 1.0;
        auto res = embedder.transform(v);
        BOOST_TEST(norm_squared(res) == sparsity);
        BOOST_TEST(l0_norm(res) == sparsity);
        for (u64 j = 0; j < dst; ++j) {
            if (res.data[j] < -0.001) {
                ++seen_neg;
            } else if (res.data[j] > 0.001) {
                ++seen_pos;
            }
            if (std::abs(res.data[j]) > 0.001) {
                counts[j]++;
            }
        }

        delete_vector(res);
    }
    BOOST_TEST(seen_pos > 0);
    BOOST_TEST(seen_neg > 0);
    BOOST_TEST_WARN(src * sparsity / 4 <= seen_pos);
    BOOST_TEST_WARN(seen_pos <= 3 * src * sparsity / 4);

    for (u64 j = 0; j < dst; ++j) {
        BOOST_TEST(counts[j] > 0);
        BOOST_TEST_WARN((src * sparsity / dst) / 2 <= counts[j]);
        BOOST_TEST_WARN(counts[j] <= 2 * src * sparsity / dst);
    }
    free(counts);

    BOOST_TEST(embedder.normalisation_divisor() == std::sqrt(sparsity));
}

BOOST_AUTO_TEST_CASE(test_sjl_block_dense_unaligned_ei) {
    const u64 src = (u64) 1 << 8;
    const u32 dst = (u64) 1 << 5;
    const u32 sparsity = 3 * (dst >> 3);
    typedef ConstantSparsity<sparsity> sstrat;
    BOOST_TEST_REQUIRE(dst % sparsity != 0);
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto embedder = RawEagerSparseBlockJL<default_hasher, sstrat>{src, dst, prng};
    u64 seen_neg = 0;
    u64 seen_pos = 0;
    u64* counts = (u64*) std::calloc(dst, sizeof(u64));

    for (u64 i = 0; i < src; ++i) {
        dense_vector v = calloc_real(src);
        v.data[i] = 1.0;
        auto res = embedder.transform(v);
        BOOST_TEST(norm_squared(res) == sparsity);
        BOOST_TEST(l0_norm(res) == sparsity);
        for (u64 j = 0; j < dst; ++j) {
            if (res.data[j] < -0.001) {
                ++seen_neg;
            } else if (res.data[j] > 0.001) {
                ++seen_pos;
            }
            if (std::abs(res.data[j]) > 0.001) {
                counts[j]++;
            }
        }

        delete_vector(res);
    }
    BOOST_TEST(seen_pos > 0);
    BOOST_TEST(seen_neg > 0);
    BOOST_TEST_WARN(src * sparsity / 4 <= seen_pos);
    BOOST_TEST_WARN(seen_pos <= 3 * src * sparsity / 4);

    for (u64 j = 0; j < dst; ++j) {
        BOOST_TEST(counts[j] > 0);
        // These counts should be a bit skewed towards the first (small) blocks
        BOOST_TEST_WARN((src * sparsity / dst) / 2 <= counts[j]);
        BOOST_TEST_WARN(counts[j] <= 2 * src * sparsity / dst);
    }
    free(counts);

    BOOST_TEST(embedder.normalisation_divisor() == std::sqrt(sparsity));
}

BOOST_AUTO_TEST_CASE(test_sjl_block_sparse_ei) {
    const u64 src = (u64) 1 << 8;
    const u32 dst = (u64) 1 << 3;
    const u32 sparsity = dst >> 2;
    typedef ConstantSparsity<sparsity> sstrat;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto embedder = RawLazySparseBlockJL<default_hasher, sstrat>{src, dst, prng};
    u64 seen_neg = 0;
    u64 seen_pos = 0;
    u64* counts = (u64*) std::calloc(dst, sizeof(u64));

    for (u64 i = 0; i < src; ++i) {
        sparse_vector* v = new sparse_vector{{i, 1.0}};
        auto res = embedder.transform(v);
        BOOST_TEST(norm_squared(res) == sparsity);
        BOOST_TEST(l0_norm(res) == sparsity);
        for (u64 j = 0; j < dst; ++j) {
            if (res.data[j] < -0.001) {
                ++seen_neg;
            } else if (res.data[j] > 0.001) {
                ++seen_pos;
            }
            if (std::abs(res.data[j]) > 0.001) {
                counts[j]++;
            }
        }
        delete_vector(res);
    }

    BOOST_TEST(seen_pos > 0);
    BOOST_TEST(seen_neg > 0);
    BOOST_TEST_WARN(src * sparsity / 4 <= seen_pos);
    BOOST_TEST_WARN(seen_pos <= 3 * src * sparsity / 4);

    for (u64 j = 0; j < dst; ++j) {
        BOOST_TEST(counts[j] > 0);
        BOOST_TEST_WARN((src * sparsity / dst) / 2 <= counts[j]);
        BOOST_TEST_WARN(counts[j] <= 2 * src * sparsity / dst);
    }
    free(counts);

    BOOST_TEST(embedder.normalisation_divisor() == std::sqrt(sparsity));
}

BOOST_AUTO_TEST_CASE(test_sjl_block_sparse_unaligned_ei) {
    const u64 src = (u64) 1 << 8;
    const u32 dst = (u64) 1 << 5;
    const u32 sparsity = 3 * (dst >> 3);
    BOOST_TEST_REQUIRE(dst % sparsity != 0);
    typedef ConstantSparsity<sparsity> sstrat;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto embedder = RawLazySparseBlockJL<default_hasher, sstrat>{src, dst, prng};
    u64 seen_neg = 0;
    u64 seen_pos = 0;
    u64* counts = (u64*) std::calloc(dst, sizeof(u64));

    for (u64 i = 0; i < src; ++i) {
        auto v = new sparse_vector{{i, 1.0}};
        auto res = embedder.transform(v);
        BOOST_TEST(norm_squared(res) == sparsity);
        BOOST_TEST(l0_norm(res) == sparsity);
        for (u64 j = 0; j < dst; ++j) {
            if (res.data[j] < -0.001) {
                ++seen_neg;
            } else if (res.data[j] > 0.001) {
                ++seen_pos;
            }
            if (std::abs(res.data[j]) > 0.001) {
                counts[j]++;
            }
        }

        delete_vector(res);
    }
    BOOST_TEST(seen_pos > 0);
    BOOST_TEST(seen_neg > 0);
    BOOST_TEST_WARN(src * sparsity / 4 <= seen_pos);
    BOOST_TEST_WARN(seen_pos <= 3 * src * sparsity / 4);

    for (u64 j = 0; j < dst; ++j) {
        BOOST_TEST(counts[j] > 0);
        // These counts should be a bit skewed towards the first (small) blocks
        BOOST_TEST_WARN((src * sparsity / dst) / 2 <= counts[j]);
        BOOST_TEST_WARN(counts[j] <= 2 * src * sparsity / dst);
    }
    free(counts);

    BOOST_TEST(embedder.normalisation_divisor() == std::sqrt(sparsity));
}

BOOST_AUTO_TEST_CASE(test_sjl_dks_dense_ei) {
    const u64 src = (u64) 1 << 8;
    const u32 dst = (u64) 1 << 3;
    const u32 sparsity = dst >> 2;
    typedef ConstantSparsity<sparsity> sstrat;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto embedder = RawEagerSparseDKSJL<default_hasher, sstrat>{src, dst, prng};
    u64 seen_neg = 0;
    u64 seen_pos = 0;
    u64* counts = (u64*) std::calloc(dst, sizeof(u64));

    for (u64 i = 0; i < src; ++i) {
        dense_vector v = calloc_real(src);
        v.data[i] = 1.0;
        auto res = embedder.transform(v);
        BOOST_TEST(norm_squared(res) <= sparsity * sparsity);
        BOOST_TEST(l0_norm(res) <= sparsity);
        for (u64 j = 0; j < dst; ++j) {
            if (res.data[j] < -0.001) {
                ++seen_neg;
            } else if (res.data[j] > 0.001) {
                ++seen_pos;
            }
            if (std::abs(res.data[j]) > 0.001) {
                counts[j]++;
            }
        }
        delete_vector(res);
    }

    BOOST_TEST(seen_pos > 0);
    BOOST_TEST(seen_neg > 0);
    BOOST_TEST_WARN(src * sparsity / 4 <= seen_pos);
    BOOST_TEST_WARN(seen_pos <= 3 * src * sparsity / 4);

    for (u64 j = 0; j < dst; ++j) {
        BOOST_TEST(counts[j] > 0);
        BOOST_TEST_WARN((src * sparsity / dst) / 2 <= counts[j]);
        BOOST_TEST_WARN(counts[j] <= 2 * src * sparsity / dst);
    }
    free(counts);

    BOOST_TEST(embedder.normalisation_divisor() == std::sqrt(sparsity));
}

BOOST_AUTO_TEST_CASE(test_sjl_dks_sparse_ei) {
    const u64 src = (u64) 1 << 8;
    const u32 dst = (u64) 1 << 3;
    const u32 sparsity = dst >> 2;
    typedef ConstantSparsity<sparsity> sstrat;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto embedder = RawLazySparseDKSJL<default_hasher, sstrat>{src, dst, prng};
    u64 seen_neg = 0;
    u64 seen_pos = 0;
    u64* counts = (u64*) std::calloc(dst, sizeof(u64));

    for (u64 i = 0; i < src; ++i) {
        sparse_vector* v = new sparse_vector{{i, 1.0}};
        auto res = embedder.transform(v);
        BOOST_TEST(norm_squared(res) <= sparsity * sparsity);
        BOOST_TEST(l0_norm(res) <= sparsity);
        for (u64 j = 0; j < dst; ++j) {
            if (res.data[j] < -0.001) {
                ++seen_neg;
            } else if (res.data[j] > 0.001) {
                ++seen_pos;
            }
            if (std::abs(res.data[j]) > 0.001) {
                counts[j]++;
            }
        }
        delete_vector(res);
    }

    BOOST_TEST(seen_pos > 0);
    BOOST_TEST(seen_neg > 0);
    BOOST_TEST_WARN(src * sparsity / 4 <= seen_pos);
    BOOST_TEST_WARN(seen_pos <= 3 * src * sparsity / 4);

    for (u64 j = 0; j < dst; ++j) {
        BOOST_TEST(counts[j] > 0);
        BOOST_TEST_WARN((src * sparsity / dst) / 2 <= counts[j]);
        BOOST_TEST_WARN(counts[j] <= 2 * src * sparsity / dst);
    }
    free(counts);

    BOOST_TEST(embedder.normalisation_divisor() == std::sqrt(sparsity));
}

BOOST_AUTO_TEST_CASE(test_toeplitz_dense_ei) {
    u64 src = (u64) 1 << 6;
    u32 dst = (u64) 1 << 3;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto embedder = RawEagerToeplitz<default_hasher>{src, dst, prng};
    u64 seen_neg = 0;
    u64 seen_pos = 0;

    for (u64 i = 0; i < src; ++i) {
        dense_vector v = calloc_real(src);
        v.data[i] = 1.0;
        auto res = embedder.transform(v);
        for (u32 j = 0; j < dst; ++j) {
            double abs_val = std::abs(res.data[j]) / embedder.normalisation_divisor();
            BOOST_TEST(abs_val == 1 / std::sqrt(dst));
            if (res.data[j] < -0.001) {
                ++seen_neg;
            } else if (res.data[j] > 0.001) {
                ++seen_pos;
            }
        }
        delete_vector(res);
    }
    BOOST_TEST(seen_pos > 0);
    BOOST_TEST(seen_neg > 0);
    BOOST_TEST_WARN(src * dst / 4 <= seen_pos);
    BOOST_TEST_WARN(seen_pos <= 3 * src * dst / 4);
}

BOOST_AUTO_TEST_CASE(test_lwt_dense_ei) {
    u64 source_dimension = (u64) 1 << 4;
    u64 seed_rows = 3;
    u64 seed_cols = 4;
    std::string matrix_s = ("++--"
                            "+-+-"
                            "+--+");
    gsl_matrix* seed_matrix = pm_string2gsl_matrix(matrix_s, seed_rows, seed_cols);
    auto lwt = RawEagerLeanWalshTransform(source_dimension, seed_matrix);
    u64 target_dimension = lwt.output_dimension();
    BOOST_TEST_REQUIRE(target_dimension == 9);

    dense_vector e0 = calloc_real(source_dimension);
    e0.data[0] = 1;
    auto actual0 = lwt.transform(e0);
    std::string expected0_s = "1 1 1 1 1 1 1 1 1";
    auto expected0 = string2dense_vector(expected0_s, target_dimension);
    BOOST_CHECK_EQUAL_COLLECTIONS(expected0.data, expected0.data + target_dimension,
                                  actual0.data, actual0.data + target_dimension);
    delete_vector(expected0);
    delete_vector(actual0);

    dense_vector e1 = calloc_real(source_dimension);
    e1.data[1] = 1;
    auto actual1 = lwt.transform(e1);
    std::string expected1_s = "1 -1 -1 1 -1 -1 1 -1 -1";
    auto expected1 = string2dense_vector(expected1_s, target_dimension);
    BOOST_CHECK_EQUAL_COLLECTIONS(expected1.data, expected1.data + target_dimension,
                                  actual1.data, actual1.data + target_dimension);
    delete_vector(expected1);
    delete_vector(actual1);

    dense_vector e6 = calloc_real(source_dimension);
    e6.data[6] = 1;
    auto actual6 = lwt.transform(e6);
    std::string expected6_s = "-1 1 -1 1 -1 1 1 -1 1";
    auto expected6 = string2dense_vector(expected6_s, target_dimension);
    BOOST_CHECK_EQUAL_COLLECTIONS(expected6.data, expected6.data + target_dimension,
                                  actual6.data, actual6.data + target_dimension);
    delete_vector(expected6);
    delete_vector(actual6);

    dense_vector e15 = calloc_real(source_dimension);
    e15.data[15] = 1;
    auto actual15 = lwt.transform(e15);
    std::string expected15_s = "1 1 -1 1 1 -1 -1 -1 1";
    auto expected15 = string2dense_vector(expected15_s, target_dimension);
    BOOST_CHECK_EQUAL_COLLECTIONS(expected15.data, expected15.data + target_dimension,
                                  actual15.data, actual15.data + target_dimension);
    delete_vector(expected15);
    delete_vector(actual15);

    BOOST_TEST(lwt.normalisation_divisor() == std::sqrt(target_dimension));
}

BOOST_AUTO_TEST_CASE(test_lwt_dense_other) {
    u64 source_dimension = (u64) 1 << 4;
    u64 seed_rows = 3;
    u64 seed_cols = 4;
    std::string matrix_s = ("++--"
                            "+-+-"
                            "+--+");
    gsl_matrix* seed_matrix = pm_string2gsl_matrix(matrix_s, seed_rows, seed_cols);
    auto lwt = RawEagerLeanWalshTransform(source_dimension, seed_matrix);
    u64 target_dimension = lwt.output_dimension();
    BOOST_TEST_REQUIRE(target_dimension == 9);

    std::string v_s = "1 0 1 0 0 10 10 0 100 100 100 0 1000 0 0 1000";
    dense_vector v = string2dense_vector(v_s, source_dimension);
    auto actual = lwt.transform(v);
    std::string expected_s = "-100 -98 -1920 100 102 -2080 -100 -98 2120";
    auto expected = string2dense_vector(expected_s, target_dimension);
    BOOST_CHECK_EQUAL_COLLECTIONS(expected.data, expected.data + target_dimension,
                                  actual.data, actual.data + target_dimension);
    delete_vector(expected);
    delete_vector(actual);
}


BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE(vector_generator_tests, * boost::unit_test::tolerance(0.0000001))


BOOST_AUTO_TEST_CASE(test_cluster_generator_sparse) {
    u64 dim = (u64) 1 << 6;
    u64 num_entries = (u64) 1 << 3;
    auto pos_prng = new prng_t{"seeds/2019-01-01.bin"};
    auto sign_prng = new prng_t{"seeds/2019-01-01.bin", 1};
    auto generator = ClusterGenerator<default_hasher>{dim, num_entries,
                                                              pos_prng, sign_prng};
    for (u64 i = 0; i < dim / num_entries; ++i) {
        sparse_vector* v;
        generator.gen_vector(v);
        BOOST_TEST(v->size() == num_entries);
        for (auto const &pair : *v) {
            BOOST_TEST(pair.first < dim);
            BOOST_TEST(std::abs(pair.second) == 1.0);
        }
        delete v;
    }
}

BOOST_AUTO_TEST_CASE(test_cluster_generator_dense) {
    u64 dim = (u64) 1 << 6;
    u64 num_entries = (u64) 1 << 3;
    auto pos_prng = new prng_t{"seeds/2019-01-01.bin"};
    auto sign_prng = new prng_t{"seeds/2019-01-01.bin", 1};
    auto generator = ClusterGenerator<default_hasher>{dim, num_entries,
                                                              pos_prng, sign_prng};
    for (u64 i = 0; i < dim / num_entries; ++i) {
        dense_vector v;
        generator.gen_vector(v);
        BOOST_TEST(l0_norm(v) == num_entries);
        BOOST_TEST(linfinity_norm(v) == 1);
        BOOST_TEST(norm_squared(v) == num_entries);
        delete_vector(v);
    }
}

BOOST_AUTO_TEST_CASE(test_cluster_ones_generator_sparse) {
    u64 dim = (u64) 1 << 10;
    u64 num_entries = (u64) 1 << 3;
    auto generator = ClusterOnesGenerator{dim, num_entries};
    for (u64 i = 0; i < dim / num_entries; ++i) {
        sparse_vector* v;
        generator.gen_vector(v);
        BOOST_TEST(v->size() == num_entries);
        for (auto const &pair : *v) {
            BOOST_TEST(i * num_entries <= pair.first);
            BOOST_TEST(pair.first < (i + 1) * num_entries);
            BOOST_TEST(pair.second == 1.0);
        }
        delete v;
    }
}

BOOST_AUTO_TEST_CASE(test_cluster_ones_generator_dense) {
    u64 dim = (u64) 1 << 10;
    u64 num_entries = (u64) 1 << 3;
    auto generator = ClusterOnesGenerator{dim, num_entries};
    for (u64 i = 0; i < dim / num_entries; ++i) {
        dense_vector v;
        generator.gen_vector(v);
        for (u64 j = 0; j < i * num_entries; ++j) {
            BOOST_TEST(v.data[j] == 0.0);
        }
        for (u64 j = i * num_entries; j < (i + 1) * num_entries; ++j) {
            BOOST_TEST(v.data[j] == 1.0);
        }
        for (u64 j = (i + 1) * num_entries; j < dim; ++j) {
            BOOST_TEST(v.data[j] == 0.0);
        }
        delete_vector(v);
    }
}

BOOST_AUTO_TEST_CASE(test_hadamard_cluster_ones_generator_dense) {
    u64 log_dim = 10;
    u64 dim = (u64) 1 << log_dim;
    u64 num_entries = (u64) 1 << 3;
    auto cluster_generator = new ClusterOnesGenerator{dim, num_entries};
    auto hadamard_preprocessor = new Hadamard{dim};
    auto hadamard_generator = ProcessedGenerator<dense_vector>{dim, cluster_generator, hadamard_preprocessor};
    for (u64 i = 0; i < dim / num_entries; ++i) {
        dense_vector generated;
        hadamard_generator.gen_vector(generated);
        auto v = hadamard_preprocessor->transform(generated);

        for (u64 j = 0; j < i * num_entries; ++j) {
            BOOST_TEST(v.data[j] == 0.0);
        }
        for (u64 j = i * num_entries; j < (i + 1) * num_entries; ++j) {
            BOOST_TEST(v.data[j] == hadamard_preprocessor->normalisation_divisor());
        }
        for (u64 j = (i + 1) * num_entries; j < dim; ++j) {
            BOOST_TEST(v.data[j] == 0.0);
        }

        delete_vector(v);
    }
}

BOOST_AUTO_TEST_CASE(test_uniform_generator_dense) {
    u64 dim = (u64) 1 << 4;
    u64 num_vectors = 100;
    u64 num_buckets = 10;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto ug = UniformGenerator<default_hasher>{dim, prng};

    u64* counts = (u64*) std::calloc(num_buckets, sizeof(u64));

    for (u64 i = 0; i < num_vectors; ++i) {
        dense_vector v;
        ug.gen_vector(v);
        for (u64 j = 0; j < dim; ++j) {
            BOOST_TEST(-1 <= v.data[j]);
            BOOST_TEST(v.data[j] <= 1);
            u64 idx = (u64) ((1.0 + v.data[j]) * (num_buckets / 2));
            if (idx == num_buckets) {
                --idx;
            }
            counts[idx]++;
        }
        delete_vector(v);
    }

    u64 avg = num_vectors * dim / num_buckets;
    for (u64 h = 0; h < num_buckets; ++ h) {
        BOOST_TEST(counts[h] > 0);
        BOOST_TEST_WARN(avg / 2 <= counts[h]);
        BOOST_TEST_WARN(counts[h] <= 2 * avg);
    }

    free(counts);
}

BOOST_AUTO_TEST_CASE(test_perturbed_uniform_generator_dense) {
    u64 dim = (u64) 1 << 4;
    u64 num_vectors = (u64) 1 << 4;
    u64 num_centers = 4;
    double l1_radius = 0.01;
    auto prng = new prng_t{"seeds/2019-01-01.bin"};
    auto pg = PerturbedUniformGenerator<default_hasher>{dim, num_centers, l1_radius, prng};

    for (u64 i = 0; i < num_vectors; ++i) {
        dense_vector v;
        pg.gen_vector(v);
        for (u64 j = 0; j < dim; ++j) {
            BOOST_TEST(-1 - l1_radius <= v.data[j]);
            BOOST_TEST(v.data[j] <= 1 + l1_radius);
        }
        delete_vector(v);
    }
}

BOOST_AUTO_TEST_CASE(test_subcube_corner_generator_sparse) {
    u64 dim = (u64) 1 << 8;
    u32 sparsity = 3;
    u64 num_vectors = (u64) 1 << 5;

    SubcubeCornerGenerator gen{dim, sparsity};
    for (u64 i = 0; i < num_vectors; ++i) {
        sparse_vector* v;
        gen.gen_vector(v);
        BOOST_TEST(v->size() == sparsity);
        for (auto const &pair : *v) {
            BOOST_TEST(std::abs(pair.second) == 1.0);
        }
        delete_vector(v);
    }
}

BOOST_AUTO_TEST_CASE(test_subcube_corner_generator_dense) {
    u64 dim = (u64) 1 << 8;
    u32 sparsity = 3;
    u64 num_vectors = (u64) 1 << 5;

    SubcubeCornerGenerator gen{dim, sparsity};
    for (u64 i = 0; i < num_vectors; ++i) {
        dense_vector v;
        gen.gen_vector(v);
        BOOST_TEST_REQUIRE(v.size == dim);
        BOOST_TEST(l0_norm(v) == sparsity);
        BOOST_TEST(linfinity_norm(v) == 1.0);
        BOOST_TEST(norm_squared(v) == sparsity);
        delete_vector(v);
    }
}

BOOST_AUTO_TEST_CASE(test_fixed_corner_generator_dense) {
    for (u64 dim = 4; dim <= 256; dim *= 2) {
        for (u32 sparsity = 1; sparsity < dim; ++sparsity) {
            FixedCornerGenerator gen{dim, sparsity};
            dense_vector v;
            gen.gen_vector(v);
            BOOST_TEST_REQUIRE(v.size == dim);
            BOOST_TEST(l0_norm(v) == sparsity);
            BOOST_TEST(linfinity_norm(v) == 1.0);
            BOOST_TEST(norm_squared(v) == sparsity);
            delete_vector(v);
        }
    }
}


BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE(vector_parser_tests)


BOOST_AUTO_TEST_CASE(test_dexter_parser) {
    sparse_vector expected_u{{1, 23}, {4, 56}, {7, 8}, {123, 321}};
    sparse_vector expected_v{{3, 7}, {4, 2}, {8, 10}};
    sparse_vector expected_w{};
    DexterParser dp{"dummy_data/dexter_dummy.data"};

    auto u = dp.next_vector();
    test_sparse_vector_equals(u, &expected_u);
    delete u;

    auto v = dp.next_vector();
    test_sparse_vector_equals(v, &expected_v);
    delete v;

    auto w = dp.next_vector();
    test_sparse_vector_equals(w, &expected_w);
    delete w;
}


BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE(full_embedder_tests, * boost::unit_test::tolerance(0.0000001))


BOOST_AUTO_TEST_CASE(test_fjlt_eager_ei) {
    u64 src = (u64) 1 << 6;
    u32 dst = (u64) 1 << 4;
    auto sign_prng = new prng_t{"seeds/2019-01-01.bin"};
    auto sparse_prng = new prng_t{"seeds/2019-01-01.bin", 1};
    auto jl = EagerFJLT<default_hasher, default_jlt_sparsity>{src, dst,
                                                              sign_prng, sparse_prng};
    u64 good_norms = 0;

    for (u64 i = 0;  i < src; ++i) {
        auto v = calloc_real(src);
        v.data[i] = 1;
        auto res = jl.transform(v);
        double norm = std::sqrt(norm_squared(res));
        if (0.75 <= norm and norm <= 1.25) {
            ++good_norms;
        }
        delete_vector(res);
    }

    BOOST_TEST(good_norms > 0);
    BOOST_TEST_WARN(3 * src / 5 <= good_norms);

    BOOST_TEST(jl.normalisation_divisor() == 1.0);
}

BOOST_AUTO_TEST_CASE(test_fjlt_eager_unaligned_ei) {
    u64 src = 3 * ((u64) 1 << 6);
    u32 dst = (u64) 1 << 5;
    auto sign_prng = new prng_t{"seeds/2019-01-01.bin"};
    auto sparse_prng = new prng_t{"seeds/2019-01-01.bin", 1};
    auto jl = EagerFJLT<default_hasher, default_jlt_sparsity>{src, dst,
                                                              sign_prng, sparse_prng};
    u64 good_norms = 0;

    for (u64 i = 0;  i < src; ++i) {
        auto v = calloc_real(src);
        v.data[i] = 1;
        auto res = jl.transform(v);
        double norm = std::sqrt(norm_squared(res));
        if (0.75 <= norm and norm <= 1.25) {
            ++good_norms;
        }
        delete_vector(res);
    }

    BOOST_TEST(good_norms > 0);
    BOOST_TEST_WARN(3 * src / 5 <= good_norms);

    BOOST_TEST(jl.normalisation_divisor() == 1.0);
}

BOOST_AUTO_TEST_CASE(test_lwt_dense_ei) {
    u64 src = (u64) 1 << 6;
    u32 dst = (u64) 1 << 4;
    auto sign_prng = new prng_t{"seeds/2019-01-01.bin"};
    auto fjlt_sign_prng = new prng_t{"seeds/2019-01-01.bin", 1};
    auto fjlt_sparse_prng = new prng_t{"seeds/2019-01-01.bin", 2};

    u64 seed_rows = 3;
    u64 seed_cols = 4;
    std::string matrix_s = ("++--"
                            "+-+-"
                            "+--+");
    gsl_matrix* seed_matrix = pm_string2gsl_matrix(matrix_s, seed_rows, seed_cols);


    auto lwt = EagerLeanWalshTransform<default_hasher, default_jlt_sparsity>{src, dst, seed_matrix, sign_prng, fjlt_sign_prng, fjlt_sparse_prng};
    u64 good_norms = 0;

    for (u64 i = 0;  i < src; ++i) {
        auto v = calloc_real(src);
        v.data[i] = 1;
        auto res = lwt.transform(v);
        double norm = std::sqrt(norm_squared(res));
        if (0.75 <= norm and norm <= 1.25) {
            ++good_norms;
        }
        delete_vector(res);
    }

    BOOST_TEST(good_norms > 0);
    BOOST_TEST_WARN(3 * src / 5 <= good_norms);

    BOOST_TEST(lwt.normalisation_divisor() == 1.0);
}

BOOST_AUTO_TEST_CASE(test_lwt_dense_unaligned_ei) {
    u64 src = 3 * ((u64) 1 << 6);
    u32 dst = (u64) 1 << 5;
    auto sign_prng = new prng_t{"seeds/2019-01-01.bin"};
    auto fjlt_sign_prng = new prng_t{"seeds/2019-01-01.bin", 1};
    auto fjlt_sparse_prng = new prng_t{"seeds/2019-01-01.bin", 2};

    u64 seed_rows = 3;
    u64 seed_cols = 4;
    std::string matrix_s = ("++--"
                            "+-+-"
                            "+--+");
    gsl_matrix* seed_matrix = pm_string2gsl_matrix(matrix_s, seed_rows, seed_cols);


    auto lwt = EagerLeanWalshTransform<default_hasher, default_jlt_sparsity>{src, dst, seed_matrix, sign_prng, fjlt_sign_prng, fjlt_sparse_prng};
    u64 good_norms = 0;

    for (u64 i = 0;  i < src; ++i) {
        auto v = calloc_real(src);
        v.data[i] = 1;
        auto res = lwt.transform(v);
        double norm = std::sqrt(norm_squared(res));
        if (0.75 <= norm and norm <= 1.25) {
            ++good_norms;
        }
        delete_vector(res);
    }

    BOOST_TEST(good_norms > 0);
    BOOST_TEST_WARN(3 * src / 5 <= good_norms);

    BOOST_TEST(lwt.normalisation_divisor() == 1.0);
}


BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE(embedder_factory_tests, * boost::unit_test::tolerance(0.0000001))


#define JLT_FACTORY_TEST(test_name, factory_t, input_t)                 \
BOOST_AUTO_TEST_CASE(test_name) {                                       \
    u64 src = (u64) 1 << 12;                                            \
    u32 dst = (u64) 1 << 8;                                             \
    u32 num_vectors = 1 << 6;                                           \
    u32 input_sparsity = 1 << 6;                                        \
    double eps = 0.3;                                                   \
    factory_t jlt_maker;                                                \
    const std::string seed_file_path = "seeds/2019-01-01.bin";          \
    auto jlt = jlt_maker.mk_jlt(seed_file_path, src, dst, 0);           \
    auto gen = ClusterOnesGenerator{src, input_sparsity};               \
                                                                        \
    u64 good_norms = 0;                                                 \
    for (u32 i = 0; i < num_vectors; ++i) {                             \
        input_t v;                                                      \
        gen.gen_vector(v);                                              \
        double expected_norm = norm_squared(v);                         \
        auto res = jlt->transform(v);                                   \
        double actual_norm = norm_squared(res);                         \
        if ((1 - eps) * expected_norm <= actual_norm                    \
            and actual_norm <= (1 + eps) * expected_norm) {             \
            ++good_norms;                                               \
        }                                                               \
        delete_vector(res);                                             \
    }                                                                   \
                                                                        \
    BOOST_TEST(good_norms > 0);                                         \
    BOOST_TEST_WARN(num_vectors / 4 <= good_norms);                     \
    BOOST_TEST(jlt->normalisation_divisor() == 1.0);                    \
                                                                        \
    delete jlt;                                                         \
}

JLT_FACTORY_TEST(test_eager_randproj_dense, BOOST_IDENTITY_TYPE((EagerRandomProjectionFactory<dense_vector, default_hasher>)), dense_vector)

JLT_FACTORY_TEST(test_eager_toeplitz_dense, BOOST_IDENTITY_TYPE((EagerToeplitzJLFactory<default_hasher>)), dense_vector)

JLT_FACTORY_TEST(test_eager_fjlt_dense, BOOST_IDENTITY_TYPE((EagerFJLTFactory<default_hasher, default_jlt_sparsity>)), dense_vector)

JLT_FACTORY_TEST(test_eager_lwt_dense, BOOST_IDENTITY_TYPE((EagerLWTFactory<default_hasher, default_jlt_sparsity, 3, 4>)), dense_vector)

JLT_FACTORY_TEST(test_eager_grhd_dense, BOOST_IDENTITY_TYPE((EagerGRHDFactory<default_hasher, default_middle_dimension>)), dense_vector)

JLT_FACTORY_TEST(test_eager_sparsejl_block_dense, BOOST_IDENTITY_TYPE((SparseJLFactory<RawEagerSparseBlockJL<default_hasher, default_jlt_sparsity>, dense_vector>)), dense_vector)

JLT_FACTORY_TEST(test_eager_sparsejl_dks_dense, BOOST_IDENTITY_TYPE((SparseJLFactory<RawEagerSparseDKSJL<default_hasher, default_jlt_sparsity>, dense_vector>)), dense_vector)

JLT_FACTORY_TEST(test_eager_fh_dense, BOOST_IDENTITY_TYPE((EagerFHFactory<dense_vector, default_hasher>)), dense_vector)

JLT_FACTORY_TEST(test_lazy_fh_dense, BOOST_IDENTITY_TYPE((LazyFHFactory<dense_vector, default_hasher>)), dense_vector)

JLT_FACTORY_TEST(test_eager_randproj_sparse, BOOST_IDENTITY_TYPE((EagerRandomProjectionFactory<sparse_vector*, default_hasher>)), sparse_vector*)

JLT_FACTORY_TEST(test_lazy_sparsejl_block_sparse, BOOST_IDENTITY_TYPE((SparseJLFactory<RawLazySparseBlockJL<default_hasher, default_jlt_sparsity>, sparse_vector*>)), sparse_vector*)

JLT_FACTORY_TEST(test_lazy_sparsejl_dks_sparse, BOOST_IDENTITY_TYPE((SparseJLFactory<RawLazySparseDKSJL<default_hasher, default_jlt_sparsity>, sparse_vector*>)), sparse_vector*)

JLT_FACTORY_TEST(test_eager_fh_sparse, BOOST_IDENTITY_TYPE((EagerFHFactory<sparse_vector*, default_hasher>)), sparse_vector*)

JLT_FACTORY_TEST(test_lazy_fh_sparse, BOOST_IDENTITY_TYPE((LazyFHFactory<sparse_vector*, default_hasher>)), sparse_vector*)


BOOST_AUTO_TEST_SUITE_END()
BOOST_AUTO_TEST_SUITE(experiment_runner_tests, * boost::unit_test::tolerance(0.0000001))

BOOST_AUTO_TEST_CASE(test_measure_embedding_time_sparse) {
    u32 input_sparsity = 1 << 3;
    u32 num_measurements = 1 << 6;
    u32 num_warmups = 1 << 3;
    u64 src = input_sparsity * (num_measurements + num_warmups);
    u32 dst = (u64) 1 << 6;
    EagerFHFactory<sparse_vector*, default_hasher> jlt_maker;
    ClusterOnesGeneratorFactory gen_maker;
    const std::string jlt_seed_file_path = "seeds/2019-01-01.bin";
    const std::string data_seed_file_path = "seeds/2019-01-02.bin";
    auto jlt = jlt_maker.mk_jlt(jlt_seed_file_path, src, dst, 0);
    auto gen = gen_maker.mk_gen(data_seed_file_path, input_sparsity);
    auto measurements = measure_embedding_time<sparse_vector*>(jlt, gen, num_measurements, num_warmups);

    BOOST_TEST(measurements->size() == num_measurements);
    for (auto &x : *measurements) {
        BOOST_TEST(x >= 0.0);
        BOOST_WARN(x < 1.0);
    }

    delete measurements;
}

BOOST_AUTO_TEST_CASE(test_measure_embedding_time_dense) {
    u64 src = (u64) 1 << 8;
    u32 dst = (u64) 1 << 6;
    u32 num_measurements = 1 << 6;
    u32 num_warmups = 1 << 3;
    EagerFHFactory<dense_vector, default_hasher> jlt_maker;
    UniformGeneratorFactory<default_hasher> gen_maker;
    const std::string jlt_seed_file_path = "seeds/2019-01-01.bin";
    const std::string data_seed_file_path = "seeds/2019-01-02.bin";
    auto jlt = jlt_maker.mk_jlt(jlt_seed_file_path, src, dst, 0);
    auto gen = gen_maker.mk_gen(data_seed_file_path, src);
    auto measurements = measure_embedding_time<dense_vector>(jlt, gen, num_measurements, num_warmups);

    BOOST_TEST(measurements->size() == num_measurements);
    for (auto &x : *measurements) {
        BOOST_TEST(x >= 0.0);
        BOOST_WARN(x < 1.0);
    }

    delete measurements;
}


BOOST_AUTO_TEST_SUITE_END()


struct FFTWCleanupFixture {
    FFTWCleanupFixture() {
    }
    ~FFTWCleanupFixture() {
        fftw_cleanup();
    }
};

BOOST_TEST_GLOBAL_FIXTURE(FFTWCleanupFixture);
