#pragma once

#include <string>
#include <sstream>
#include <stdexcept>
#include <gsl/gsl_vector.h>
#include "types.hpp"
#include "transformer.hpp"
#include "prng.hpp"
#include "random_sign_generator.hpp"


template<class hasher_t>
class EagerRandomSigns: public Transformer<dense_vector, dense_vector> {
    const u64 vector_dimension;
    gsl_vector* const signs;
    std::string sign_gen_name;

public:
    using prng_t = typename hasher_t::prng_t;

    EagerRandomSigns(u64 vector_dimension, prng_t* prng) :
        vector_dimension(vector_dimension),
        signs(gsl_vector_alloc(vector_dimension)) {

        HashingSignGenerator<hasher_t> sign_gen{prng};
        for (u32 i = 0; i < vector_dimension; ++i) {
            gsl_vector_set(signs, i, sign_gen.next_sign());
        }
        sign_gen_name = sign_gen.name();
    }

    ~EagerRandomSigns() {
        gsl_vector_free(signs);
    }

    dense_vector transform(dense_vector x) const {
        assert(x.size == vector_dimension);
        gsl_vector_view x_gsl_view = gsl_vector_view_array(x.data, vector_dimension);
        gsl_vector_mul(&x_gsl_view.vector, signs);
        return x;
    }

    u64 output_dimension() const {
        return vector_dimension;
    }

    double normalisation_divisor() const {
        return 1;
    }

    std::string name() const {
        std::stringstream ss;
        ss << "(EagerRandomSigns " << sign_gen_name << ")";
        return ss.str();
    }
};
