from collections import namedtuple
import glob
import matplotlib.pyplot as plt
import datetime
import argparse

from parameters import figure_label_size as label_size, figure_size


TimeDatum = namedtuple('TimeDatum',
                       ['dim', 'min', 'p10', 'med', 'p90', 'max'])
DataHeader = namedtuple('DataHeader',
                        ['label', 'target_dimension'])
PlotConfig = namedtuple('PlotConfig',
                        ['xlabel', 'title_format'])

data_type2plot_config = {'dense': PlotConfig(r'$d$', r'Dense embedders, $m = {0}$'),
                         'sparse': PlotConfig(r'$|x|_0$', r'Sparse embedders, $m = {0}$')}


def parse_output(filename):
    print(f'Parsing {filename}', end='\r')
    time_data = []
    label = filename
    target_dimension = None

    with open(filename) as f:
        line = f.readline().strip()

        while line.startswith('#') or line == '':
            if 'label:' in line:
                label = line.split(':')[1].strip()
            elif 'target dimension:' in line:
                target_dimension = int(line.split(':')[1])
            line = f.readline().strip()

        while line != '' and not line.startswith('#'):
            dim, min, p10, med, p90, max = map(float, line.split())
            dim = int(dim)
            time_data.append(TimeDatum(dim, min, p10, med, p90, max))
            line = f.readline().strip()

    return DataHeader(label, target_dimension), time_data


def make_plot_fig(config, target_dimension):
    fig = plt.figure(figsize=figure_size)
    ax = fig.add_subplot()

    ax.set_xlabel(config.xlabel, fontsize=label_size)
    ax.set_ylabel(r'Time (s)', fontsize=label_size)
    ax.set_title(config.title_format.format(target_dimension))

    return fig, ax


def plot_time_experiment(data_type='dense', use_markers=False):
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')

    dst2fig = dict()
    plot_config = data_type2plot_config[data_type]

    data_folder = '../experiment_output/'
    pattern = f'{data_folder}time_{data_type}_*.txt'
    paths = sorted(glob.glob(pattern))
    if len(paths) == 0:
        print(f'Error could not find any data files, glob pattern: {pattern}')

    if use_markers:
        markers = 'x^sod2|*+D3vp.<2PX'
        if len(markers) < len(paths):
            print('Warning: There are more plots than I prepared markers for!')
    else:
        markers = ''

    for path, marker in zip(paths, markers + ' ' * len(paths)):
        header, data = parse_output(path)
        target_dimension = header.target_dimension
        if target_dimension in dst2fig:
            fig, ax = dst2fig[target_dimension]
        else:
            fig, ax = make_plot_fig(plot_config, target_dimension)
            dst2fig[target_dimension] = fig, ax

        xs, ys, low_errs, hi_errs = [], [], [], []
        for datum in data:
            xs.append(datum.dim)
            ys.append(datum.med)
        ax.loglog(xs, ys, label=header.label, basex=10, basey=10, marker=marker)

    for target_dimension, (fig, ax) in dst2fig.items():
        ax.legend()
        out_id = str(datetime.datetime.now().isoformat('-'))
        fig.savefig(f'time_{data_type}_{target_dimension:05}_{out_id}.pdf',
                    bbox_inches='tight', dpi='figure', transparent=True)
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='A utility for generating plots for running time experiments')
    parser.add_argument('-d', '--datatype',
                        action='store', dest='data_type', choices=['dense', 'sparse'],
                        help='Choose which experiment data type to generate plot for')
    parser.add_argument('-m', '--markers',
                        action='store_true', dest='use_markers',
                        help='Add data point markers to plots')

    args = parser.parse_args()
    if args.data_type is None:
        data_type = input('Data type: ')
    else:
        data_type = args.data_type

    plot_time_experiment(data_type=data_type, use_markers=args.use_markers)
