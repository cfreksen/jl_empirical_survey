#!/usr/bin/env python3

"""Convert text to bags of words."""

import re
import struct
from collections import Counter


def str2dict(text):
    # Remove punctuation and split
    text_pure = re.split(r'[ \t\r\n,.?!-:;()[\]{}"\']+', text.lower())
    res = Counter(text_pure)
    del res['']
    return res


def dict2vector(dct, all_words):
    return [dct.get(word, 0) for word in all_words]


def vector2bytes(vec):
    return struct.pack(str(len(vec)) + 'd', *vec)


def texts2bytess(texts):
    all_words = set()
    dicts = [str2dict(text) for text in texts]
    for dic in dicts:
        all_words |= dic.keys()
    all_words_ordered = list(all_words)
    bytess = [vector2bytes(dict2vector(dic, all_words_ordered))
              for dic in dicts]
    return bytess


def main():
    """
    Run the program.

    Reads command line arguments and drive computation.

    """
    res1 = str2dict("Hello my 			name is mon' name. How are are\n you?")
    res2 = str2dict('It is nice to have a name like you have one, right.')
    print(res1)
    print(res2)

    all_words = set()
    all_words |= res1.keys()
    all_words |= res2.keys()
    print(all_words)

    all_words_ordered = list(all_words)

    v1 = dict2vector(res1, all_words_ordered)
    v2 = dict2vector(res2, all_words_ordered)

    b1 = vector2bytes(v1)
    b2 = vector2bytes(v2)

    print(v1)
    print(v2)

    print(b1)
    print(b2)


if __name__ == '__main__':
    main()
