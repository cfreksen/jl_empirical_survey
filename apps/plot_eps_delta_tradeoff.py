from collections import namedtuple
import glob
import matplotlib.pyplot as plt
import datetime
import argparse
from math import log2

from parameters import figure_label_size as label_size, figure_size, min_delta_margin, max_delta, num_deltas_plotted

DataHeader = namedtuple('DataHeader',
                        ['jlt_label', 'gen_label', 'gen_id', 'input_size', 'target_dimension', 'num_measurements', 'num_jlt_samples'])
PlotConfig = namedtuple('PlotConfig',
                        ['title_format'])

data_type2plot_config = {'dense': PlotConfig(r'Dense embedders, {0}, $d = {1}, m = {2}$'),
                         'sparse': PlotConfig(r'Sparse embedders, {0}, $|x|_0 = {1}, m = {2}$')}


def frange(low, high, step):
    curr = low
    i = 1
    epsilon = step / 2.0
    while curr + epsilon < high:
        yield curr
        curr = low + i * step
        i += 1


def parse_output(filename):
    print(f'Parsing {filename}', end='\r')
    distortion_data = []
    jlt_label = filename
    gen_label = 'Unknown Generator'
    gen_id = '?'
    input_size = None
    target_dimension = None
    num_measurements = None
    num_jlt_samples = None

    with open(filename) as f:
        line = f.readline().strip()

        while line.startswith('#') or line == '':
            if 'JLT label:' in line:
                jlt_label = line.split(':')[1].strip()
            elif 'Generator label:' in line:
                gen_label = line.split(':')[1].strip()
            elif 'Generator ID:' in line:
                gen_id = line.split(':')[1].strip()
            elif 'target dimension:' in line:
                target_dimension = int(line.split(':')[1])
            elif 'input size:' in line:
                input_size = int(line.split(':')[1])
            elif 'num measurements:' in line:
                num_measurements = int(line.split(':')[1])
            elif 'num jlt samples:' in line:
                num_jlt_samples = int(line.split(':')[1])
            line = f.readline().strip()

        while line != '' and not line.startswith('#'):
            eps, cnt = line.split()
            eps = float(eps)
            cnt = int(cnt)
            for _ in range(cnt):
                distortion_data.append(eps)
            line = f.readline().strip()

    return DataHeader(jlt_label, gen_label, gen_id, input_size, target_dimension, num_measurements, num_jlt_samples), sorted(distortion_data)


def make_plot_fig(config, gen_label, input_size, target_dimension):
    fig = plt.figure(figsize=figure_size)
    ax = fig.add_subplot()

    ax.set_xlabel(r'$\delta$', fontsize=label_size)
    ax.set_ylabel(r'$\varepsilon$', fontsize=label_size)
    ax.set_title(config.title_format.format(gen_label, input_size, target_dimension))

    return fig, ax


def plot_distortion_experiment(data_type='dense', use_markers=False):
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif')

    dm2fig = dict()
    plot_config = data_type2plot_config[data_type]
    data_folder = '../experiment_output/'
    pattern = f'{data_folder}dist_{data_type}_*.txt'
    paths = sorted(glob.glob(pattern))
    if len(paths) == 0:
        print(f'Error could not find any data files, glob pattern: {pattern}')

    if use_markers:
        markers = 'x^sod2|*+D3vp.<2PX'
        if len(markers) < len(paths):
            print('Warning: There are more plots than I prepared markers for!')
    else:
        markers = ''

    for path, marker in zip(paths, markers + ' ' * len(paths)):
        header, data = parse_output(path)
        input_size = header.input_size
        target_dimension = header.target_dimension
        num_measurements = header.num_measurements
        num_jlt_samples = header.num_jlt_samples
        gen_label = header.gen_label
        gen_id = header.gen_id
        if (gen_id, input_size, target_dimension) in dm2fig:
            fig, ax = dm2fig[(gen_id, input_size, target_dimension)]
        else:
            fig, ax = make_plot_fig(plot_config, gen_label, input_size, target_dimension)
            dm2fig[(gen_id, input_size, target_dimension)] = fig, ax

        xs, ys = [], []
        min_ldelta = log2(min_delta_margin / num_jlt_samples)
        max_ldelta = log2(max_delta)
        for ldelta in frange(min_ldelta, max_ldelta, (max_ldelta - min_ldelta) / num_deltas_plotted):
            delta = 2 ** ldelta
            idx = int((1 - delta) * num_measurements)
            xs.append(delta)
            ys.append(data[idx])
        ax.semilogx(xs, ys, label=header.jlt_label, marker=marker, basex=10)

    for (gen_id, input_size, target_dimension), (fig, ax) in dm2fig.items():
        ax.legend(loc='upper right')
        out_id = str(datetime.datetime.now().isoformat('-'))
        fig_path = f'dist_{data_type}_{gen_id}_{input_size:05}_{target_dimension:05}_{out_id}.pdf'
        print(f'Writing to {fig_path}', end='\r')
        fig.savefig(fig_path, bbox_inches='tight', dpi='figure', transparent=True)
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='A utility for generating plots for distortion/error probability tradeoff experiments')
    parser.add_argument('-d', '--datatype',
                        action='store', dest='data_type', choices=['dense', 'sparse'],
                        help='Choose which experiment data type to generate plot for')
    parser.add_argument('-m', '--markers',
                        action='store_true', dest='use_markers',
                        help='Add data point markers to plots')
    args = parser.parse_args()
    if args.data_type is None:
        data_type = input('Data type: ')
    else:
        data_type = args.data_type

    plot_distortion_experiment(data_type, use_markers=args.use_markers)
