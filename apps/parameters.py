figure_label_size = 20
figure_size = (15, 6)

# min_delta_margin * 1/num <= delta <= max_delta
min_delta_margin = 10
max_delta = 0.5
num_deltas_plotted = 256
